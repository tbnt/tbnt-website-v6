<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules_types', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('type', 255);
            $table->string('name', 255);
            $table->unsignedTinyInteger('position');

            $table->index('type');
            $table->index('position');
        });

        DB::table('modules_types')->insert([
            [
                'type' => 'wysiwyg',
                'name' => 'WYSIWYG',
                'position' => 1,
            ], [
                'type' => 'wysiwyg_2',
                'name' => 'WYSIWYG two columns',
                'position' => 2,
            ], [
                'type' => 'wysiwyg_image',
                'name' => 'WYSIWYG - image',
                'position' => 3,
            ], [
                'type' => 'image_wysiwyg',
                'name' => 'Image - WYSIWYG',
                'position' => 4,
            ], [
                'type' => 'image',
                'name' => 'Image fullwidth',
                'position' => 5,
            ], [
                'type' => 'image_2',
                'name' => 'Image two columns',
                'position' => 6,
            ], [
                'type' => 'image_3',
                'name' => 'Image three columns',
                'position' => 7,
            ], [
                'type' => 'video',
                'name' => 'Video fullwidth',
                'position' => 8,
            ], [
                'type' => 'quote',
                'name' => 'Quote',
                'position' => 9,
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
