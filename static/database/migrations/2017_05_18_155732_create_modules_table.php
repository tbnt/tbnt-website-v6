<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedTinyInteger('module_type_id');
            $table->unsignedInteger('image_id_1');
            $table->unsignedInteger('image_id_2');
            $table->unsignedInteger('image_id_3');
            $table->text('text_1');
            $table->text('text_2');
            $table->string('url', 255);
            $table->dateTime('created_at');
            $table->dateTime('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
