<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_langs', function (Blueprint $table)
        {
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('lang_id');
            $table->string('name', 255);

            $table->index('category_id');
            $table->index('lang_id');

            $table->unique(['category_id', 'lang_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
