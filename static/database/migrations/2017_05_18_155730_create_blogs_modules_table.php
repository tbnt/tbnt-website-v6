<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs_modules', function (Blueprint $table)
        {
            $table->unsignedInteger('blog_id');
            $table->unsignedInteger('module_id');
            $table->unsignedInteger('position');

            $table->index('blog_id');
            $table->index('module_id');
            $table->index('position');

            $table->unique(['blog_id', 'module_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
