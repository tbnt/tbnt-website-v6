<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_admin', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('username', 255);
            $table->string('password', 255);
            $table->string('remember_token', 255);
            $table->tinyInteger('type')->unsigned();
            $table->tinyInteger('is_active')->unsigned();
            $table->dateTime('created_at');
            $table->dateTime('updated_at');

            $table->index('is_active');

            $table->unique('username');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
