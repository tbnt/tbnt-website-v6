<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('lang_id');
            $table->unsignedInteger('image_id');
            $table->unsignedTinyInteger('is_active');
            $table->dateTime('visible_at');
            $table->dateTime('created_at');
            $table->dateTime('updated_at');

            $table->index('is_active');
            $table->index('visible_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
