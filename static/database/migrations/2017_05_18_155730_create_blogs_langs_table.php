<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs_langs', function (Blueprint $table)
        {
            $table->unsignedInteger('blog_id');
            $table->unsignedInteger('lang_id');
            $table->string('title', 255);
            $table->string('url', 255);
            $table->string('meta_description', 255);
            $table->text('description');

            $table->index('blog_id');
            $table->index('lang_id');
            $table->index('url');

            $table->unique(['blog_id', 'lang_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
