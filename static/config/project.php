<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Domain
    |--------------------------------------------------------------------------
    |
    | This value is the main domain of the project.
    |
    | Admin section can only be accessible if the current domain is matching
    | this main domain.
    |
    */

    'domain' => env('PROJECT_DOMAIN', 'localhost'),

    /*
    |--------------------------------------------------------------------------
    | Application Email
    |--------------------------------------------------------------------------
    |
    | This value is the main email of the project.
    |
    */

    'email' => env('PROJECT_EMAIL', 'test@email.com'),

    /*
    |--------------------------------------------------------------------------
    | Reset Password Duration
    |--------------------------------------------------------------------------
    |
    | This value is the time of the validity for the password recovery.
    |
    */

    'reset_password_duration' => 4,

    /*
    |--------------------------------------------------------------------------
    | Activate Account Duration
    |--------------------------------------------------------------------------
    |
    | This value is the time of the validity for the account activation.
    |
    */

    'activation_token_duration' => 4,

];
