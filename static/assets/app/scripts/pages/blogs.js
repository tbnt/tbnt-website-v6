'use strict';

import Animate from '../components/animate';
import Pagination from '../helpers/pagination';

let el = {};

let _pagination = null;
let _paginationConfig = {};

const init = () =>
{
	_paginationConfig = _Scripts.get('BLOGS_CONFIG');

	initDOM();
	initEvents();
	initPagination();
};

const initDOM = () =>
{
	el = {};

	el.pagination_none = $('[data-pagination-none]')
};

const initEvents = () =>
{

};

const initPagination = () =>
{
	el.pagination_none.setHidden(true);

	if (_pagination !== null)
		_pagination.clear();

	_pagination = new Pagination({
		url: 'blogs',
		pagination: 'blogs',
		content: 'blogs',
		data: _paginationConfig,
		onLoad: (data, isEnd) => {
			if (_.keys(data.blogs).length === 0) el.pagination_none.setHidden(false);

			Animate.latestPost();
		},
		onComplete: () => {},
	});
};

export default
{
	init,
};
