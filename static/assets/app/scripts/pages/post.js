'use strict';

import Animate from '../components/animate';

let el = {};

const init = () =>
{
	Animate.postAnimIn();
	postTop();
};
const postTop = () =>
{
	var arrowScrollTop = $('footer .top');

	arrowScrollTop.on('click', function(){
		TweenLite.to('body', 1, {scrollTo:0});
	});
};
export default
{
	init,
};
