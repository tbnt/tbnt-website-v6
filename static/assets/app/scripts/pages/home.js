'use strict';

let el = {};

let isMobile = false;

const init = () =>
{
	isMobile = _Utils.isMobile.any();

	initDOM();
	initEvents();
};

const initDOM = () =>
{
	el = {};

	el.video = $('.reel-video');
	el.videoContainer = $('.video-container');
	el.button = $('.video-play-button');
	el.controls = $('.video-controls');
};

const initEvents = () =>
{
	//el.button.on('click', function() { togglePlay(); });
	el.video.on('click', function() { togglePlay(); });
	el.videoContainer.on('click', function() { togglePlay(); });
};

const togglePlay = () =>
{
	const video = el.video.get(0);

	if (video.paused) {
		video.play();
		el.controls.addClass( "hide" );
		if (isMobile === true) {
			if (video.requestFullscreen) {
				video.requestFullscreen();
			} else if (video.webkitRequestFullscreen){
				video.webkitRequestFullscreen();
			} else if (video.mozRequestFullScreen){
				video.mozRequestFullScreen();
			} else if (video.msRequestFullscreen){
				video.msRequestFullscreen();
			}
		}
	} else {
		video.pause();
		el.controls.removeClass( "hide" );
	}
};

export default
{
	init,
};
