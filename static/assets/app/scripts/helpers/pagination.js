'use strict';

const _Pagination = function(config)
{
	let el = {};

	let _isLocked = false;
	let _isFullyLoaded = false;
	let _isCleared = false;
	let _windowHeight = 0;

	let _config = _.assign({
		url: '',
		data: {},
		pagination: '',
		content: '',
		trigger: document,
		html: 'html',
		load: true,
		start: 0,
		count: 20,
		delta: 2000,
		debug: false,
		onStart: () => '',
		onLoad: () => '',
		onError: () => '',
		onComplete: () => '',
	}, config);

	const init = () =>
	{
		if (_config.url === undefined || _config.url === '')
			return warnError('Pagination missing "url" parameter:');

		if (_config.pagination === undefined || _config.pagination === '')
			return warnError('Pagination missing "pagination" parameter:');

		if ($('[data-pagination="' + _config.pagination + '"]').length === 0)
			return warnError('Pagination undefined "pagination" element:');

		if ($(_config.trigger).length === 0)
			return warnError('Pagination undefined "trigger" element:');

		initDOM();
		initEvents();
		initPagination();

		lock(false);

		if (_config.load !== false)
			loadItems();

		return true;
	};

	const initDOM = () =>
	{
		el = {};

		el.app = $(_config.trigger);

		el.pagination = $('[data-pagination="' + _config.pagination + '"]');
		el.content = el.pagination.find('[data-pagination-content]');
		el.loader = el.pagination.find('[data-pagination-loader]');
	};

	const initEvents = () =>
	{
		el.app.on('scroll', shouldLoadItems);

		$(window).on('resize', initPagination);
		$(document).one('pjax:before', clearEvents);
	};

	const initPagination = () =>
	{
		_windowHeight = $(window).height();
	};

	const shouldLoadItems = (e) =>
	{
		e.preventDefault();

		if (_isFullyLoaded === true)
			clearEvents();

		if (_isCleared === true)
			return;

		requestAnimationFrame(() => {
			const documentTop = $(document).scrollTop();
			const documentHeight = $(document).height();

			const delta = Math.abs((documentTop + _windowHeight) - documentHeight);

			if (_config.debug === true)
				console.log('Pagination scroll:', { documentTop, documentHeight, delta });

			if (_config.delta > delta)
				loadItems();
		});
	};

	const loadItems = () =>
	{
		if (lock() === true) return;
			lock(true);

		showLoader(true);
		emitStart();

		const values = _.assign({
			count: _config.count,
		}, _config.data, {
			start: _config.start,
		});

		_Ajax.post(_config.url, values, {
			debug: _config.debug,
			success: (data) => {
				if (_isCleared === true)
					return _isFullyLoaded = true;

				const contentData = data[_config.content];
				const contentCount = _.keys(contentData).length;
				const contentEnd = contentCount < values.count;

				if (_config.debug === true)
					console.log('Pagination load:', { contentCount, contentData, contentEnd, values, });

				el.content.append(_.map(contentData, (content) => content[_config.html] || ''));

				_config.start += contentCount;
				_isFullyLoaded = contentEnd;

				lock(contentEnd);
				emitLoad(data, contentEnd);
			},
			error: (error) => {
				emitError();
			},
			complete: (error) => {
				showLoader(false);

				emitComplete();
			},
		});
	};

	const showLoader = (visible) =>
	{
		el.loader.setHidden(visible === false);
	};

	const lock = (isLocked) =>
	{
		if (isLocked !== undefined)
			_isLocked = isLocked;

		return _isLocked;
	};

	const clear = () =>
	{
		_config.start = 0;

		el.content.empty();

		clearEvents();
		lock(true);
	};

	const clearEvents = () =>
	{
		_isCleared = true;

		el.app.off('scroll', shouldLoadItems);

		$(window).off('resize', initPagination);
	};

	const onLoad = (callback) =>
	{
		_config.onLoad = callback;
	};

	const onError = (callback) =>
	{
		_config.onError = callback;
	};

	const onComplete = (callback) =>
	{
		_config.onComplete = callback;
	};

	const emitStart = () =>
	{
		_config.onStart();
	};

	const emitLoad = (data, isEnd) =>
	{
		_config.onLoad(data, isEnd);
	};

	const emitError = () =>
	{
		_config.onError();
	};

	const emitComplete = () =>
	{
		_config.onComplete();
	};

	const warnError = (message) =>
	{
		return console.warn(message, _config) || false;
	};

	return init() && {
		loadItems,
		showLoader,
		lock,
		clear,
		onLoad,
		onError,
		onComplete,
	};
};

export default _Pagination;
