'use strict';

const callback = function()
{
	//initMap();
};


var initMap = function()  {
	var uluru = {lat: -25.363, lng: 131.044};
	var map = new google.maps.Map(document.getElementById('catalog-map'), {
		zoom: 4,
		center: uluru
	});
	var marker = new google.maps.Marker({
		position: uluru,
		map: map
	});
};

export default
{
	initMap,
};

$(document).ready(callback);

$(document).on('pjax:callback', callback);
