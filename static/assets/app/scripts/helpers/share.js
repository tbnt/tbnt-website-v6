'use strict';

let el = {};

const _shareWindowWidth = 600;

let _twitterAccount = '';

const init = function()
{
	_twitterAccount = _.trim(_.trim(_Scripts.get('TWITTER_ACCOUNT')), '@');

	initDOM();
	initListeners();
};

const initDOM = function()
{
	el = {};

	el.share_facebook = $('[data-share="facebook"]');
	el.share_twitter = $('[data-share="twitter"]');
	el.share_pinterest = $('[data-share="pinterest"]');
	el.share_linked_in = $('[data-share="linkedin"]');

	el.share_descrption = $('[data-share-description]');
	el.share_media = $('[data-share-media]');
};

const initListeners = function()
{
	el.share_facebook.on('click', function() { shareFacebook(); });
	el.share_twitter.on('click', function() { shareTwitter(); });
	el.share_pinterest.on('click', function() { sharePinterest(); });
	el.share_linked_in.on('click', function() { shareLinkedIn(); });
};

const openShareModal = function(socialUrl, shareUrl)
{
	const box = getBoxPosition();

	window.open(socialUrl + shareUrl, '_blank', 'status=0, menubar=0, toolbar=0, resizable=1, scrollbars=1, top=100, left=' + box + ', width=' + _shareWindowWidth + ', height=370');
};

const shareFacebook = function()
{
	requestLink('https://facebook.com/sharer.php?u=');
};

const shareTwitter = function()
{
	const via = getTwitterAccount();
	const text = getShareText();

	requestLink('https://twitter.com/intent/tweet?' + (via ? 'via=' + via + '&' : '') + (text ? 'text=' + text + '&' : '') + 'url=');
};

const sharePinterest = function()
{
	const media = getShareMedia();
	const text = getShareText();

	requestLink('http://pinterest.com/pin/create/button/?' + (media ? 'media=' + media + '&' : '') + (text ? 'description=' + text + '&' : '') + 'url=');
};

const shareLinkedIn = function()
{
	requestLink('https://www.linkedin.com/cws/share?url=');
};

const requestLink = function(socialUrl)
{
	openShareModal(socialUrl, getShareUrl());
};

const getShareUrl = function()
{
	return window.location.href;
};

const getShareText = function()
{
	return el.share_descrption.length ? el.share_descrption.text() : $('head').find('meta[property="og:description"]').attr('content');
};

const getShareMedia = function()
{
	return el.share_media.length ? el.share_media.attr('src') : $('head').find('meta[property="og:image"]').attr('content');
};

const getTwitterAccount = function()
{
	return _twitterAccount;
};

const getBoxPosition = function()
{
	return ($(window).width() - _shareWindowWidth) / 2;
};

export default
{
	init,
	facebook: shareFacebook,
	twitter: shareTwitter,
	pinterest: sharePinterest,
	linkedin: shareLinkedIn,
};
