'use strict';

const _defaultOptions = {
	items: 1,
	dots: true,
	nav: true,
	lazyLoad: true,
	navText: [
		'<img src="public/images/icons/svg/arrow-right-color.svg">',
		'<img src="public/images/icons/svg/arrow-right-color.svg">',
	]
};

const initCarousel = ($el, options = {}) =>
{
	if ($el.length === 0)
		return emitError('Carousel init error: $el not exists', $el);

	for (var i = $el.length - 1; i >= 0; i--)
		$el.eq(i).owlCarousel(_.assign({}, _defaultOptions, options));
};

const emitError = function()
{
	return console.warn(...arguments) && false;
};

export default
{
	init: initCarousel,
};
