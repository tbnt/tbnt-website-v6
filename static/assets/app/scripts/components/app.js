'use strict';

let _version = 0;

const init = () =>
{
	_version = Laravel.version;

	initEvents();

	initAjax();
};

const initEvents = () =>
{
	$(document).on('pjax:callback', function() { checkVersion(); });
};

const initAjax = () =>
{
	_Ajax.setPrefix('ajax');
	_Ajax.setVersion(_version);
};

const checkVersion = () =>
{
	if (Laravel.version !== _version) _pages.showNewVersionModal();
};

export default
{
	init,
};
