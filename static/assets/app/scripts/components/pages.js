'use strict';

import Breakdown from './breakdown';


let el = {};

const init = () =>
{
	initDOM();
	initEvents();
	animStart();
};

const initDOM = () =>
{
	el = {};
};

const initEvents = () =>
{
	$(document)
	.on('click touch', '[data-prevent]', function(e) { e.preventDefault(); return false; })
	.on('click touch', '[data-go-to-top]', function(e) { scrollToTop(); });
};

const animEnd = () =>
{

};

const animStart = () =>
{
	var tlAnimInit = new TimelineMax()
	tlAnimInit.staggerTo('[data-point]', .6, { y:0, autoAlpha: 1}, .2)
	.staggerFromTo('[data-element-intro ]', .6, { scale:.9, y:-40, autoAlpha: 0}, { scale:1, y:0, autoAlpha: 1}, .2, '=-.2')
	.to('[data-intro-hidden ]', .6, { autoAlpha: 1}, '=-.2')
	.to('[data-shining-effect]',  .7, {x: '100%'})
	.set('body', {'overflow-y': 'auto'});



	if (_Utils.isMobile.any() === false) {
		var introEl1 = '.intro-composition [data-element-1]';
		var introEl2 = '.intro-composition [data-element-2]';
		var introEl3 = '.intro-composition [data-element-3]';
		var introEl4 = '.intro-composition [data-element-4]';

		var tlAnimIntroEl1 = new TimelineMax({
			delay: 3,
			repeat: -1,
		})
		tlAnimIntroEl1
		.to(introEl1, 6, {x: -32,y:+16,ease: Sine.easeInOut})
		.to(introEl1, 6, {x: -16,y:-16,ease: Sine.easeInOut})
		.to(introEl1, 6, {x: 0,y:0,ease: Sine.easeInOut})


		var tlAnimIntroEl2 = new TimelineMax({
			delay: 3,
			repeat: -1,
		})
		tlAnimIntroEl2
		.to(introEl2, 6, {x: -16,y:-16,ease: Sine.easeInOut})
		.to(introEl2, 6, {x: 0,y:+32,ease: Sine.easeInOut})
		.to(introEl2, 6, {x: 0,y:0,ease: Sine.easeInOut})

		var tlAnimIntroEl3 = new TimelineMax({
			delay: 3,
			repeat: -1,
		})
		tlAnimIntroEl3
		.to(introEl3, 6, {x: -32,y:-32,ease: Sine.easeInOut})
		.to(introEl3, 6, {x: +16,y:+16,ease: Sine.easeInOut})
		.to(introEl3, 6, {x: 0,y:0,ease: Sine.easeInOut})

		var tlAnimIntroEl4 = new TimelineMax({
			delay: 3,
			repeat: -1,
		})
		tlAnimIntroEl4
		.to(introEl4, 6, {x: +32,y:-16,ease: Sine.easeInOut})
		.to(introEl4, 6, {x: -32,y:-32,ease: Sine.easeInOut})
		.to(introEl4, 6, {x: 0,y:0,ease: Sine.easeInOut})
	}

};

const aminApp = () =>
{
	animEnd();
};

const scrollToTop = () =>
{
	TweenLite.to('body', 1, {scrollTo:0});
};

export default {
	init,
	scrollTop: scrollToTop,
};

$(document).on('pjax:send', function() { animStart(); });
$(document).on('pjax:callback', function() { animEnd(); });
