'use strict';

let el = {};

const init = () =>
{
	initDOM();
	initEvents();
	initMenu();
};

const initDOM = () =>
{
	el = {};

	el.navigation = $('[data-navigation]');
};

const initEvents = () =>
{

};

const initMenu = () =>
{
	$('[data-menu-item]').removeClass('link-active');
	$('[data-menu-item="' + Laravel.menu + '"]').addClass('link-active');
};

export default
{
	init,
};
