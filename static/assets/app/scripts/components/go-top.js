'use strict';

let el = {};

const init = () =>
{
	initDOM();
	initEvents();
};

const initDOM = () =>
{
	el = {};

	el.go_to_top = $('[data-go-to-top]');
};

const initEvents = () =>
{
	el.go_to_top.on('click', function() { scrollToTop(); });
};

const scrollToTop = () =>
{
	TweenLite.to('body', 1, {scrollTo:0});
};

export default
{
	init,
	scrollTop: scrollToTop,
};
