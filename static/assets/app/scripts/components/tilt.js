'use strict';

import Breakdown from './breakdown';

let el = {};

let _tilts = null;

const init = () =>
{
	initDOM();

	if (_Utils.isNavigator.ieEdge() === true || _Utils.isNavigator.safari() === true) {
		return el.tilts_images.css('transform', 'none');
	}

	initEvents();
	initTilt(true);
};

const initDOM = () =>
{
	el = {};

	el.tilts = $('.tilt');
	el.tilts_images = el.tilts.find('img');
};

const initEvents = () =>
{
	_Utils.addEvent('breakdown.updated', updateTilt);
};

const updateTilt = (breakdown) =>
{
	if (Breakdown.is(['mobile', 'tablet']) === true)
		destroyTilt();
	else
		initTilt();
};

const initTilt = (forceInit = false) =>
{
	if (_tilts !== null && forceInit !== true)
		return;

	if (forceInit === true)
		destroyTilt();

	if (Breakdown.is(['mobile', 'tablet']) === true)
		return;

	_tilts = el.tilts.tilt({
		maxTilt:        20,
		perspective:    1000, // Transform perspective, the lower the more extreme the tilt gets.
		easing:         'cubic-bezier(.03,.98,.52,.99)',    // Easing on enter/exit.
		scale:          1, // 2 = 200%, 1.5 = 150%, etc..
		speed:          300, // Speed of the enter/exit transition.
		transition:     true, // Set a transition on enter/exit.
		axis:           null, // What axis should be disabled. Can be X or Y.
		reset:          true, // If the tilt effect has to be reset on exit.
		glare:          false, // Enables glare effect
		maxGlare:       1, // Glare effect (0 - 1)
		reset: 			true
	});
};

const destroyTilt = () =>
{
	if (_tilts === null)
		return;

	_tilts.tilt.destroy.call(_tilts);
	_tilts = null;
};

export default
{
	init,
};
