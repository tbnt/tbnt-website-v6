'use strict';

let el = {};

let _currentBreakdown = '';

const _breakdowns = {
	mobile: 0,
	tablet: 768,
	desktop: 1024,
	widescreen: 1200,
};

const init = () =>
{
	initDOM();
	initEvents();

	resizeBreakdown();
};

const initDOM = () =>
{
	el = {};

	el.window = $(window);
};

const initEvents = () =>
{
	el.window.on('resize', resizeBreakdown);
};

const resizeBreakdown = () =>
{
	const newBreakdown = getNewBreakdown();
	let emitEvent = false;

	if (_currentBreakdown !== newBreakdown)
		emitEvent = true;

	_currentBreakdown = newBreakdown;

	if (emitEvent === true)
		_Utils.triggerEvent('breakdown.updated', newBreakdown);
};

const getNewBreakdown = () =>
{
	const windowWidth = (el.window || $(window)).width();
	let newBreakdown = '';

	for (let i = 0, k = _.keys(_breakdowns), c = k.length; i < c; i++) {
		const breakdownName = k[i];
		const breakdownValue = _breakdowns[breakdownName];

		const nextBreakdownName = k[i + 1];
		const nextBreakdownValue = _breakdowns[nextBreakdownName];

		if (windowWidth >= breakdownValue) {
			if (nextBreakdownValue !== undefined) {
				if (windowWidth < nextBreakdownValue) {
					newBreakdown = breakdownName;
					break;
				}
			}
			else {
				newBreakdown = breakdownName;
				break;
			}
		}
	}

	return newBreakdown;
};

const getCurrentBreakdown = () =>
{
	return _currentBreakdown;
};

const isBreakdown = (breakdownNames) =>
{
	return Boolean(_.filter(_.map(_Utils.initArray(breakdownNames), (breakdownName) => (_currentBreakdown || getNewBreakdown()) === breakdownName)).length);
};

export default
{
	init,
	current: getCurrentBreakdown,
	is: isBreakdown,
};
