'use strict';

// Utils
import _Ajax from '../../utils/js/ajax';
import _App from '../../utils/js/app';
import _Pjax from '../../utils/js/pjax';
import _Scripts from '../../utils/js/scripts';
import _Trans from '../../utils/js/trans';
import _Url from '../../utils/js/url';
import _Utils from '../../utils/js/utils';

// Components
import Animate from './components/animate';
import App from './components/app';
import Menu from './components/menu';
import Pages from './components/pages';
import Breakdown from './components/breakdown';
import Tilt from './components/tilt';

// Helpers
import Share from './helpers/share';

// Pages
import Blogs from './pages/blogs';
import Post from './pages/post';
import Home from './pages/home';

// Make modules accessible
_Utils.access(_Ajax, '_Ajax');
_Utils.access(_App, '_App');
_Utils.access(_Pjax, '_Pjax');
_Utils.access(_Scripts, '_Scripts');
_Utils.access(_Trans, '_Trans');
_Utils.access(_Url, '_Url');
_Utils.access(_Utils, '_Utils');

// Main
$(document).ready(function()
{
	_App.init([
		{ page: 'app', js: [App] },
		]);

	_Utils.addEvent('laravel.config.ready', () => {
		_App.registerJs([
			{ page: 'app', js: [Pages, Menu, Animate, Share, Breakdown, Tilt] },
			{ page: 'home', js: [Home] },
		]);
	});
});
