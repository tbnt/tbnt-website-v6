'use strict';

import _pages from './pages';

let _version = 0;

const init = () =>
{
	_version = Laravel.version;

	initEvents();

	initUrl();
	initAjax();
	initMoment();
	initUIKit();
	initProgress();
	initFastClick();
};

const initEvents = () =>
{
	$(document).on('pjax:callback', function() { checkVersion(); });

	_Utils.addEvent('laravel.config.ready', () => _pages.showApp());
};

const initUrl = () =>
{
	_Url.setFile('urls_admin')
	_Url.setPrefix('admin/')
};

const initAjax = () =>
{
	_Ajax.setPrefix('admin');
	_Ajax.setVersion(_version);
};

const initMoment = () =>
{
	moment.locale(Laravel.lang.code);
};

const initUIKit = () =>
{
	UIkit.components.tab.options.defaults.duration = 0;
};

const initProgress = () =>
{
	NProgress.configure({
		trickleSpeed: 100,
	});
};

const initFastClick = () =>
{
	if (_Utils.isMobile.ios() === true)
		FastClick.attach(document.body);
};

const checkVersion = () =>
{
	if (Laravel.version !== _version) _pages.showNewVersionModal();
};

export default
{
	init,
};
