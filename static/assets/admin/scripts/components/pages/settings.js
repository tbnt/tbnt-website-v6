'use strict';

let el = {};

const init = () =>
{
	initDOM();
	initEvents();
};

const initDOM = () =>
{
	el = {};

	el.form = $('[data-form]');

	el.button_submit = $('[data-submit]');
};

const initEvents = () =>
{
	el.button_submit.on('click', function() { updateSettings(); });
};

const updateSettings = () =>
{
	const formUrl = _Url.get('settings_edit', {}, false);
	const formData = new FormData(el.form.get(0));

	_Form.clearErrors();
	_Ajax.post(formUrl, formData, {
		loader_error: true,
		success: (data) => {
			_Pjax.refresh(() => _Notification.show({ message: 'The settings has been updated', status: 'success' }));
		},
		error: (error) => _Form.addErrors(error, {
			form: el.form,
		}),
	});
};

export default
{
	init,
};
