'use strict';

let el = {};

let _blog = {};

let _isUpdating = false;
let _nextModuleId = 1;

const init = () =>
{
	_blog = _Scripts.get('BLOG', {});

	initDOM();
	initEvents();

	initWysiwyg(el.modules.find('.module-content-wysiwyg').find('textarea'));
	initCaption(el.modules.find('.module-content-image').find('textarea'));
	initTooltip(el.form.find('[data-title]'));
};

const initDOM = () =>
{
	el = {};

	el.form = $('[data-form-blog]');
	el.form_save = $('[data-submit-blog]');

	el.preview = $('[data-preview-blog]');

	el.modules = $('[data-modules]');
	el.modules_templates = $('[data-modules-templates]');
};

const initEvents = () =>
{
	el.preview.on('click', function() { previewBlog(); })
	el.form_save.on('click', function() { updateBlog($(this).attr('data-submit-blog')); });

	$(document)
		.on('click', '[data-module-push-up]', function () { moveModuleUp($(this).closest('.modules')); })
		.on('click', '[data-module-push-down]', function () { moveModuleDown($(this).closest('.modules')); })
		.on('click', '[data-module-delete]', function () { removeModule($(this).closest('.modules')); })
		.on('click', '[data-module-type]', function () { addModule($(this)); })
		.on('mouseenter', '[data-modules-types-hover]', function () { showModulesTypes($(this).siblings('[data-modules-types]')); })
		.on('mouseleave', '[data-module-add]', function () { showModulesTypes($(this).find('[data-modules-types]'), false); });
};

const initWysiwyg = ($textareas) =>
{
	const toolbarButtons = [
		'bold',
		'italic',
		'insertLink',
	];

	_Froala.init($textareas, {
		toolbarButtons: toolbarButtons,
		toolbarButtonsMD: toolbarButtons,
		toolbarButtonsSM: toolbarButtons,
		toolbarButtonsXS: toolbarButtons,
	});
};

const initCaption = ($captions) =>
{
	const toolbarButtons = [
		'insertLink',
	];

	_Froala.init($captions, {
		toolbarButtons: toolbarButtons,
		toolbarButtonsMD: toolbarButtons,
		toolbarButtonsSM: toolbarButtons,
		toolbarButtonsXS: toolbarButtons,
		linkAlwaysBlank: true,
		charCounterCount: false,
		multiLine: false,
		heightMin: 0,
	});
};

const initTooltip = ($tooltips) =>
{
	for (let i = 0, k = $tooltips, c = k.length; i < c; i++)
		k.eq(i).attr('title', k.eq(i).attr('data-title'));
};

const updateBlog = (updateMode) =>
{
	if (lockUpdate() === true) return;
		lockUpdate(true);

	const formUrl = _Url.get('blog_edit', { id: _blog.id }, false);
	const formData = new FormData(el.form.get(0));

	_Form.clearErrors();
	_Ajax.post(formUrl, formData, {
		loader_error: true,
		success: (data) => {
			const url = updateMode === 'back' ? _Url.get('blogs') : _Url.get('blog_edit', { id: data.blog.id });
			const callback = () => _Notification.show({ message: 'The blog has been updated', status: 'success' });

			_Pjax.href(url, callback);
		},
		error: (error) => _Form.addErrors(error, {
			names: { title: 'langs.title', description: 'langs.description', meta_description: 'langs.meta_description', url: 'langs.url' },
			form: el.form,
		}),
		complete: (error) => {
			lockUpdate(false);
		},
	});
};

const previewBlog = () =>
{
	const formUrl = _Url.get('blog_preview', { id: _blog.id }, false);
	const formData = new FormData(el.form.get(0));

	_Form.clearErrors();
	_Ajax.post(formUrl, formData, {
		loader: true,
		success: (data) => {
			setTimeout(() => window.open('admin/' + formUrl, '_blank'), 100);
		},
		error: (error) => _Form.addErrors(error, {
			names: { title: 'langs.title', description: 'langs.description', meta_description: 'langs.meta_description', url: 'langs.url' },
			form: el.form,
		}),
	});
};

const lockUpdate = (isUpdating) =>
{
	if (isUpdating !== undefined) {
		_isUpdating = isUpdating;
		_Loader[isUpdating ? 'start' : 'stop']();
	}

	return _isUpdating;
};

const showModulesTypes = ($modulesTypes, show = true) =>
{
	$modulesTypes.setHidden(show === false);
};

const addModule = ($module) =>
{
	const moduleType = $module.attr('data-module-type');

	const newModuleId = 'new-' + getNewModuleId();
	const newModule = el.modules_templates.find('.module-' + moduleType).get(0).outerHTML
		.split('NEWID').join(newModuleId);

	const $parentModule = $module.closest('.modules');

	$parentModule.after(newModule);

	const $newModule = $parentModule.next();

	if ($newModule.find('.module-content-wysiwyg').find('textarea').length)
		initWysiwyg($newModule.find('.module-content-wysiwyg').find('textarea'));

	if ($newModule.find('.module-content-image').find('textarea').length)
		initCaption($newModule.find('.module-content-image').find('textarea'));

	if ($newModule.find('[data-title]').length)
		initTooltip($newModule.find('[data-title]'));

	showModulesTypes($parentModule.find('[data-modules-types]'), false);
};

const moveModuleUp = ($module) =>
{
	$module.after($module.prev().detach());
};

const moveModuleDown = ($module) =>
{
	$module.before($module.next().detach());
};

const removeModule = ($module) =>
{
	$module.remove();
};

const getNewModuleId = () =>
{
	return _nextModuleId += 1;
};

export default
{
	init,
};
