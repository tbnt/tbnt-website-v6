'use strict';

let el = {};

let _isUpdating = false;

const init = () =>
{
	initDOM();
	initEvents();
};

const initDOM = () =>
{
	el = {};

	el.form = $('[data-form-blog]');
	el.form_save = $('[data-submit-blog]');

	el.form_field_name = $('[name="langs[title]"]');
	el.form_field_url = $('[name="langs[url]"]');
};

const initEvents = () =>
{
	el.form_field_name.on('blur', function() { updateUrl($(this).val()); });
	el.form_save.on('click', function() { insertBlog($(this).attr('data-submit-blog')); });
};

const insertBlog = (insertMode) =>
{
	if (lockUpdate() === true) return;
		lockUpdate(true);

	const formUrl = _Url.get('blog_new', {}, false);
	const formData = new FormData(el.form.get(0));

	_Form.clearErrors();
	_Ajax.post(formUrl, formData, {
		loader_error: true,
		success: (data) => {
			const url = insertMode === 'edit' ? _Url.get('blog_edit', { id: data.blog.id }) : _Url.get('blogs');
			const callback = () => _Notification.show({ message: 'The blog has been created', status: 'success' });

			_Pjax.href(url, callback);
		},
		error: (error) => _Form.addErrors(error, {
			names: { title: 'langs.title', description: 'langs.description', meta_description: 'langs.meta_description', url: 'langs.url' },
			form: el.form,
		}),
		complete: (error) => {
			lockUpdate(false);
		},
	});
};

const lockUpdate = (isUpdating) =>
{
	if (isUpdating !== undefined) {
		_isUpdating = isUpdating;
		_Loader[isUpdating ? 'start' : 'stop']();
	}

	return _isUpdating;
};

const updateUrl = (url) =>
{
	url = _.trim(url);

	if (url === '' || _.trim(el.form_field_url.val()) !== '')
		return;

	el.form_field_url.val(_.kebabCase(url));
};

export default
{
	init,
};
