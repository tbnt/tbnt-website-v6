'use strict';

let el = {};

const init = () =>
{
	initDOM();
	initEvents();
	initDescription();
};

const initDOM = () =>
{
	el = {};

	el.form = $('[data-form-blog]');
	el.form_description = el.form.find('[name="langs[description]"]');
};

const initEvents = () =>
{
};

const initDescription = () =>
{
	const toolbarButtons = [
		'bold',
		'italic',
	];

	_Froala.init(el.form_description, {
		toolbarButtons: toolbarButtons,
		toolbarButtonsMD: toolbarButtons,
		toolbarButtonsSM: toolbarButtons,
		toolbarButtonsXS: toolbarButtons,
	});
};

export default
{
	init,
};
