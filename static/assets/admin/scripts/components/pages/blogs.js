'use strict';

let el = {};

let _mixitup = null;

const init = () =>
{
	initDOM();
	initEvents();
	initMixitup();
};

const initDOM = () =>
{
	el = {};

	el.filters_languages = $('[data-filters-languages]');
	el.filters_categories = $('[data-filters-categories]');
	el.filters_status = $('[data-filters-status]');

	el.blogs = $('[data-blogs]');
	el.blogs_none = $('[data-blogs-none]');
	el.blogs_items = $('[data-blog-id]');
	el.blogs_duplicate = $('[data-blog-duplicate]');
	el.blogs_delete = $('[data-blog-delete]');
};

const initEvents = () =>
{
	el.filters_languages.on('click', '[type="reset"]', function(e) { resetLanguagesFilters(); });
	el.filters_categories.on('click', '[type="reset"]', function(e) { resetCategoriesFilters(); });
	el.filters_status.on('click', '[type="reset"]', function(e) { resetStatusFilters(); });

	el.blogs_items.on('click', function(e) { if ($(e.target).closest('[data-blog-delete], [data-blog-duplicate]').length) return; goToBlog($(this)); });
	el.blogs_duplicate.on('click', function() { duplicateBlog($(this)); });
	el.blogs_delete.on('click', function() { deleteBlog($(this)); });
};

const initMixitup = () =>
{
	if (_mixitup !== null)
		_mixitup.destroy();

	_mixitup = mixitup(el.blogs.get(0), {
		multifilter: {
			enable: true,
		},
		callbacks: {
			onMixStart: () => showNoBlogPost(false),
			onMixFail: () => showNoBlogPost(true),
		},
	});
};

const goToBlog = ($button) =>
{
	const blogId = $button.attr('data-blog-id');
	const formUrl = _Url.get('blog_edit', { id: blogId });

	_Pjax.href(formUrl);
};

const duplicateBlog = ($button) =>
{
	_Confirm.show('Do you want to duplicate this blog?', () => {
		const blogId = $button.attr('data-blog-duplicate');
		const formUrl = _Url.get('blog_duplicate', { id: blogId }, false);

		_Form.clearErrors();
		_Ajax.post(formUrl, {}, {
			loader_error: true,
			success: (data) => {
				_Pjax.refresh(_Notification.show({ message: 'The blog has been duplicated', status: 'success' }));
			},
			error: (error) => _Form.addErrors(error, {
				form: el.form,
			}),
		});
	});
};

const deleteBlog = ($button) =>
{
	_Confirm.show('Do you want to delete this blog?', () => {
		const blogId = $button.attr('data-blog-delete');
		const formUrl = _Url.get('blog_delete', { id: blogId }, false);

		_Form.clearErrors();
		_Ajax.post(formUrl, {}, {
			loader_error: true,
			success: (data) => {
				_Pjax.refresh(_Notification.show({ message: 'The blog has been deleted', status: 'success' }));
			},
			error: (error) => _Form.addErrors(error, {
				form: el.form,
			}),
		});
	});
};

const resetLanguagesFilters = () =>
{
	resetMixitupFilters('languages', el.filters_languages.find('input'));
};

const resetCategoriesFilters = () =>
{
	resetMixitupFilters('categories', el.filters_languages.find('input'));
};

const resetStatusFilters = () =>
{
	resetMixitupFilters('status', el.filters_languages.find('input'));
};

const resetMixitupFilters = (groupName, $inputs) =>
{
	const filters = $.map($inputs, (o) => o['value']).concat(['.filter-post']);

	_mixitup.setFilterGroupSelectors(groupName, filters);
	_mixitup.parseFilterGroups();

	$inputs.prop('checked', false);
};

const showNoBlogPost = (visible) =>
{
	el.blogs_none.setHidden(visible === false);
};

export default
{
	init,
};
