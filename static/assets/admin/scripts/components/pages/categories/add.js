'use strict';

let el = {};

let _isUpdating = false;

const init = () =>
{
	initDOM();
	initEvents();
};

const initDOM = () =>
{
	el = {};

	el.form = $('[data-form-category]');
	el.form_save = $('[data-submit-category]');
};

const initEvents = () =>
{
	el.form_save.on('click', function() { insertCategory($(this).attr('data-submit-category')); });
};

const insertCategory = (insertMode) =>
{
	if (lockUpdate() === true) return;
		lockUpdate(true);

	const formUrl = _Url.get('category_new', {}, false);
	const formData = new FormData(el.form.get(0));

	_Form.clearErrors();
	_Ajax.post(formUrl, formData, {
		loader_error: true,
		success: (data) => {
			const url = insertMode === 'edit' ? _Url.get('category_edit', { id: data.category.id }) : _Url.get('categories');
			const callback = () => _Notification.show({ message: 'The category has been created', status: 'success' });

			_Pjax.href(url, callback);
		},
		error: (error) => _Form.addErrors(error, {
			names: { 'en.url': 'langs.en.url', 'fr.url': 'langs.fr.url', 'en.name': 'langs.en.name', 'fr.name': 'langs.fr.name' },
			form: el.form,
		}),
		complete: (error) => {
			lockUpdate(false);
		},
	});
};

const lockUpdate = (isUpdating) =>
{
	if (isUpdating !== undefined) {
		_isUpdating = isUpdating;
		_Loader[isUpdating ? 'start' : 'stop']();
	}

	return _isUpdating;
};

export default
{
	init,
};
