'use strict';

let el = {};

const init = () =>
{
	initDOM();
	initEvents();
};

const initDOM = () =>
{
	el = {};

	el.categories_delete = $('[data-category-delete]');
};

const initEvents = () =>
{
	el.categories_delete.on('click', function() { deleteCategory($(this)); });
};

const deleteCategory = ($button) =>
{
	_Confirm.show('Do you want to delete this category?', () => {
		const categoryId = $button.attr('data-category-delete');
		const formUrl = _Url.get('category_delete', { id: categoryId }, false);

		_Form.clearErrors();
		_Ajax.post(formUrl, {}, {
			loader_error: true,
			success: (data) => {
				_Pjax.refresh(_Notification.show({ message: 'The category has been deleted', status: 'success' }));
			},
			error: (error) => _Form.addErrors(error, {
				form: el.form,
			}),
		});
	});
};

export default
{
	init,
};
