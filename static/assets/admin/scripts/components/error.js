'use strict';

let el = {};

const init = () =>
{
	initDOM();
	initBack();
};

const initDOM = () =>
{
	el = {};

	el.back_button = $('[data-back]');
};

const initBack = (userId) =>
{
	el.back_button.setHidden(history.length <= 2);
};

export default
{
	init,
};
