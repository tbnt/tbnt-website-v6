'use strict';

let el = {};

const init = () =>
{
	initDOM();
	initEvents();

	animStart();
};

const initDOM = () =>
{
	el = {};

	el.modal_logged_out = $('[data-modal="logged-out"]');
	el.modal_new_version = $('[data-modal="new-version"]');
};

const initEvents = () =>
{
	_Utils.addEvent('ajax.loader.start', () => _Loader.start());
	_Utils.addEvent('ajax.loader.stop', () => _Loader.stop());

	_Utils.addEvent('ajax.error.logged_out', () => showLoggedOutModal());
	_Utils.addEvent('ajax.error.new_version', () => showNewVersionModal());
	_Utils.addEvent('ajax.error.notification', (error) => _Notification.show({ message: error, status: 'danger' }));
};

const animEnd = (init = false) =>
{
	if (init === false)
		NProgress.set(.99);

	_Loader.stop(() => _Utils.triggerEvent('page.loaded'));

	setTimeout(() => NProgress.done(), 100);
};

const animStart = () =>
{
	NProgress.start();
};

const showApp = () =>
{
	animEnd();

	setTimeout(() => {
		$('[data-loader]').removeClass('active');

		setTimeout(() => $('[data-loader]').setHidden(true), 270);
	}, 200);
};

const showLoggedOutModal = () =>
{
	_Modal.show(el.modal_logged_out);
};

const showNewVersionModal = () =>
{
	_Modal.show(el.modal_new_version);
};

export default
{
	init,
	showApp,
	showLoggedOutModal,
	showNewVersionModal,
};

$(document).on('pjax:send', function() { animStart(); });
$(document).on('pjax:callback', function() { animEnd(); });
