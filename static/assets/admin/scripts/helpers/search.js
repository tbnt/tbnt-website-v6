'use strict';

const callback = () =>
{
	initEvents();
};

const initEvents = () =>
{
	$(document).on('click', '[data-field-search] [data-focus]', function() { focusInput($(this)); });
	$(document).on('click', '[data-field-search] [data-empty]', function() { emptyInput($(this)); });
	$(document).on('input', '[data-field-search] input', function() { updateInput($(this)); });
	$(document).on('keyup', '[data-field-search] input', function(e) { enterInput(e, $(this)); });
};

const focusInput = function($mixed)
{
	$mixed.parent('[data-field-search]').find('input').trigger('focus');
};

const emptyInput = function($mixed)
{
	$mixed.parent('[data-field-search]').find('input').val('').trigger('keyup').siblings('[data-field-empty]').addClass('uk-invisible');
};

const updateInput = function($input)
{
	$input.siblings('[data-field-empty]')[$input.val() === '' ? 'addClass' : 'removeClass']('uk-invisible');
};

const enterInput = function(e, $input)
{
	if (e.which !== 13)
		return;

	$input.siblings('[data-enter]').trigger('click');
};

const getValue = function($mixed)
{
	return _.trim($mixed.parent('[data-field-search]').find('input').val().toLowerCase());
};

export default
{
	focus: focusInput,
	empty: emptyInput,
	value: getValue,
};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
