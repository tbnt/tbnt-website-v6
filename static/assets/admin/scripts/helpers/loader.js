'use strict';

const start = () =>
{
	$('[data-loader]').addClass('active-loader').setHidden(false);

	NProgress.start();
};

const stop = (callback = () => '') =>
{
	$('[data-loader]').removeClass('active-loader');

	setTimeout(() => NProgress.done(), 100);
	setTimeout(() => { $('[data-loader]').setHidden(true); callback(); }, 270);
};

export default
{
	start,
	stop,
};
