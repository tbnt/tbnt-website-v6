'use strict';

const callback = () =>
{
	initEvents();
};

const initEvents = () =>
{
	$(document).on('click', '[data-confirm]', function(e) { confirmDialog($(this)); return _Utils.blockEvent(e); });
};

const confirmDialog = ($link) =>
{
	UIkit.modal.confirm($link.attr('data-confirm'), { stack: true }).then(() => _Pjax.href($link.attr('href'), $link.attr('pjax') === undefined), () => '');
};

const show = (notification, callback = () => '', error = (e) => '') =>
{
	UIkit.modal.confirm(notification, { stack: true }).then(callback).catch(error);
};

export default
{
	show,
};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
