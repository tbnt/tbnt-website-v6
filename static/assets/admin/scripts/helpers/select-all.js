'use strict';

const callback = () =>
{
	initEvents();
};

const initEvents = () =>
{
	$(document).on('change', '[data-select-all-item]', toggleItem);
	$(document).on('change', '[data-select-all]', toggleItemsGroup);
};

const emit = ($select, groupName) =>
{
	_Utils.triggerEvent('select-all.' + groupName, { $select: $select });

	if (groupName.indexOf('-') !== -1) {
		for (let i = 0, g = groupName.split('-'), c = g.length; i < c; i++)
			_Utils.triggerEvent('select-all.' + g[i] + '~', { $select: $select });
	}
};

const toggleItem = function (groupName)
{
	const $item = $(this);

	if (typeof groupName !== 'string')
		groupName = $item.attr('data-select-all-item');

	const $items = $('[data-select-all-item="' + groupName + '"]:visible');
	const $itemsChecked = $('[data-select-all-item="' + groupName + '"]:visible:checked');
	const $itemsGroup = $('[data-select-all="' + groupName + '"]');

	if ($itemsGroup.length > 0)
		$itemsGroup.prop('checked', $items.length === $itemsChecked.length);

	emit($item, groupName);
};

const toggleItemsGroup = function ()
{
	const $itemsGroup = $(this);

	const groupName = $itemsGroup.attr('data-select-all');
	const isChecked = $itemsGroup.is(':checked');

	const $items = $('[data-select-all-item="' + groupName + '"]:visible');

	if ($items.length > 0)
		$items.prop('checked', isChecked);

	emit($itemsGroup, groupName);
};

const activeGroup = (groupName, checked, visible = false) =>
{
	const $items = $('[data-select-all-item="' + groupName + '"]' + (visible === true ? '' : ':visible'));
	const $itemsGroup = $('[data-select-all="' + groupName + '"]');

	if ($items.length > 0)
		$items.prop('checked', checked);

	if ($itemsGroup.length > 0)
		$itemsGroup.prop('checked', checked);

	emit($itemsGroup, groupName);
};

const refresh = (groupName) =>
{
	const $items = $('[data-select-all-item="' + groupName + '"]:visible');
	const $itemsChecked = $('[data-select-all-item="' + groupName + '"]:visible:checked');
	const $itemsGroup = $('[data-select-all="' + groupName + '"]');

	if ($itemsGroup.length > 0)
		$itemsGroup.prop('checked', $items.length === $itemsChecked.length);

	emit($itemsGroup, groupName);
};

const isItemChecked = (groupName) =>
{
	return $('[data-select-all-item="' + groupName + '"]:checked').length > 0;
};

const isItemsGroupChecked = (groupName) =>
{
	return $('[data-select-all="' + groupName + '"]').prop('checked');
};

const setCount = (groupName, count) =>
{
	if (count === false)
		$('[data-select-all="' + groupName + '"]').siblings('label').find('[data-count]').setHidden(true);
	else
		$('[data-select-all="' + groupName + '"]').siblings('label').find('[data-count]').setHidden(false).find('[data-count-text]').text(count);
};

export default
{
	activeGroup,
	refresh,
	isItemChecked,
	isItemsGroupChecked,
	setCount,
};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
