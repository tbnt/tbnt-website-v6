'use strict';

let _fields = {};

const _errorConfig = {
	'.card-module': {
		options: {
			'uk-card-form-danger': true,
		},
		action: 'after',
	},
	'[data-form-upload-error]': {
		options: {
			'uk-margin-small-bottom': '!hasClass',
		},
		action: 'after',
	},
	'[data-error-after]': {
		action: 'after',
	},
	'[data-error-append]': {
		action: 'append',
	},
	'[data-error-prepend]': {
		action: 'prepend',
	},
	default: {
		action: 'after',
	},
};

const _errorOptions = {
	names: {},
	alert: true,
	alert_message: 'Unknown error',
	alert_status: 'danger',
	alert_pos: 'top-center',
	alert_timeout: 5000,
	alert_errors: true,
	offset: true,
	debug: false,
	form: null,
};

const clearErrors = (names, offset = 0) =>
{
	if (names === undefined)
		names = _.keys(_fields);

	if (Array.isArray(names) === false)
		names = [names];

	for (let i = 0, ci = names.length; i < ci; i++) {
		const name = names[i];
		const field = _fields[name];

		if (field === undefined)
			continue;

		// Remove error
		$('[data-error-name="' + name + '"]').eq(offset).remove();

		// Reset error container class
		for (let j = 0, kj = _.keys(field.options), cj = kj.length; j < cj; j++) {
			if (field.options[kj[j]] === true) field.$field.removeClass(kj[j]);
		}

		delete _fields[name];
	}
};

const getErrors = (error) =>
{
	if (_Utils.isObject(error, true) === false)
		return { error: error };

	if (error.responseJSON !== undefined)
		return error.responseJSON;

	if (error.responseText !== undefined)
		return { error: _Trans.get('errors.unknown') };

	return error;
};

const addErrors = (error, errorOptions = {}) =>
{
	errorOptions = _.assign({}, _errorOptions, errorOptions);
	error = getErrors(error);

	if (Array.isArray(error) === true)
		error = error[0];

	if (errorOptions.debug === true)
		console.warn('Form error:', error, errorOptions);

	// Proceed errors
	const format = errorOptions.names;
	let allErrorsMessages = [];

	_.map(error || {}, (messages, name) => {
		name = format[name] || name;

		let fieldName = name;
		let fieldOffset = 0;

		// Fix array name
		if (String(name).indexOf('.') !== -1) {
			let names = name.split('.');
				names[0] = format[names[0]] || names[0];

			fieldName = names.shift();

			for (let i = 0, c = names.length; i < c; i++) {
				const tempName = names[i];

				if (isNaN(tempName) === true || errorOptions.offset === false) {
					fieldName += '[' + tempName + ']';
				}
				else {
					fieldName += '[]';
					fieldOffset += Number(tempName);
				}
			}
		}

		// Get field
		let $field = errorOptions.form !== null && $(errorOptions.form).length !== 0
			? $(errorOptions.form).find('[name="' + fieldName + '"]').eq(fieldOffset)
			: $('[name="' + fieldName + '"]').eq(fieldOffset);

		// Find error container
		let options = {};
		let parentClass = '';

		for (let i = 0, ki = _.keys(_errorConfig), ci = ki.length; i < ci; i++) {
			parentClass = ki[i];

			if (parentClass !== 'default' && $field.parents(parentClass).length === 0)
				continue;

			if (parentClass !== 'default')
				$field = $field.parents(parentClass).eq(0);

			for (let j = 0, kj = _.keys(_errorConfig[parentClass].options), cj = kj.length; j < cj; j++) {
				const fieldClass = kj[j];
				const option = _errorConfig[parentClass].options[fieldClass];

				if ($()[option] !== undefined)
					options[fieldClass] = option.indexOf('!') === 0
						? $field[option.substring(1)](fieldClass) === false
						: $field[option](fieldClass) === true;
				else
					options[fieldClass] = Boolean(option);
			}

			break;
		}

		// Clear old error
		clearErrors(fieldName, fieldOffset);

		// Format errors messages
		const errorsMessages = _.uniq(Array.isArray(messages) ? messages : [messages]).join('<br>');

		if (errorOptions.alert_errors === true || errorOptions.alert_errors === fieldName)
			allErrorsMessages.push(errorsMessages);

		// Add error class to error container
		try {
			$field.addClass(_.keys(_errorConfig[parentClass].options || []).join(' '))[_errorConfig[parentClass].action](
				'<p class="uk-margin-small uk-text-danger" data-error-name="' + fieldName + '">' +
					errorsMessages +
				'</p>'
			);
		}
		catch (e) {}

		// Save field for future reset
		_fields[fieldName] = { $field, options };

		if (errorOptions.debug === true)
			console.warn('_Field error:', fieldName, _fields[fieldName]);
	});

	// Alert error
	if (errorOptions.alert === true) {
		_Notification.show({
			message: errorOptions.alert_errors && allErrorsMessages.length ? allErrorsMessages.join('<br>') : errorOptions.alert_message,
			status: errorOptions.alert_status,
			pos: errorOptions.alert_pos,
			timeout: errorOptions.alert_timeout,
		});
	}
};

export default
{
	clearErrors,
	addErrors,
	getErrors,
};
