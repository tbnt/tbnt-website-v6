'use strict';

let _notifications = {};
let _notificationIdLast = 0;

const callback = () =>
{
	initEvents();
};

const initEvents = () =>
{
	$(document).on('click', '[data-notification-id]', function() { hide($(this)); });
};

const show = function(notification)
{
	notification = UIkit.notification(notification);

	const notificationId = _notificationIdLast += 1;
	const notificationEl = $('.uk-notification-message:not([data-notification-id])');

	notificationEl.attr('data-notification-id', notificationId);

	_notifications[notificationId] = { el: notificationEl, notification };

	return notificationEl;
};

const hide = function($notification)
{
	const notificationId = Number($notification.attr('data-notification-id'));
	const notification = _notifications[notificationId];

	if (notification === undefined)
		return;

	notification.notification.close();
};

export default
{
	show,
	hide,
};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
