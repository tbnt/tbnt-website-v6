'use strict';

const callback = () =>
{
	initEvents();
};

const initEvents = () =>
{
	$(document).on('click', function(e) { hide(e); });
};

const hide = function(e)
{
	const $target = $(e.target);
	const isInDropdown = $target.closest('[uk-dropdown]').length !== 0
		|| $target.next('[uk-dropdown]').length !== 0
		|| $target.closest('li').find('.uk-navbar-dropdown').length !== 0;

	if (isInDropdown === true)
		return;

	hideAll();
};

const hideAll = function()
{
	for (let i = 0, $dropdowns = $('[uk-dropdown]'), c = $dropdowns.length; i < c; i++) {
		const uikitDropdown = UIkit.dropdown($dropdowns.eq(i));

		if (uikitDropdown.isActive()) {
			uikitDropdown.delayHide = 0;
			uikitDropdown.hide();
		}
	}

	$('[uk-navbar]').find('.uk-navbar-dropdown.uk-open').siblings('a').trigger('click');
};

export default
{
	hideAll,
};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
