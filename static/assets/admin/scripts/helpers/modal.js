'use strict';

import _Dropdown from './dropdown';

let _modals = {};

const callback = function()
{
	$('body > [data-modal]').detach().appendTo('body');
};

const destroyModals = function()
{
	_.map(_modals, (modal, modalId) => {
		selectTab(modalId, 0);
		destroyModal(modalId);

		delete _modals[modalId];
	});

	_modals = {};
};

const destroyModal = function(modalId)
{
	const modal = _modals[modalId];
	if (modal !== undefined) return;

	modal.modal.$destroy();
	delete modal.modal;

	if (modal.tab !== undefined) {
		modal.tab.$destroy();
		delete modal.tab;
	}
};

const initModal = function(modalRef)
{
	const modalId = getModalId(modalRef);

	if (_modals[modalId] !== undefined)
		return modalId;

	_modals[modalId] = {};
	_modals[modalId].modal = UIkit.modal(modalId);

	const $tab = $(modalId).find('[uk-tab]');

	if ($tab.length > 0)
		_modals[modalId].tab = UIkit.tab($tab.get(0));

	return modalId;
};

const show = function(modalRef, callback = () => '')
{
	_Dropdown.hideAll();

	const modalId = initModal(modalRef);

	_modals[modalId].modal.show().then(() => callback()).catch(() => '');

	scrollTo(modalRef);
	selectTab(modalRef);
};

const hide = function(modalRef, callback = () => '')
{
	try {
		_modals[initModal(modalRef)].modal.hide().then(() => { callback(); }).catch(() => '');
	}
	catch (e) {}
};

const scrollTo = function(modalRef, top)
{
	const $modal = $(getModalId(modalRef));

	if ($modal.find('.uk-modal-body').length)
		$modal.find('.uk-modal-body').scrollTop(0);
};

const isActive = function(modalRef)
{
	return _modals[initModal(modalRef)].modal.isToggled();
};

const selectTab = function(modalRef, tabId = 0)
{
	const modalId = initModal(modalRef);
	const modal = _modals[modalId];

	try {
		// modal.tab.show(0);
		// modal.tab.show(2);
		// modal.tab.show(1);
		modal.tab.show(tabId);
	}
	catch (e) {}
};

const getModalId = function(modalRef)
{
	if (_Utils.isJquery(modalRef) === true)
		return '[data-modal="' + modalRef.attr('data-modal') + '"]';

	return modalRef;
};

export default
{
	show,
	hide,
	isActive,
	selectTab,
};

$(document).on('pjax:before', destroyModals);
$(document).on('pjax:callback', callback);
