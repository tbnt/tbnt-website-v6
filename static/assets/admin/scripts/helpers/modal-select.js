'use strict';

const _ModalSelect = function(config)
{
	let el = {};

	let _mixitup = {
		active: null,
		inactive: null,
	};

	let _config = _.assign({
		mixitup: '',
		parent: '',
		modal: '',
		url: '',
		// items: {},
		one: false,
		onChange: () => '',
		onCancel: () => '',
		onSelect: () => '',
	}, config);

	let _items = {};
	let _isOpened = false;

	const init = () =>
	{
		if (_config.modal === undefined || _config.modal === '')
			return emitError('ModalSelect missing "modal" parameter:');

		if (_config.url === undefined || _config.url === '')
			return emitError('ModalSelect missing "url" parameter:');

		if (_config.parent === undefined || _config.parent === '')
			return emitError('ModalSelect missing "parent" parameter:');

		if ($('[data-modal="' + _config.modal + '"]').length === 0)
			return emitError('ModalSelect undefined "modal" element:');

		initDOM();
		initEvents();

		return true;
	};

	const destroy = () =>
	{
		_.map(el, (item, itemKey) => delete el[itemKey]);
		el = {};

		destroyMixitup('active');
		destroyMixitup('inactive');
	};

	const destroyMixitup = (groupType) =>
	{
		if (_config.mixitup === undefined)
			return;

		if (_mixitup[groupType] !== null)
			_mixitup[groupType].destroy();
	};

	const initDOM = () =>
	{
		el = {};

		el.modal = $('[data-modal="' + _config.modal + '"]');

		el.search = el.modal.find('[data-modal-select-search]');
		el.search_none = el.modal.find('[data-modal-select-search-none]');

		el.loader = el.modal.find('[data-modal-select-loader]');

		el.active_group = el.modal.find('[data-modal-select-selected]');
		el.inactive_group = el.modal.find('[data-modal-select-unselected]');

		el.cancel = el.modal.find('[data-modal-select-cancel]');
		el.select = el.modal.find('[data-modal-select-select]');
	};

	const initEvents = () =>
	{
		el.search.on('keyup', function() { triggerAction(search, $(this).val()); });

		el.active_group.on('change', 'input', function() { triggerAction(toggleItem, $(this)); });
		el.inactive_group.on('change', 'input', function() { triggerAction(toggleItem, $(this)); });

		el.cancel.on('click', function() { emitCancel(); });
		el.select.on('click', function() { emitSelect(); });
	};

	const triggerAction = (callback, value) =>
	{
		if (_isOpened === false)
			return;

		callback(value);
	};

	const toggleItem = ($select) =>
	{
		const $item = $select.parents('[' + _config.parent + ']');
		const isSelected = $select.is(':checked');

		const itemId = Number($item.attr(_config.parent));
		const item = _items[itemId];

		if (item === undefined)
			return;

		// Change group
		if (isSelected === true) moveToActiveGroup(item);
		else moveToInactiveGroup(item);

		// Emit item changed
		emitChange(item, isSelected);

		// Update
		refreshGroups();
		searchRefresh();
	};

	const moveToActiveGroup = (item) =>
	{
		if (_config.one === false) {
			item._el.detach().appendTo(el.active_group);

			_Checkbox.set(item._el.find('input'), true);

			el.active_group.parent().setHidden(false);
			el.inactive_group.parent().setHidden(el.inactive_group.find('[' + _config.parent + ']').length === 0);
		}

		// Update item
		if (_items[item.id] !== undefined)
			_items[item.id]._selected = true;
	};

	const moveToInactiveGroup = (item) =>
	{
		item._el.detach().appendTo(el.inactive_group);

		_Checkbox.set(item._el.find('input'), false);

		el.inactive_group.parent().setHidden(false);
		el.active_group.parent().setHidden(el.active_group.find('[' + _config.parent + ']').length === 0);

		// Update item
		if (_items[item.id] !== undefined)
			_items[item.id]._selected = false;
	};

	const refreshGroups = () =>
	{
		refreshGroup('active', el.active_group);
		refreshGroup('inactive', el.inactive_group);

		el.active_group.parent().setHidden(el.active_group.find('[' + _config.parent + ']').length === 0);
		el.inactive_group.parent().setHidden(el.inactive_group.find('[' + _config.parent + ']').length === 0);
	};

	const refreshGroup = (groupType, group) =>
	{
		destroyMixitup(groupType);

		_mixitup[groupType] = mixitup(group.get(0), { load: { sort: _config.mixitup } });
	};

	const getItem = (itemId) =>
	{
		const $itemActive = el.active_group.find('[' + _config.parent + '="' + itemId + '"]');
		if ($itemActive.length !== 0) return $itemActive;

		const $itemInactive = el.inactive_group.find('[' + _config.parent + '="' + itemId + '"]');
		if ($itemInactive.length !== 0) return $itemInactive;
	};

	const getActive = () =>
	{
		return _.filter(_items, (item) => { if (Boolean(item._selected) === true) return item; });
	};

	const setActive = (itemsIds = [], filters = {}) =>
	{
		itemsIds = _Utils.mapNumber(itemsIds);

		_.map(_items, (item) => {
			for (let i = 0, k = _.keys(filters), c = k.length; i < c; i++) {
				const key = k[i];

				if (item[key] === undefined || _Utils.initArray(filters[key]).indexOf(item[key]) > -1)
					continue;

				item._hidden = true;
				item._el.setHidden(true);

				moveToInactiveGroup(item);

				return;
			}

			item._hidden = false;
			item._el.setHidden(false);

			if (itemsIds.indexOf(item.id) !== -1 && Boolean(item._selected) === false) {
				moveToActiveGroup(item);
			}
			else if (itemsIds.indexOf(item.id) === -1 && Boolean(item._selected) === true) {
				moveToInactiveGroup(item);
			}
		});

		// Update
		refreshGroups();
		searchShouldShowNone();
	};

	const search = (search) =>
	{
		search = _.trim(search.toLowerCase());

		_.map(_items, (item, itemId) => {
			if (item._hidden !== true) item._el.setHidden(search !== '' && String(item._search || '').indexOf(search) === -1 && Boolean(item._selected) === false);
		});

		searchShouldShowNone();
	};

	const searchRefresh = () =>
	{
		search(el.search.val());
	};

	const searchEmpty = () =>
	{
		el.search.val('');

		searchRefresh();
	};

	const searchShouldShowNone = () =>
	{
		el.search_none.setHidden(el.inactive_group.find('[' + _config.parent + ']:visible').length > 0);
	};

	const show = (activeIds = -1, filters = {}) =>
	{
		searchEmpty();

		el.active_group.empty();
		el.inactive_group.empty();
		el.search_none.setHidden(true);

		_Modal.show(el.modal);
		_isOpened = true;

		if (_.keys(_items).length === 0) {
			el.loader.setHidden(false);

			_Ajax.post(_config.url, {
				filters,
			}, {
				success: (data) => {
					displayItems(data.items, activeIds, filters);
				},
				error: (error) => _Form.addErrors(error, {
					form: el.modal,
				}),
				complete: (error) => {
					el.loader.setHidden(true);
				},
			});
		}
		else {
			displayItems(_items, activeIds, filters);
		}
	};

	const displayItems = (items, activeIds = -1, filters = {}) =>
	{
		items = _.values(items);

		_items = {};

		for (let i = 0, c = items.length; i < c; i++) {
			let item = items[i];
				item._el = $(item._html || '<div />');
				item._selected = false;

			_items[item.id] = item;
			el.inactive_group.append(item._el);
		}

		if (activeIds !== -1)
			setActive(activeIds, filters);

		refreshGroups();
		searchShouldShowNone();
	};

	const hide = () =>
	{
		_Modal.hide(el.modal);
		_isOpened = false;
	};

	const onChange = (callback) =>
	{
		_config.onChange = callback;
	};

	const onCancel = (callback) =>
	{
		_config.onCancel = callback;
	};

	const onSelect = (callback) =>
	{
		_config.onSelect = callback;
	};

	const emitError = (message) =>
	{
		return console.warn(message, _config) || false;
	};

	const emitChange = (item, active) =>
	{
		_config.onChange(item, active);
	};

	const emitCancel = () =>
	{
		_config.onCancel(getActive());

		hide();
	};

	const emitSelect = () =>
	{
		_config.onSelect(getActive());

		hide();
	};

	return init() && {
		refreshGroups,
		getActive,
		setActive,
		show,
		hide,
		search,
		searchEmpty,
		onChange,
		onCancel,
		onSelect,
	};

	$(document).on('pjax:send', destroy);
};

export default _ModalSelect;
