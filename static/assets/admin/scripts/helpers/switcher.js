'use strict';

let el = {};

let _switchersPositions = {};

const callback = () =>
{
	initDOM();
	initEvents();
};

const refresh = () =>
{
	initSwitcher();
};

const initDOM = () =>
{
	el = {};
	el.switchers = $('[data-switcher-id]');
};

const initEvents = () =>
{
	_.map(el.switchers, (switcher) => UIkit.tab(switcher).connects.on('shown', (e, tab) => updateTab(tab)));
};

const initSwitcher = () =>
{
	for (let i = 0, k = _.keys(_switchersPositions), c = k.length; i < c; i++) {
		const $switcher = $('[data-switcher-id="' + k[i] + '"]');

		if ($switcher.length > 0)
			selectTab($switcher.get(0), _switchersPositions[k[i]]);
	}
};

const updateTab = (tab) =>
{
	const switcherId = tab.$el.attr('data-switcher-id');

	if (switcherId !== undefined)
		_Utils.triggerEvent('switcher.show', { tab_id: switcherId, index: _switchersPositions[switcherId] = tab.toggles.filter('.uk-active').index() });
};

const selectTab = (switcher, tabId = 0) =>
{
	switcher = UIkit.tab(switcher);

	try {
		switcher.show(tabId);
		switcher.$destroy();
	}
	catch (e) {}
};

export default
{
	selectTab,
};

$(document).ready(callback);
$(document).on('pjax:refresh', refresh);
$(document).on('pjax:callback', callback);
