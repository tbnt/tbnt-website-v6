'use strict';

// const _licence = 'YG-10xB-7zomqmtD3ali==';
const _licence = 'huh1mpC8bnof1B-9ji1==';

const _toolbarButtons = [
	'bold',
	'italic',
	'underline',
	'strikeThrough',
	'subscript',
	'superscript',
	'paragraphFormat',
	'quote',
	'formatOL',
	'formatUL',
	'|',
	'insertImage',
	'insertFile',
	'insertLink',
	'insertHR',
	'insertTable',
	'|',
	'selectAll',
	'clearFormatting',
	'undo',
	'redo',
	'html',
	// '|',
	// 'fullscreen',
];

const _defaultOptions = {
	toolbarButtons: _toolbarButtons,
	toolbarButtonsMD: _toolbarButtons,
	toolbarButtonsSM: _toolbarButtons,
	toolbarButtonsXS: _toolbarButtons,

	paragraphFormat: {
		N: 'Normal',
		H1: 'Heading 1',
		H2: 'Heading 2',
	},

	imageUploadURL: 'admin/upload/image',
	imageUploadMethod: 'POST',
	imageAllowedTypes: [
		'jpeg',
		'jpg',
		'png',
	],

	fileAllowedTypes: [
		'application/pdf',
	],
	fileMaxSize: 1024 * 1024 * 100,
	fileUploadURL: 'admin/upload/file',
	fileUploadMethod: 'POST',

	linkAlwaysNoFollow: true,
	linkEditButtons: [
		'linkOpen',
		'linkEdit',
		'linkRemove',
	],
	linkInsertButtons: [],
	linkMultipleStyles: false,
	linkStyles: [],

	heightMin: 200,
	shortcutsEnabled: [],
	spellcheck: true,
	toolbarSticky: false,
	spellcheck: false,
	scaytAutoload: false,

	key: _licence,

	onImageUploaded: (e, editor, response) => {},
	onImageError: (e, editor, error, response) => {},
	onFileUploaded: (e, editor, response) => {},
	onFileError: (e, editor, error, response) => {},
};

const initFroala = function($el, options = {})
{
	if ($el.length === 0)
		return emitError('Froala init error: $el not exists', $el);

	options = _.assign({}, _defaultOptions, options);

	for (var i = $el.length - 1; i >= 0; i--) {
		const $textarea = $el.eq(i);

		if ($textarea[0].tagName.toLowerCase() !== 'textarea') {
			emitError('Froala init error: $el.eq(' + i + ') is not type textarea', $textarea);
			continue;
		}

		$textarea.froalaEditor(options)
			.on('froalaEditor.image.uploaded', options.onImageUploaded)
			.on('froalaEditor.image.error', options.onImageError)
			.on('froalaEditor.file.uploaded', options.onFileUploaded)
			.on('froalaEditor.file.error', options.onFileError);
	}
};

const emitError = function()
{
	return console.warn(...arguments) && false;
};

export default
{
	init: initFroala,
};
