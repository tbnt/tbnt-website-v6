'use strict';

const callback = () =>
{
	initEvents();
};

const initEvents = () =>
{
	$(document).on('click', '[data-filters] *', function() { toggleFilter($(this)); });
};

const toggleFilter = function ($input)
{
	const $filter = $input.closest('[data-filters]');

	$filter.prev('button')[$filter.find('input:checked').length ? 'addClass' : 'removeClass']('uk-button-active');
};

export default {};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
