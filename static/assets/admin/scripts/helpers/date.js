'use strict';

const formatString = (dateString, formatOut, formatIn = 'YYYY-MM-DD') =>
{
	return moment(dateString, formatIn).format(_Trans.get('dates.js.' + formatOut));
};

const formatDate = (date, formatOut) =>
{
	return moment(date).format(_Trans.get('dates.js.' + formatOut));
};

const format = (date, formatOut) =>
{
	return moment(date).format(formatOut);
};

const now = (formatOut = 'YYYY-MM-DD HH:mm:ss') =>
{
	return format(new Date(), formatOut);
};

export default
{
	formatString,
	formatDate,
	format,
	now,
};
