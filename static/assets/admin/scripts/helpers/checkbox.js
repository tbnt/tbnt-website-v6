'use strict';

const callback = () =>
{
	initEvents();
};

const initEvents = () =>
{
	$(document).on('click', '[data-card-checkbox] label', function() { toggleLabel($(this)); });
	$(document).on('click', '[data-card-checkbox] input', function() { toggleInput($(this)); });
};

const toggleLabel = function($label)
{
	const $input = $label.siblings('input');
	const $labelSiblings = $label.siblings('label');

	$label.setHidden($label.is(':visible'));
	$labelSiblings.setHidden($labelSiblings.is(':visible'));

	$input.prop('checked', $input.prop('checked') === false);
	$input.trigger('change');
};

const toggleInput = function($input)
{
	const $labelActive = $input.siblings('label').first();
	const $labelInactive = $input.siblings('label').last();
	const isChecked = $input.is(':checked');

	$labelActive.setHidden(isChecked === false);
	$labelInactive.setHidden(isChecked);
};

const setInput = function($input, checked)
{
	$input.prop('checked', checked);

	toggleInput($input);
};

export default
{
	toggleLabel,
	toggleInput,
	set: setInput,
};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
