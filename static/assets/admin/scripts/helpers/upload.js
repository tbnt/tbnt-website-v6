'use strict';

const callback = () =>
{
	initEvents();
};

const initEvents = () =>
{
	$(document).on('change', '[data-upload-input]', function() { loadFile($(this)); });
	$(document).on('click', '[data-upload-delete]', function() { deleteFile($(this)); });
};

const loadFile = ($input) =>
{
	const input = $input.get(0);

	if (input.files.length === 0 || input.files[0] === undefined)
		return;

	const file = input.files[0];
	let reader = new FileReader();

	reader.onerror = (e) => console.warn('File load error', e);
	reader.onload = (e) => displayFile($input, e.target.result);
	reader.readAsDataURL(file);
};

const displayFile = ($input, src) =>
{
	$input = _Utils.isJquery($input) === true ? $input
		: $('[data-upload-file="' + $input + '"]');

	if ($input.attr('data-upload-file') !== undefined)
		$input = $input.find('[data-upload-input]');

	const $uploadBox = $input.parents('[data-upload-file]');
	const $previewBox = $uploadBox.next('[data-upload-preview]');

	$previewBox.find('[data-upload-file-id]').remove();
	$previewBox.prepend('<img class="uk-display-block" src="' + src + '" data-upload-file-id />');

	$previewBox.setHidden(false);
	$uploadBox.setHidden($previewBox.find('[data-upload-delete]').length !== 0);

	$input.siblings('span').text('Replace file');
};

const deleteFile = ($button) =>
{
	_Confirm.show('Do you want to delete this image?', () => {
		const $previewBox = $button.parents('[data-upload-preview]');
		const $uploadBox = $previewBox.prev('[data-upload-file]');
		const fileId = Number($previewBox.childrenAttr('[data-upload-file-id]'));

		$uploadBox.setHidden(false);
		$previewBox.setHidden(true);

		$previewBox.find('[data-upload-file-id]').remove();

		if (isNaN(fileId) === false && fileId !== 0)
			$previewBox.after('<input type="hidden" name="deleted_' + $previewBox.attr('data-upload-preview') + '[]" value="' + fileId + '" />');

		const $input = $uploadBox.find('[data-upload-input]');

		_Utils.emptyInputFile($input);
		$input.siblings('span').text('Upload file');
	});
};

const initDrop = (uploadName, options = {}) =>
{
	const $uploadBox = _Utils.isJquery(uploadName) === true ? uploadName
		: $('[data-upload-file="' + uploadName + '"]');

	if ($uploadBox.length === 0)
		return console.warn('Upload init error: undefined "upload" element', uploadName, options);

	// Init default options
	options = _.assign({
		multiple: false,
		params: {
			'_token': Laravel.token,
		},
		onBefore: () => '',
		onSuccess: () => '',
		onError: () => '',
		onComplete: () => '',
		beforeAll: () => {
			if (options.debug === true)
				console.log('Upload start:', options);

			options.onBefore();
		},
		error: (response) => {
			if (options.debug === true)
				console.warn('Upload error:', options, response);

			options.onError(response);
		},
		complete: (response) => {
			if (options.debug === true)
				console.log('Upload complete:', options, response);

			if (options.loader !== false)
				_Loader.stop();

			if (_Ajax.isResponseSuccess(response) === true) options.onSuccess(response);
			else options.onError(response);
		},
		completeAll: (response) => {
			options.onComplete(response);
		}
	}, options);

	// Init progress bar
	if (options.bar !== false) {
		let bar = $uploadBox.find('[data-upload-progress]').get(0);

		options = _.assign(options, {
			load: (e) => {
			},
			loadStart: (e) => {
				bar.removeAttribute('hidden');
				bar.max = e.total;
				bar.value = e.loaded;
			},
			progress: (e) => {
				bar.max = e.total;
				bar.value = e.loaded;
			},
			loadEnd: (e) => {
				bar.max = e.total;
				bar.value = e.loaded;
			},
			completeAll: (response) => {
				bar.setAttribute('hidden', 'hidden');

				options.onComplete(response);
			}
		});
	}

	// Debug
	if (options.debug === true)
		console.log('Upload init:', options);

	// Start loader
	if (options.loader !== false)
		_Loader.start();

	// Start uploader
	UIkit.upload($uploadBox, options);
};

export default
{
	displayFile,
	initDrop,
};

$(document).ready(callback);
$(document).on('pjax:callback', callback);
