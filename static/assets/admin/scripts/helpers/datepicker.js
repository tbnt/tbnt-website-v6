'use strict';

const callback = () =>
{
	_Utils.addEvent('laravel.config.ready', () => initDatepicker());
};

const initDatepicker = () =>
{
	for (let i = 0, k = _.keys(Flatpickr.l10ns), c = k.length; i < c; i++)
		Flatpickr.l10ns[k[i]].firstDayOfWeek = 1;

	initDate('input[type="date"]');
	initTime('input[type="time"]');
};

const initDate = (selector, options = {}) =>
{
	if (_Utils.isMobile.any() === true)
		return $(selector).val(options.defaultDate || new Date());

	flatpickr(selector, _.assign({
		locale: Laravel.lang.code,
		altFormat: _Trans.get('dates.fp.lmd'),
		altInput: true,
		onOpen: () => { if (options.onOpen) options.onOpen(); },
		onChange: () => { if (options.onChange) options.onChange(); },
		onClose: () => { if (options.onClose) options.onClose(); },
		onMonthChange: () => { if (options.onMonthChange) options.onMonthChange(); },
		onYearChange: () => { if (options.onYearChange) options.onYearChange(); },
	}, options));
};

const initTime = (selector, options = {}) =>
{
	let date = options.defaultDate || new Date();

	if (_Utils.isString(options.defaultDate) === false)
		date = _Utils.leadingString(date.getHours(), 2) + ':' + _Utils.leadingString(date.getMinutes(), 2)

	return $(selector).attr('placeholder', _Trans.get('dates.time_format')).val(date);
};

export default
{
	initDate,
	initTime,
};

$(document).ready(callback);
$(document).on('pjax:callback', initDatepicker);
