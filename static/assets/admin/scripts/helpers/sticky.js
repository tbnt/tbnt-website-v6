'use strict';

import Breakdown from './breakdown';

const _Sticky = function(config)
{
	let el = {};

	let _isSticky = false;
	let _parentHeight = 0;

	let _config = _.assign({
		parent: null,
		content: null,
		top: 0,
		bottom: false,
		mobile: false,
		mobileBreakdown: ['mobile', 'tablet'],
		parent_class: '',
		content_class: '',
		debug: false,
		zIndex: 8000,
		onChange: () => '',
	}, config);

	const init = () =>
	{
		if (_Utils.isJquery($(_config.parent)) === false || $(_config.parent).length === 0)
			return _Utils.returnLogWarn('Sticky invalid "parent" parameter:', _config);

		if (_Utils.isJquery($(_config.content)) === false || $(_config.content).length === 0)
			return _Utils.returnLogWarn('Sticky invalid "content" parameter:', _config);

		initDOM();
		initEvents();
		initSticky();

		return true;
	};

	const initDOM = () =>
	{
		el = {};

		el.window = $(window);
		el.document = $(document);

		el.sticky = $(_config.parent);
		el.sticky_content = $(_config.content);

		_parentHeight = el.sticky.height();
	};

	const initEvents = () =>
	{
		el.window.on('resize', onResize);
		el.document.on('scroll', function() { onScroll(); });

		_Utils.addEvent('breakdown.updated', onScroll);
	};

	const initSticky = () =>
	{
		onResize();
	};

	const onResize = () =>
	{
		if (_config.bottom !== false)
			_config.top = el.window.height() - _config.bottom - _parentHeight;

		onScroll();
	};

	const onScroll = () =>
	{
		if (_config.mobile === false && isBreakdownMobile() === true)
			return unsetSticky();

		const scrollTop = el.document.scrollTop();

		const stickyPosTop = el.sticky.offset().top - _config.top;
		const stickyIsFixed = Boolean(Number(el.sticky_content.attr('data-is-fixed')));

		if (scrollTop >= stickyPosTop) {
			if (stickyIsFixed === false) setSticky();
		}
		else {
			if (stickyIsFixed === true) unsetSticky();
		}

		if (_config.debug === true)
			console.log('Sticky scroll:', { stickyPosTop, scrollTop, stickyIsFixed });
	};

	const setSticky = () =>
	{
		if (_isSticky === true)
			unsetSticky();

		el.sticky.css({
			height: el.sticky.outerHeight(true),
		}).addClass(_config.parent_class);

		let values = {
			top: _config.top,
			left: el.sticky_content.offset().left,
			width: el.sticky_content.outerWidth(),
			height: el.sticky_content.outerHeight(),
		};

		if (_Utils.isNavigator.ieEdge() === true)
			values = _.mapValues(values, (value) => Math.ceil(value));

		el.sticky_content.css(_.assign({
			'position': 'fixed',
			'z-index': _config.zIndex,
		}, values)).attr('data-is-fixed', 1).addClass(_config.content_class);

		_isSticky = true;
	};

	const unsetSticky = () =>
	{
		if (_isSticky === false)
			return;

		el.sticky.css({
			height: '',
		}).removeClass(_config.parent_class);

		el.sticky_content.css({
			position: '',
			top: '',
			left: '',
			width: '',
			height: '',
			'z-index': '',
		}).attr('data-is-fixed', 0).removeClass(_config.content_class);

		_isSticky = false;
	};

	const isBreakdownMobile = () =>
	{
		return Breakdown.is(_config.mobileBreakdown);
	};

	return init() && {};
};

export default _Sticky;
