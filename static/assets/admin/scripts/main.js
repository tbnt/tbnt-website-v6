'use strict';

// Modules
import _Ajax from '../../utils/js/ajax';
import _App from '../../utils/js/app';
import _Pjax from '../../utils/js/pjax';
import _Scripts from '../../utils/js/scripts';
import _Trans from '../../utils/js/trans';
import _Url from '../../utils/js/url';
import _Utils from '../../utils/js/utils';

// Helpers
import _Checkbox from './helpers/checkbox';
import _Confirm from './helpers/confirm';
import _Date from './helpers/date';
import _Datepicker from './helpers/datepicker';
import _Dropdown from './helpers/dropdown';
import _Filter from './helpers/filter';
import _Form from './helpers/form';
import _Froala from './helpers/froala';
import _Loader from './helpers/loader';
import _Modal from './helpers/modal';
import _ModalSelect from './helpers/modal-select';
import _Notification from './helpers/notification';
import _Pagination from './helpers/pagination';
import _Search from './helpers/search';
import _SelectAll from './helpers/select-all';
import _Switcher from './helpers/switcher';
import _Upload from './helpers/upload';

// Components
import App from './components/app';
import Menu from './components/menu';
import Pages from './components/pages';

// Pages
import Blogs from './components/pages/blogs';
import BlogAdd from './components/pages/blogs/add';
import BlogEdit from './components/pages/blogs/edit';
import BlogView from './components/pages/blogs/view';

import Categories from './components/pages/categories';
import CategoryAdd from './components/pages/categories/add';
import CategoryEdit from './components/pages/categories/edit';

import Settings from './components/pages/settings';

// Make modules accessible
_Utils.access(_Ajax, '_Ajax');
_Utils.access(_App, '_App');
_Utils.access(_Checkbox, '_Checkbox');
_Utils.access(_Confirm, '_Confirm');
_Utils.access(_Date, '_Date');
_Utils.access(_Datepicker, '_Datepicker');
_Utils.access(_Dropdown, '_Dropdown');
_Utils.access(_Form, '_Form');
_Utils.access(_Froala, '_Froala');
_Utils.access(_Loader, '_Loader');
_Utils.access(_Modal, '_Modal');
_Utils.access(_ModalSelect, '_ModalSelect');
_Utils.access(_Notification, '_Notification');
_Utils.access(_Pagination, '_Pagination');
_Utils.access(_Pjax, '_Pjax');
_Utils.access(_Search, '_Search');
_Utils.access(_SelectAll, '_SelectAll');
_Utils.access(_Scripts, '_Scripts');
_Utils.access(_Switcher, '_Switcher');
_Utils.access(_Trans, '_Trans');
_Utils.access(_Upload, '_Upload');
_Utils.access(_Url, '_Url');
_Utils.access(_Utils, '_Utils');

// Main
$(document).ready(function()
{
	_App.init([
		{ page: 'app', js: [App] },
	]);

	_Utils.addEvent('laravel.config.ready', () => {
		_App.registerJs([
			{ page: 'app', js: [Pages, Menu] },
			{ page: 'app.admin', js: [] },

			{ page: 'blogs', js: [Blogs] },
			{ page: 'blog.create', js: [BlogView, BlogAdd] },
			{ page: 'blog.update', js: [BlogView, BlogEdit] },

			{ page: 'categories', js: [Categories] },
			{ page: 'category.create', js: [CategoryAdd] },
			{ page: 'category.update', js: [CategoryEdit] },

			{ page: 'settings', js: [Settings] },
		]);
	});
});
