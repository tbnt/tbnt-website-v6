'use strict';

/*
|--------------------------------------------------------------------------
| Dependencies
|--------------------------------------------------------------------------
|
| - jQuery
| - lodash
| - uikit classes
|
*/

/*
|--------------------------------------------------------------------------
| jQuery
|--------------------------------------------------------------------------
|
*/

/**
 * jQuery - Intersect elements
 *
 * @param {jQuery} jq Object to intersect
 * @return {jQuery}
 */
$.fn.intersect = function(jq)
{
	if (typeof jq === 'string')
		jy = $(jq);

	return $(this).filter(jq);
}

/**
 * jQuery - Get parents attribute value
 *
 * @param {string} attr Parent data attribute
 * @return {jQuery|undefined}
 */
$.fn.parentsAttr = function(attr)
{
	attr = removeBrackets(attr);
	const $parent = $(this).parents('[' + attr + ']');

	return $parent.eq(0) ? _.trim($parent.eq(0).attr(attr), '"') : undefined;
}

/**
 * jQuery - Get children attribute value
 *
 * @param {string} attr Children data attribute
 * @return {jQuery|undefined}
 */
$.fn.childrenAttr = function(attr)
{
	attr = removeBrackets(attr);
	const $children = $(this).children('[' + attr + ']');

	return $children.eq(0) ? _.trim($children.eq(0).attr(attr), '"') : undefined;
}

/**
 * jQuery - Set element hidden
 *
 * @param {boolean} hidden Element is hidden
 * @return {jQuery}
 */
$.fn.setHidden = function(hidden)
{
	return $(this).setProp('hidden', hidden);
}

/**
 * jQuery - Set element visible
 *
 * @param {boolean} visible Element is visible
 * @return {jQuery}
 */
$.fn.setVisible = function(visible)
{
	return $(this).css('visibility', visible === true ? 'visible' : 'hidden');
}

/**
 * jQuery - Set element property
 *
 * @param {string} property Element property
 * @param {boolean} visible Property is add or removed
 * @return {jQuery}
 */
$.fn.setProp = function(prop, visible)
{
	if (visible === false)
		return $(this).removeAttr(prop);
	else
		return $(this).attr(prop, '');
}

/**
 * jQuery - Serialize form object
 *
 * @return {object}
 */
$.fn.serializeObject = function()
{
	return $(this).serializeArray().reduce(function(a, x) { a[x.name] = x.value; return a; }, {});
}

/**
 * Get element attribute by attribute
 *
 * @param {attr} attr Attribute of the element
 * @return {string}
 */
const elAttr = (attr) =>
{
	const $el = $('[' + attr + ']');
	if ($el.length === 0) return '';

	return $el.attr(attr);
};



/*
|--------------------------------------------------------------------------
| Events
|--------------------------------------------------------------------------
|
*/

let _eventStack = {};

/**
 * Add event
 *
 * @param {string} name Name of the event
 * @param {function} callback Callback of the event
 * @return {void}
 */
const addEvent = (name, callback) =>
{
	if (_eventStack[name] === undefined)
		_eventStack[name] = [];

	_eventStack[name].push(callback);
};

/**
 * Trigger event
 *
 * @param {string} name Name of the event
 * @param {mixed} params Params to send
 * @return {void}
 */
const triggerEvent = (name, params) =>
{
	if (_eventStack[name] === undefined)
		return;

	for (let i = 0, c = _eventStack[name].length; i < c; i++)
		_eventStack[name][i](params);
};

/**
 * Remove event
 *
 * @param {string} name Name of the event
 * @return {void}
 */
const removeEvent = (name) =>
{
	delete _eventStack[name];
};

/**
 * Remove all events
 *
 * @return {void}
 */
const removeEvents = () =>
{
	_eventStack = {};
};

/**
 * Block event
 *
 * @param {object} event Event
 * @return {void}
 */
const blockEvent = function(event)
{
	event.stopPropagation();
	event.preventDefault();

	return false;
};



/*
|--------------------------------------------------------------------------
| Window
|--------------------------------------------------------------------------
|
*/

/**
 * Access var globally
 *
 * @param {string} name Name of the global var
 * @param {mixed} value Mixed
 * @return {void}
 */
const access = (value, name) =>
{
	if (name === undefined)
		window._access = [value].concat(window._access || []);
	else
		window[name] = value;
};



/*
|--------------------------------------------------------------------------
| Elements
|--------------------------------------------------------------------------
|
*/

/**
 * Block element
 *
 * @param {el} $el jQuery element
 * @param {boolean} block Block the element
 * @param {number} timeout Timeout to release blocking
 * @return {void}
 */
const blockElement = function($el, block, timeout)
{
	$el[block === false ? 'off' : 'on']('click', blockEvent);
	if (timeout !== undefined) setTimeout(() => blockElement($el, false), timeout);
};

/**
 * Show element
 *
 * @param {el} $el jQuery element
 * @param {mixed} value Value to check with target
 * @param {mixed} target Target to be checked with value
 * @param {boolean} strict Strict comparator
 * @return {void}
 */
const showElement = function($el, value, target, strict = true)
{
	if (strict === true) $el[value === target ? 'removeClass' : 'addClass']('uk-hidden');
	else $el[value == target ? 'removeClass' : 'addClass']('uk-hidden');
};

/**
 * Is event in element dimension range
 *
 * @param {object} e Event
 * @param {el} $el jQuery element
 * @param {object} dim Range { w: width, h: height, t: top, b: bottom, l: left, r: right }
 * @return {boolean}
 */
const isInElementRange = function(e, $box, dim = {})
{
	dim = _.assign({}, dim);

	const width = $box.outerWidth();
	const height = $box.outerHeight();

	const offsetTop = $box.offset().top;
	const offsetLeft = $box.offset().left;

	if (dim.w === undefined) dim.w = width;
	if (dim.h === undefined) dim.h = height;

	if (dim.l === undefined) { if (dim.r === undefined) dim.l = 0; else dim.l = width - dim.r - dim.w; }
	if (dim.t === undefined) { if (dim.b === undefined) dim.t = 0; else dim.t = height - dim.b - dim.h; }

	const leftMin = offsetLeft + dim.l;
	const leftMax = offsetLeft + dim.l + dim.w;

	const topMin = offsetTop + dim.t;
	const topMax = offsetTop + dim.t + dim.h;

	const posX = e.pageX;
	const posY = e.pageY;

	return posX >= leftMin && posX <= leftMax && posY >= topMin && posY <= topMax;
};



/*
|--------------------------------------------------------------------------
| Array
|--------------------------------------------------------------------------
|
*/

/**
 * Array init
 *
 * @param {array} arr Array to init
 * @return {array}
 */
const initArray = function(arr)
{
	if (Array.isArray(arr) === false)
		return [arr];

	return arr;
};

/**
 * Search needle in array
 *
 * @param {string|array} needle Needle
 * @param {array} arr Array to search
 * @return {integer}
 */
const exists = (needle, arr) =>
{
	if (Array.isArray(needle) === false) {
		return arr.indexOf(needle) > -1;
	}
	else {
		var i = needle.length;
		while (i--) { if (arr.indexOf(needle[i]) > -1) return true; }
	}

	return false;
};

/**
 * Array unique
 *
 * @param {array} arr Array to filter
 * @return {array}
 */
const uniqueArray = function(arr)
{
	return arr.filter(function(value, index, self) {
		return self.indexOf(value) === index;
	});
};

/**
 * Array map values as number
 *
 * @param {array} arr Array to filter
 * @return {array}
 */
const mapNumber = function(arr)
{
	return arr.map(function(value) {
		return Number(value);
	});
};



/*
|--------------------------------------------------------------------------
| Object
|--------------------------------------------------------------------------
|
*/

/**
 * Check if value is jquery object
 *
 * @param {mixed} obj Mixed
 * @return {string}
 */
const isJquery = (obj) =>
{
	return obj instanceof jQuery;
}

/**
 * Check if value is object
 *
 * @param {mixed} value Value to check
 * @param {boolean} strict Check Jquery === false
 * @return {boolean}
 */
const isObject = (value, strict = false) =>
{
	return typeof value === 'object' && Array.isArray(value) === false && (strict === false || isJquery(value) === false);
};

/**
 * Get object name
 *
 * @param {mixed} obj Mixed
 * @param {string} name Object name
 * @param {boolean} strict Strict comparator
 * @return {string}
 */
const isObjectType = (obj, name, strict = true) =>
{
	if (strict === true)
		return getObjectType(obj).indexOf(name) !== -1;
	else
		return getObjectType(obj).toLowerCase().indexOf(name.toLowerCase()) !== -1;
}

/**
 * Get object name
 *
 * @param {mixed} obj Mixed
 * @return {string}
 */
const getObjectType = (obj) =>
{
	if (obj === null)
		return null;

	return _.trim(Object.prototype.toString.call(obj).replace('[object', '').replace(']', ''));
}

/**
 * Get object index
 *
 * @param {mixed} obj Mixed
 * @param {integer} index Object Index
 * @return {mixed}
 */
const getObjectIndex = (obj, index) =>
{
	return obj[_.keys(obj)[index]];
}

/**
 * Convert object to array
 *
 * @param {object} obj Object to convert
 * @return {array}
 */
const objectToArray = (obj) =>
{
	return $.map(obj, function(value, index) {
		return value;
	});
};

/**
 * Get keys of object
 *
 * @param {object} obj Object to search value
 * @return {array}
 */
const keys = (obj) =>
{
	return Object.keys(obj);
};

/**
 * Get size of object
 *
 * @param {object} obj Object to search value
 * @return {integer}
 */
const size = (obj) =>
{
	return keys(obj).length;
};

/**
 * Search object value with property
 *
 * @param {object} obj Object to search value
 * @param {string} path Object path
 * @return {mixed}
 */
const deepValue = (obj, path) =>
{
	try {
		const pathParts = path.split('.');

		for (let i = 0, c = pathParts.length; i < c; i++)
			obj = obj[pathParts[i]];
	}
	catch (e) {
		return path;
	}

	return obj;
};

/**
 * Set object value with property
 *
 * @param {object} obj Object to search value
 * @param {string} path Object path
 * @return {mixed}
 */
const deepAssign = (obj, path, value) =>
{
	let propNames = path.split('.');
	let propLength = propNames.length - 1;
	let tmpObj = obj;

	for (let i = 0; i <= propLength ; i++)
		tmpObj = tmpObj[propNames[i]] = i !== propLength ?  {} : value;

	return obj;
}



/*
|--------------------------------------------------------------------------
| Collections
|--------------------------------------------------------------------------
|
*/

/**
 * Sort array of objects with property
 *
 * @param {array} arr Array to sort
 * @param {string} path Object path
 * @return {array}
 */
const sortByAttr = (arr, path, caseInsensitive = true) =>
{
	return arr.sort(function(a, b)
	{
		let x = deepValue(a, path);
		let y = deepValue(b, path);

		if (caseInsensitive) {
			x = x.toLowerCase();
			y = y.toLowerCase();
		}

		return x < y ? -1 : x > y ? 1 : 0;
	});
};

/**
 * Natural sort array of objects with property
 *
 * @param {array} arr Array to sort
 * @param {string} path Object path
 * @return {array}
 */
const sortByAttrAlphaNum = (arr, path, caseInsensitive = true) =>
{
	for (let z = 0, t; t = arr[z] && deepValue(arr[z], path); z++) {
		let tz = [];
		let x = 0, y = -1, n = 0, i, j;

		while (i = (j = t.charAt(x++)).charCodeAt(0)) {
			let m = (i == 46 || (i >=48 && i <= 57));
			if (m !== n) {
				tz[++y] = "";
				n = m;
			}
			tz[y] += j;
		}

		deepAssign(arr[z], path, tz);
	}

	arr.sort(function(x, y)
	{
		let a = deepValue(x, path);
		let b = deepValue(y, path);

		for (let x = 0, aa, bb; (aa = a[x]) && (bb = b[x]); x++) {
			if (caseInsensitive) {
				aa = aa.toLowerCase();
				bb = bb.toLowerCase();
			}
			if (aa !== bb) {
				let c = Number(aa), d = Number(bb);
				if (c == aa && d == bb) {
					return c - d;
				} else return (aa > bb) ? 1 : -1;
			}
		}
		return a.length - b.length;
	});

	for (let z = 0; z < arr.length; z++)
		deepAssign(arr[z], path, deepValue(arr[z], path).join(""));

	return arr;
}



/*
|--------------------------------------------------------------------------
| Variables
|--------------------------------------------------------------------------
|
*/

/**
 * Return typeof lowercase
 *
 * @param {mixed} mixed Var
 * @return {string}
 */
const varType = (mixed) =>
{
	return String(typeof mixed).toLowerCase();
};

/**
 * Check if var type if boolean
 *
 * @param {mixed} mixed Var
 * @return {boolean}
 */
const isBool = (mixed) =>
{
	return varType(mixed) === 'boolean';
};

/**
 * Check if var type if function
 *
 * @param {mixed} mixed Var
 * @return {boolean}
 */
const isFunc = (mixed) =>
{
	return varType(mixed) === 'function';
};

/**
 * Check if var type if string
 *
 * @param {mixed} mixed Var
 * @return {boolean}
 */
const isString = (mixed) =>
{
	return varType(mixed) === 'string';
};

/**
 * Check if var type if number
 *
 * @param {mixed} mixed Var
 * @return {boolean}
 */
const isNumber = (mixed) =>
{
	return varType(mixed) === 'number';
};



/*
|--------------------------------------------------------------------------
| String
|--------------------------------------------------------------------------
|
*/

/**
 * Trim char string
 *
 * @return {string}
 */
const trimChar = (origString, charToTrim) =>
{
	charToTrim = escapeRegExp(charToTrim);
	var regEx = new RegExp("^[" + charToTrim + "]+|[" + charToTrim + "]+$", "g");

	return origString.replace(regEx, "");
};

/**
 * Escape special characters for use in a regular expression
 *
 * @return {string}
 */
const escapeRegExp = (strToEscape) =>
{
	return strToEscape.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
};

/**
 * Leading strings
 *
 * @param {string} str String to add leading string
 * @param {number} count Prefix count
 * @param {string} prefix Prefix
 * @param {boolean} truncate Truncate at count
 * @return {string}
 */
const leadingString = (str, count, prefix = '0', truncate = false) =>
{
	str = String(str);
	count = count < 1 ? 1 : count;

	if (truncate === false && str.length > count)
		return str;

	return String(_.fill(Array(count), prefix).join('') + str).substr(0 - count);
};

/**
 * Remove brackets
 *
 * @param {string} str String to clean
 * @return {string}
 */
const removeBrackets = (str) =>
{
	return _.trim(_.trim(str, '['), ']');
};

/**
 * Remove diacritics
 *
 * @param {string} str String to clean
 * @return {string}
 */
const removeDiacritics = (str) =>
{
	const diacriticsRemovalMap = [
		{'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
		{'base':'AA','letters':/[\uA732]/g},
		{'base':'AE','letters':/[\u00C6\u01FC\u01E2]/g},
		{'base':'AO','letters':/[\uA734]/g},
		{'base':'AU','letters':/[\uA736]/g},
		{'base':'AV','letters':/[\uA738\uA73A]/g},
		{'base':'AY','letters':/[\uA73C]/g},
		{'base':'B', 'letters':/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},
		{'base':'C', 'letters':/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},
		{'base':'D', 'letters':/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},
		{'base':'DZ','letters':/[\u01F1\u01C4]/g},
		{'base':'Dz','letters':/[\u01F2\u01C5]/g},
		{'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
		{'base':'F', 'letters':/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},
		{'base':'G', 'letters':/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},
		{'base':'H', 'letters':/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
		{'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
		{'base':'J', 'letters':/[\u004A\u24BF\uFF2A\u0134\u0248]/g},
		{'base':'K', 'letters':/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},
		{'base':'L', 'letters':/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},
		{'base':'LJ','letters':/[\u01C7]/g},
		{'base':'Lj','letters':/[\u01C8]/g},
		{'base':'M', 'letters':/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},
		{'base':'N', 'letters':/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},
		{'base':'NJ','letters':/[\u01CA]/g},
		{'base':'Nj','letters':/[\u01CB]/g},
		{'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
		{'base':'OI','letters':/[\u01A2]/g},
		{'base':'OO','letters':/[\uA74E]/g},
		{'base':'OU','letters':/[\u0222]/g},
		{'base':'P', 'letters':/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},
		{'base':'Q', 'letters':/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},
		{'base':'R', 'letters':/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},
		{'base':'S', 'letters':/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},
		{'base':'T', 'letters':/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},
		{'base':'TZ','letters':/[\uA728]/g},
		{'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
		{'base':'V', 'letters':/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},
		{'base':'VY','letters':/[\uA760]/g},
		{'base':'W', 'letters':/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
		{'base':'X', 'letters':/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},
		{'base':'Y', 'letters':/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},
		{'base':'Z', 'letters':/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},
		{'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
		{'base':'aa','letters':/[\uA733]/g},
		{'base':'ae','letters':/[\u00E6\u01FD\u01E3]/g},
		{'base':'ao','letters':/[\uA735]/g},
		{'base':'au','letters':/[\uA737]/g},
		{'base':'av','letters':/[\uA739\uA73B]/g},
		{'base':'ay','letters':/[\uA73D]/g},
		{'base':'b', 'letters':/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
		{'base':'c', 'letters':/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
		{'base':'d', 'letters':/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
		{'base':'dz','letters':/[\u01F3\u01C6]/g},
		{'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
		{'base':'f', 'letters':/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
		{'base':'g', 'letters':/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
		{'base':'h', 'letters':/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
		{'base':'hv','letters':/[\u0195]/g},
		{'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
		{'base':'j', 'letters':/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
		{'base':'k', 'letters':/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
		{'base':'l', 'letters':/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
		{'base':'lj','letters':/[\u01C9]/g},
		{'base':'m', 'letters':/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
		{'base':'n', 'letters':/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
		{'base':'nj','letters':/[\u01CC]/g},
		{'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
		{'base':'oi','letters':/[\u01A3]/g},
		{'base':'ou','letters':/[\u0223]/g},
		{'base':'oo','letters':/[\uA74F]/g},
		{'base':'p','letters':/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
		{'base':'q','letters':/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
		{'base':'r','letters':/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
		{'base':'s','letters':/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
		{'base':'t','letters':/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
		{'base':'tz','letters':/[\uA729]/g},
		{'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
		{'base':'v','letters':/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
		{'base':'vy','letters':/[\uA761]/g},
		{'base':'w','letters':/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
		{'base':'x','letters':/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
		{'base':'y','letters':/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
		{'base':'z','letters':/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}
	];

	for (var i = 0; i < diacriticsRemovalMap.length; i++)
		str = str.replace(diacriticsRemovalMap[i].letters, diacriticsRemovalMap[i].base);

	return str;
}



/*
|--------------------------------------------------------------------------
| Number
|--------------------------------------------------------------------------
|
*/

/**
 * Format number
 *
 * @param {number} price Number to format
 * @param {number} c Decimal
 * @param {string} d Dot separator
 * @param {string} t Thousand separator
 * @return {string}
 */
const formatNum = (price, c, d, t) =>
{
	var n = price,
		c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d == undefined ? "." : d,
		t = t == undefined ? "'" : t,
		s = n < 0 ? "-" : "",
		i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
		j = (j = i.length) > 3 ? j % 3 : 0;

	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

/**
 * Force return number
 *
 * @param {mixed} number Number
 * @return {number}
 */
const forceNumber = (number, fallback = 0) =>
{
	number = Number(number);
	number = varType(number) !== 'number' || isNaN(number) === true ? fallback : number;

	return number;
};



/*
|--------------------------------------------------------------------------
| Colors
|--------------------------------------------------------------------------
|
*/

/**
 * Convert rgb(a) color to hex
 *
 * @param {string} rgb RGB color
 * @return {string}
 */
const colorToHex = (rgb) =>
{
	rgb = rgb.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);
	return (rgb && rgb.length === 4) ? '#' +
		('0' + parseInt(rgb[1], 10).toString(16)).slice(-2) +
		('0' + parseInt(rgb[2], 10).toString(16)).slice(-2) +
		('0' + parseInt(rgb[3], 10).toString(16)).slice(-2) : '';
};



/*
|--------------------------------------------------------------------------
| Date
|--------------------------------------------------------------------------
|
*/

/**
 * Convert date to UTC string
 *
 * @param {string} value Date value
 * @return {string}
 */
const dateUTC = (value) =>
{
	return `${value.split(' ').join('T')}Z`;
};

/**
 * Get diff between months count between two dates
 *
 * @param {date} startDate
 * @param {date} endDate
 * @return {number}
 */
const monthDiff = (startDate, endDate) =>
{
	return ((endDate.getFullYear() - startDate.getFullYear()) * 12) + (endDate.getMonth() - startDate.getMonth());
};



/*
|--------------------------------------------------------------------------
| Validations
|--------------------------------------------------------------------------
|
*/

/**
 * Check emil format
 *
 * @param {string} email Email
 * @return {boolean}
 */
const isEmail = (email) =>
{
	const regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return regex.test(email)
};



/*
|--------------------------------------------------------------------------
| Forms
|--------------------------------------------------------------------------
|
*/

/**
 * Empty input file
 *
 * @return {DOMnode}
 */
const emptyInputFile = (input) =>
{
	if (isJquery(input) === true)
		input = input.get(0);

	try {
		input.value = '';
		input.type = 'text';
		input.type = 'file';
	}
	catch(e) {}
};



/*
|--------------------------------------------------------------------------
| Navigator
|--------------------------------------------------------------------------
|
*/

/**
 * Get query parameter
 *
 * @param {string} name Value parameter
 * @param {string} url Url to parse
 * @return {string}
 */
const getQueryParameter = (name, url) =>
{
	if (!url) url = window.location.href;
	name = name.replace(/[\[\]]/g, "\\$&");

	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)", "i"),
		results = regex.exec(url);

	if (!results) return null;
	if (!results[2]) return '';

	return decodeURIComponent(results[2].replace(/\+/g, " "));
};

/**
 * Check mobile
 *
 * @return {boolean}
 */
const isMobile =
{
	agent: navigator.userAgent.toLowerCase(),
	android: function()
	{
		return Boolean(this.agent.match(/android/i));
	},
	blackberry: function()
	{
		return Boolean(this.agent.match(/blackberry/i));
	},
	ios: function()
	{
		return Boolean(this.agent.match(/iphone|ipad|ipod/i));
	},
	opera: function()
	{
		return Boolean(this.agent.match(/opera mini/i));
	},
	windows: function()
	{
		return Boolean(this.agent.match(/iemobile/i) || this.agent.match(/wpdesktop/i));
	},
	any: function()
	{
		return Boolean((this.android() || this.blackberry() || this.ios() || this.opera() || this.windows()));
	}
};

/**
 * Check navigator
 *
 * @return {boolean}
 */
const isNavigator =
{
	opera: function()
	{
		// Opera 8.0+
		return (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
	},
	firefox: function()
	{
		// Firefox 1.0+
		return typeof InstallTrigger !== 'undefined';
	},
	safari: function()
	{
		var chrome   = navigator.userAgent.indexOf('Chrome') > -1;
		var safari   = navigator.userAgent.indexOf("Safari") > -1;

		return chrome === false && safari == true;
	},
	ie: function()
	{
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf('MSIE '); // IE 10 or older
		var trident = ua.indexOf('Trident/'); // IE 11

		if (msie > 0)
			return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10) !== 0;

		if (trident > 0) {
			var rv = ua.indexOf('rv:');
			return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10) !== 0;
		}

		// Internet Explorer 6-11
		return /*@cc_on!@*/false || !!document.documentMode;
	},
	edge: function()
	{
		var ua = window.navigator.userAgent;
		var edge = ua.indexOf('Edge/');

		if (edge > 0)
			return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10) !== 0;

		// Edge 20+
		return !this.ie && !!window.StyleMedia;
	},
	ieEdge: function()
	{
		return this.ie() || this.edge();
	},
	chrome: function()
	{
		// Chrome 1+
		return !!window.chrome && !!window.chrome.webstore;
	},
	blink: function()
	{
		// Blink engine detection
		return (isChrome || isOpera) && !!window.CSS;
	}
};



/*
|--------------------------------------------------------------------------
| Helpers
|--------------------------------------------------------------------------
|
*/

let _getUniqId_increment = 0;

/**
 * Get unique identifier
 *
 * @return {string}
 */
const getUniqId = (append) =>
{
	var uniqId = String((new Date()).getTime());
	if (append !== false) uniqId = uniqId + (append || '') + (_getUniqId_increment += 1);

	return uniqId;
};



/*
|--------------------------------------------------------------------------
| Debug
|--------------------------------------------------------------------------
|
*/

/**
 * Get FormData values
 *
 * @param {object} formData FormData
 * @return {object}
 */
const debugFormData = (formData) =>
{
	if (getObjectType(formData) !== 'FormData' || formData.entries === undefined)
		return formData;

	const entries = formData.entries();
    const result = {};

    let next;
    let pair;

    while ((next = entries.next()) && next.done === false) {
        pair = next.value;
        result[pair[0]] = pair[1];
    }

	return result;
};



export default
{
	/*
	| jQuery
	|--------------------------------------------------------------------------
	*/

	elAttr,

	/*
	| Events
	|--------------------------------------------------------------------------
	*/

	addEvent,
	triggerEvent,
	removeEvent,
	removeEvents,
	blockEvent,

	/*
	| Window
	|--------------------------------------------------------------------------
	*/

	access,

	/*
	| Elements
	|--------------------------------------------------------------------------
	*/

	blockElement,
	showElement,
	isInElementRange,

	/*
	| Array
	|--------------------------------------------------------------------------
	*/

	initArray,
	exists,
	uniqueArray,
	mapNumber,

	/*
	| Object
	|--------------------------------------------------------------------------
	*/

	isJquery,
	isObject,
	isObjectType,
	getObjectType,
	getObjectIndex,
	objectToArray,
	keys,
	size,
	deepValue,
	deepAssign,

	/*
	| Collections
	|--------------------------------------------------------------------------
	*/

	sortByAttr,
	sortByAttrAlphaNum,

	/*
	| Variables
	|--------------------------------------------------------------------------
	*/

	varType,
	isBool,
	isFunc,
	isString,
	isNumber,

	/*
	| String
	|--------------------------------------------------------------------------
	*/

	trimChar,
	escapeRegExp,
	leadingString,
	removeBrackets,
	removeDiacritics,

	/*
	| Number
	|--------------------------------------------------------------------------
	*/

	formatNum,
	forceNumber,

	/*
	| Colors
	|--------------------------------------------------------------------------
	*/

	colorToHex,

	/*
	| Date
	|--------------------------------------------------------------------------
	*/

	dateUTC,
	monthDiff,

	/*
	| Validations
	|--------------------------------------------------------------------------
	*/

	isEmail,

	/*
	| Forms
	|--------------------------------------------------------------------------
	*/

	emptyInputFile,

	/*
	| Navigator
	|--------------------------------------------------------------------------
	*/

	getQueryParameter,
	isMobile,
	isNavigator,

	/*
	| Helpers
	|--------------------------------------------------------------------------
	*/

	getUniqId,

	/*
	| Debug
	|--------------------------------------------------------------------------
	*/

	debugFormData,

};
