'use strict';

const get = (name, defaultValue) =>
{
	if (window._scripts === undefined)
		reset();

	const value = window._scripts[name];
	return value !== undefined ? value : defaultValue;
};

const getAll = () =>
{
	return window._scripts || {};
};

const reset = () =>
{
	window._scripts = {};
};

export default
{
	get,
	getAll,
	reset,
};
