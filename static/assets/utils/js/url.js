'use strict';

let _file = 'urls';
let _prefix = '';

const get = (key, params, withPrefix = true) =>
{
	return (withPrefix ? _prefix : '') + _Trans.get(_file + '.' + key, params);
};

const getAll = () =>
{
	return Laravel.trans[_file] || {};
};

const setFile = (file) =>
{
	_file = file;
};

const setPrefix = (prefix) =>
{
	_prefix = prefix;
};

export default
{
	get,
	getAll,
	setFile,
	setPrefix,
};
