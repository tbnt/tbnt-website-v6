'use strict';

let _config = [];

const init = function(config)
{
	callJs(config);
	getConfig();
};

const callback = function()
{
	callJs();
};

const getCurrentMenu = function()
{
	return Laravel.menu;
};

const getLangId = function()
{
	return Laravel.lang.id;
};

const getLangCode = function()
{
	return Laravel.lang.code;
};

const isJs = function(page)
{
	return Laravel.js.indexOf(page) > -1;
};

const registerJs = function(config = [])
{
	_config = _.concat([], _config, config);

	callJs();
};

const callJs = function(config)
{
	_.map(config || _config, (config) => {
		if (isJs(config.page) === false)
			return;

		if (Array.isArray(config.js) === false)
			config.js = [config.js];

		_.map(config.js, (js) => {
			if (js.init !== undefined) js.init();
			else console.warn('Undefined method "init":', _Utils.getObjectType(js), js);
		});
	});
};

const getConfig = function()
{
	_Ajax.get('app-config', {
		prefix: 'ajax',
		success: (data) => {
			Laravel.trans = _.assign({}, data.trans);
			Laravel.const = _.assign({}, data.const);
		},
		error: (error) => {
			console.warn('App load trans error:', error);
		},
		complete: (error) => {
			_Utils.triggerEvent('laravel.config.ready');
		},
	});
};

export default
{
	init,
	getCurrentMenu,
	getLangId,
	getLangCode,
	isJs,
	registerJs,
};

$(document).on('pjax:callback', callback);
