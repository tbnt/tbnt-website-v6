<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Example command
|--------------------------------------------------------------------------
|
| ex: php artisan inspire
|
*/

Artisan::command('inspire', function ()
{
	$this->comment('Inspiring::quote()');
})
->describe('Display an inspiring quote');



/*
|--------------------------------------------------------------------------
| Hash password
|--------------------------------------------------------------------------
|
| ex: php artisan hash_password {password}
|
*/

Artisan::command('hash_password {password}', function ($password)
{
	$this->comment(Hash::make($password));
})
->describe('Hash password');



/*
|--------------------------------------------------------------------------
| Encrypt string
|--------------------------------------------------------------------------
|
| ex: php artisan encrypt_string {string}
|
*/

Artisan::command('encrypt_string {string}', function ($string)
{
	$this->comment(encrypt($string));
})
->describe('Encrypt string');



/*
|--------------------------------------------------------------------------
| Create database
|--------------------------------------------------------------------------
|
| ex: php artisan create_db {db_name}
|
*/

Artisan::command('create_db {db_name}', function ($db_name)
{
	$host = env('DB_HOST');
	$username = env('DB_USERNAME');
	$password = env('DB_PASSWORD');
	$database = e($db_name);

	$this->comment('Creating database...');

	try {
		$db = new \PDO('mysql:host='.$host.';charset=utf8', $username, $password);
		$db->exec('
			CREATE DATABASE `'.$database.'`;
			CREATE USER "'.$username.'"@"'.$host.'" IDENTIFIED BY "'.$password.'";
			GRANT ALL ON `'.$database.'`.* TO "'.$username.'"@"'.$host.'";
			FLUSH PRIVILEGES;'
		);
	} catch (\PDOException $e) {
		return $this->comment('Error creating database: '.$e->getMessage());
	}

	$this->comment('Creating tables...');

	Artisan::call('migrate');

	$this->comment('Database "'.$db_name.'" ready.');
})
->describe('Encrypt db_name');
