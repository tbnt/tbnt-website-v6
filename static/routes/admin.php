<?php

/*
|--------------------------------------------------------------------------
| AUTH
|--------------------------------------------------------------------------
|
*/

Route::get('/logout', 'AuthController@getLogout')->name('admin.logout');



/*
|--------------------------------------------------------------------------
| LOGGED OUT ROUTES
|--------------------------------------------------------------------------
|
*/

Route::group(['middleware' => [
	'auth.admin.logged_out',
]], function ()
{
	/*
	|--------------------------------------------------------------------------
	| AUTH
	|--------------------------------------------------------------------------
	|
	*/

	Route::get('/login', 'AuthController@getLogin')->name('admin.login');

	Route::post('/login', 'AuthController@postLogin');
});



/*
|--------------------------------------------------------------------------
| LOGGED IN ROUTES
|--------------------------------------------------------------------------
|
*/

Route::group(['middleware' => [
	'auth.admin.logged_in',
]], function ()
{
	/*
	|--------------------------------------------------------------------------
	| UPDATE
	|--------------------------------------------------------------------------
	|
	*/

	Route::get('/update-site', 'UpdateController@update');



	/*
	|--------------------------------------------------------------------------
	| Goecode
	|--------------------------------------------------------------------------
	|
	*/

	Route::post('/geocode', 'GeocodeController@geocode');



	/*
	|--------------------------------------------------------------------------
	| HOME
	|--------------------------------------------------------------------------
	|
	*/

	Route::get('/', 'HomeController@getHome')->name('admin.home');



	/*
	|--------------------------------------------------------------------------
	| CATEGORIES
	|--------------------------------------------------------------------------
	|
	*/

	Route::get('/categories', 'CategoryController@getCategories')->name('admin.categories');
	Route::get('/category/new', 'CategoryController@getNewCategory')->name('admin.category.new');
	Route::get('/category/{category_id}/edit', 'CategoryController@getEditCategory')->where('category_id', '[0-9]+')->name('admin.category.edit');

	Route::post('/category/new', 'CategoryController@postNewCategory');
	Route::post('/category/{category_id}/edit', 'CategoryController@postEditCategory')->where('category_id', '[0-9]+');
	Route::post('/category/{category_id}/delete', 'CategoryController@postDeleteCategory')->where('category_id', '[0-9]+');



	/*
	|--------------------------------------------------------------------------
	| BLOGS
	|--------------------------------------------------------------------------
	|
	*/

	Route::get('/blogs', 'BlogController@getBlogs')->name('admin.blogs');
	Route::get('/blog/new', 'BlogController@getNewBlog')->name('admin.blog.new');
	Route::get('/blog/{blog_id}/edit', 'BlogController@getEditBlog')->where('blog_id', '[0-9]+')->name('admin.blog.edit');
	Route::get('/blog/{blog_id}/preview', 'BlogController@getPreviewBlog')->where('blog_id', '[0-9]+')->name('admin.blog.preview');

	Route::post('/blog/new', 'BlogController@postNewBlog');
	Route::post('/blog/{blog_id}/edit', 'BlogController@postEditBlog')->where('blog_id', '[0-9]+');
	Route::post('/blog/{blog_id}/delete', 'BlogController@postDeleteBlog')->where('blog_id', '[0-9]+');
	Route::post('/blog/{blog_id}/duplicate', 'BlogController@postDuplicateBlog')->where('blog_id', '[0-9]+');
	Route::post('/blog/{blog_id}/preview', 'BlogController@postPreviewBlog')->where('blog_id', '[0-9]+');



	/*
	|--------------------------------------------------------------------------
	| SETTINGS
	|--------------------------------------------------------------------------
	|
	*/

	Route::get('/settings', 'SettingsController@getSettings')->name('admin.settings');

	Route::post('/settings/edit', 'SettingsController@postEditSettings');
});
