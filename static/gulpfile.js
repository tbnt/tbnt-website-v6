var elixir = require('laravel-elixir');

elixir.config.js.folder = '../../';
elixir.config.js.outputFolder = '../';

elixir.config.css.folder = '../../';
elixir.config.css.outputFolder = '../';

elixir.config.css.less.folder = '../../';
elixir.config.css.sass.folder = '../../';

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix)
{
	// Styles
	mix.sass(
		'assets/app/styles/app.scss',
		'assets/build/app/styles/custom.css'
	);

	mix.styles(
		[
			'node_modules/flatpickr/dist/flatpickr.min.css',
			'node_modules/flatpickr/dist/ie.css',
			'node_modules/flatpickr/dist/themes/light.css',
			'node_modules/animate.css/animate.min.css',
			'node_modules/owl.carousel/dist/assets/owl.carousel.css',
			'node_modules/owl.carousel/dist/assets/owl.theme.default.css',
			'assets/build/app/styles/custom.css',
		],
		'public/app/styles/vendor.css'
	);

	// Scripts
	mix.webpack(
		[
			'assets/app/scripts/main.js',
		],
		'assets/build/app/scripts/custom.js'
	);

	mix.scripts(
		[
			'node_modules/jquery/dist/jquery.js',
			'node_modules/lodash/lodash.min.js',
			'node_modules/gsap/src/uncompressed/TweenMax.js',
			'node_modules/gsap/src/uncompressed/plugins/ScrollToPlugin.js',
			'node_modules/gsap/src/uncompressed/utils/SplitText.js',
			'node_modules/wowjs/dist/wow.js',
			'node_modules/flatpickr/dist/flatpickr.js',
			'node_modules/tilt.js/dest/tilt.jquery.min.js',
			'node_modules/owl.carousel/dist/owl.carousel.js',
			'assets/app/scripts/functions.js',
			'assets/build/app/scripts/custom.js'
		],
		'public/app/scripts/vendor.js'
	);
});
