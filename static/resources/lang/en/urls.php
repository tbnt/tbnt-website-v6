<?php

return [

	/*
	|--------------------------------------------------------------------------
	| URLS
	|--------------------------------------------------------------------------
    |
    | Custom
    |
	*/

	'home' 					=> 'en',

	'blogs' 				=> 'en/blogs',
	'blog_post' 			=> 'en/blog',
	'blog_id' 				=> '[0-9a-z\-]+',

];
