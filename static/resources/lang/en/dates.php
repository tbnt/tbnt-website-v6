<?php

return [

	/*
	|--------------------------------------------------------------------------
	| DATES
	|
	| PHP - http://php.net/manual/en/function.strftime.php
	| JS - https://momentjs.com/docs/#/displaying
	|--------------------------------------------------------------------------
    |
    | Custom
    |
	*/

	'lmdhm' 				=> '%A %m.%d at %H:%M',
	'lmd' 					=> '%A %m.%d',
	'lmy' 					=> '%d %B %Y',
	'l' 					=> '%A',
	'md' 					=> '%m.%d',

	'js' => [
		'lmdhm' 			=> 'dddd MM.DD at HH:mm',
		'lmd' 				=> 'dddd MM.DD',
		'md' 				=> 'MM.DD',
		'mdy' 				=> 'MM.DD.YYYY',
	],

	'fp' => [
		'lmd' 				=> 'F j, Y',
	],

	'time_format' 			=> 'Format "hh:mm", ex: "23:45"',

];
