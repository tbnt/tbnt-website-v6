<?php

return [

	/*
	|--------------------------------------------------------------------------
	| EMAILS
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'subject' 				=> app_name(),
	'end_message'			=> 'With our best wishes, :name',
	'copyright'				=> 'Copyright ©',

];
