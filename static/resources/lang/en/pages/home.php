<?php

return [

	/*
	|--------------------------------------------------------------------------
	| HOME
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'menu_item_1'					=> 'VR version',
	'menu_item_2'					=> 'l\'avent UI kit',
	'menu_item_1_tag'				=> 'Coming soon!',

	'meta_title' 					=> 'Digital & Mobile App Development Agency based in Lausanne - Thanks but no thanks',
	'meta_desc' 					=> 'Our web agency based in Lausanne designs and develops unique digital experimences. Contact us to let us know about your project!',

	'title_element_1'				=> 'DIFF',
	'title_element_2'				=> 'ERENT',
	'title_element_3'				=> 'IS',
	'title_element_4'				=> 'BETTER',

	'intro_img_subtitle'			=> 'public/images/intro/subtitle_en.svg',
	'intro_presentation_tbnt_1'		=> 'We are millennials and we grew up with the digital transformation. We evolved surrounded by interfaces.',
	'intro_presentation_tbnt_2'		=> 'We challenge the status quo and create unique and customized digital experiments to achieve the most ambitious of your projects.',

	'reel_title_element_1'			=> 'SHOW',
	'reel_title_element_2'			=> 'REEL',

	'client_title_element_1'		=> 'OUR',
	'client_title_element_2'		=> 'CLIENTS',
	'client_many_more'				=> 'They trust us',
	'client_working_for'			=> 'Currently working on',
	'client_cta'					=> 'Are you next?',

	'client_title_1'				=> 'Paleo',
	'client_desc_1'					=> 'Turning festival media management into a simple and powerful tool.',
	'client_tags_1'					=> 'Work achieved : UX / UI, custom development',

	'client_title_2'				=> 'Ikentoo',
	'client_desc_2'					=> 'Creating a live dashboard application for bars and restaurants’ owners.',
	'client_tags_2'					=> 'Work achieved : UX / UI, react native development.',

	'client_title_3'				=> 'Jumpfax ',
	'client_desc_3'					=> 'The best connection between horse riders and the showjumping world.',
	'client_tags_3'					=> 'Work achieved : UX / UI, custom dev., native iOS and Android',

	'client_title_4'				=> 'Gottex Brokers',
	'client_desc_4'					=> 'Thinking outside of the box and discover finance from a different angle.',
	'client_tags_4'					=> 'Work achieved : Rebranding, UX/UI, content production, custom development',

	'client_title_5'				=> 'Timea b.',
	'client_desc_5'					=> 'Reshaping Switzerland’s number one woman tennis player’s online identity',
	'client_tags_5'					=> 'Work achieved : Rebranding, UX/UI, content production, custom development',

	'client_title_6'				=> 'MICR',
	'client_desc_6'					=> 'A chain for human dignity.',
	'client_tags_6'					=> 'Work achieved : UX / UI, web and mobile dev.',

	'client_title_7'				=> 'Montreux Jazz Insider',
	'client_desc_7'					=> 'Reshape of Montreux Jazz Insider — a subscription-based app which brings together fans of the festival and provides them access to exclusive advantages and news.',
	'client_tags_7'					=> 'Work achieved : UX, UI, react native development.',

	'service_title_element_1'		=> 'OUR',
	'service_title_element_2'		=> 'Services',

	'service_col_1_title'			=> 'We think',
	'service_col_2_title'			=> 'We design',
	'service_col_3_title'			=> 'We build',
	'service_col_4_title'			=> 'We use',

	'service_col_1_label_1'			=> 'Industry and audience research ',
	'service_col_1_desc_1'			=> 'To gather information about your market and audience.',
	'service_col_1_label_2'			=> 'Digital strategy',
	'service_col_1_desc_2'			=> 'To plan and communicate properly.',
	'service_col_1_label_3'			=> 'Product sprints',
	'service_col_1_desc_3'			=> 'To test and validate your assumptions.',

	'service_col_2_label_1'			=> 'Visual identities',
	'service_col_2_desc_1'			=> 'To make you look appealing',
	'service_col_2_label_2'			=> 'UX strategies',
	'service_col_2_desc_2'			=> 'To convert your users into future clients',
	'service_col_2_label_3'			=> 'User-interface designs',
	'service_col_2_desc_3'			=> 'To keep your visitors coming back',

	'service_col_3_label_1'			=> 'Tailor made websites & plateforms',
	'service_col_3_desc_1'			=> 'To make sure we develop what you need.',
	'service_col_3_label_2'			=> 'Native or hybrid apps',
	'service_col_3_desc_2'			=> 'Because your market is mobile',
	'service_col_3_label_3'			=> 'E-commerce solutions',
	'service_col_3_desc_3'			=> 'To make sure your next order is just one click away.',

	'service_col_4_label_1'			=> 'SEO',
	'service_col_4_desc_1'			=> 'To get your grounded on search engines',
	'service_col_4_label_2'			=> 'Social media advertising',
	'service_col_4_desc_2'			=> 'To target your audience and leverage your brand',
	'service_col_4_label_3'			=> 'Google AdWords',
	'service_col_4_desc_3'			=> 'To get your website across the web instantly',

	'service_col_4_brandsup'		=> 'In collaboration with',

	'team_title_element_1'			=> 'Team',
	'team_title_element_2'			=> 'Players',

	'team_1_work'					=> 'Co-founder & team management',
	'team_2_work'					=> 'Co-founder & development director',
	'team_3_work'					=> 'Digital designer',
	'team_4_work'					=> 'Full stack developer',
	'team_button'					=> 'Play on Spotify',

	'team_thanks'					=> 'Special thanks to all the people who helped us grow our company',

	'contact_title_element_1'		=> 'Say',
	'contact_title_element_2'		=> 'Hello?',
	'contact_col_1_label'			=> 'You\'d like to pass by?',
	'contact_col_1_item_1'			=> 'Avenue d\'Ouchy 4',
	'contact_col_1_item_2'			=> '1006 Lausanne',
	'contact_col_1_item_3'			=> 'Switzerland',

	'contact_col_2_label'			=> 'We\'d love to hear from you.',
	'contact_col_2_item_1'			=> 'General Inquiries',
	'contact_col_2_item_2'			=> 'New Business',

	'contact_col_3_label'			=> 'Follow us on',
	'contact_col_4_label'			=> 'See you soon 👋',

];
