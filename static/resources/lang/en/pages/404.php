<?php

return [

	/*
	|--------------------------------------------------------------------------
	| APP DEFAULTS
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Thanks but no thanks',
	'meta_desc' 			=> '',


	'title_element_1'			=>  'Error',
	'title_element_2'			=>  '404',
	'paragraph_information_1'	=>  'we can\'t find the page you\'re looking for',
	'paragraph_information_2'	=>  'It might be better if you return to the homepage ',
	'back_to_home'				=> 	'Yeah, take me there',

	];




