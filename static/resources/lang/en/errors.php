<?php

return [

	/*
	|--------------------------------------------------------------------------
	| ERRORS
	|--------------------------------------------------------------------------
	|
	*/

	'page_error_title' 		=> 'Page error',
	'page_error' 			=> 'This page doesn\'t exists or contains an error.',

	'csrf_token' 			=> 'Seems your session was expired. Please <a href="javascript:window.location.reload()">refresh this page</a>',
	'post_too_large' 		=> 'Files may not be greater than 16Mo.',
	'logged_out' 			=> 'Seems you are logged out now. Please log in again.',
	'new_version' 			=> 'A new version of this platform is available. Please refresh this page.',
	'unknown' 				=> 'Unknown error.',
	'network_lost' 			=> 'The network connection was lost.',

	'not_authorized_title' 	=> 'Unauthorized',
	'not_authorized' 		=> 'This action in unauthorized.',

];
