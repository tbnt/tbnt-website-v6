<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	'accepted'             => 'The :attribute must be accepted.',
	'active_url'           => 'The :attribute is not a valid URL.',
	'after'                => 'The :attribute must be a date after :date.',
	'alpha'                => 'The :attribute may only contain letters.',
	'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
	'alpha_num'            => 'The :attribute may only contain letters and numbers.',
	'array'                => 'The :attribute must be an array.',
	'before'               => 'The :attribute must be a date before :date.',
	'between'              => [
		'numeric' => 'The :attribute must be between :min and :max.',
		'file'    => 'The :attribute must be between :min and :max kilobytes.',
		'string'  => 'The :attribute must be between :min and :max characters.',
		'array'   => 'The :attribute must have between :min and :max items.',
	],
	'boolean'              => 'The :attribute field must be true or false.',
	'confirmed'            => 'The :attribute confirmation does not match.',
	'date'                 => 'The :attribute is not a valid date.',
	'date_format'          => 'The :attribute does not match the format :format.',
	'different'            => 'The :attribute and :other must be different.',
	'digits'               => 'The :attribute must be :digits digits.',
	'digits_between'       => 'The :attribute must be between :min and :max digits.',
	'dimensions'           => 'The :attribute has invalid image dimensions.',
	'distinct'             => 'The :attribute field has a duplicate value.',
	'email'                => 'The :attribute must be a valid email address.',
	'exists'               => 'The selected :attribute is invalid.',
	'file'                 => 'The :attribute must be a file.',
	'filled'               => 'The :attribute field is required.',
	'image'                => 'The :attribute must be an image.',
	'in'                   => 'The selected :attribute is invalid.',
	'in_array'             => 'The :attribute field does not exist in :other.',
	'integer'              => 'The :attribute must be an integer.',
	'ip'                   => 'The :attribute must be a valid IP address.',
	'json'                 => 'The :attribute must be a valid JSON string.',
	'max'                  => [
		'numeric' => 'The :attribute may not be greater than :max.',
		'file'    => 'The :attribute may not be greater than :max kilobytes.',
		'string'  => 'The :attribute may not be greater than :max characters.',
		'array'   => 'The :attribute may not have more than :max items.',
	],
	'mimes'                => 'The :attribute must be a file of type: :values.',
	'mimetypes'            => 'The :attribute must be a file of type: :values.',
	'min'                  => [
		'numeric' => 'The :attribute must be at least :min.',
		'file'    => 'The :attribute must be at least :min kilobytes.',
		'string'  => 'The :attribute must be at least :min characters.',
		'array'   => 'The :attribute must have at least :min items.',
	],
	'not_in'               => 'The selected :attribute is invalid.',
	'numeric'              => 'The :attribute must be a number.',
	'present'              => 'The :attribute field must be present.',
	'regex'                => 'The :attribute format is invalid.',
	'required'             => 'The :attribute field is required.',
	'required_if'          => 'The :attribute field is required when :other is :value.',
	'required_unless'      => 'The :attribute field is required unless :other is in :values.',
	'required_with'        => 'The :attribute field is required when :values is present.',
	'required_with_all'    => 'The :attribute field is required when :values is present.',
	'required_without'     => 'The :attribute field is required when :values is not present.',
	'required_without_all' => 'The :attribute field is required when none of :values are present.',
	'same'                 => 'The :attribute and :other must match.',
	'size'                 => [
		'numeric' => 'The :attribute must be :size.',
		'file'    => 'The :attribute must be :size kilobytes.',
		'string'  => 'The :attribute must be :size characters.',
		'array'   => 'The :attribute must contain :size items.',
	],
	'string'               => 'The :attribute must be a string.',
	'timezone'             => 'The :attribute must be a valid zone.',
	'unique'               => 'The :attribute has already been taken.',
	'uploaded'             => 'The :attribute failed to upload.',
	'url'                  => 'The :attribute format is invalid.',

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'geocode_success'       => 'The address could not be geocoded.',
	'file_exists'           => 'The file does not exist.',
	'image_exists'          => 'The image does not exist.',
	'lang_exists'           => 'The language does not exist.',
	'lang_code_exists'      => 'The language does not exist.',
	'time_format'           => 'The :attribute does not match the format "hh:mm".',

	'blog_exists'           => 'The blog post does not exist.',
	'blog_url_unique'       => 'The url is already taken by an another blog post.',
	'category_exists'       => 'The category does not exist.',
	'category_url_unique'   => 'The url is already taken by an another category.',

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'lang' => [
			'accepted' => 'At least one language should be specified.'
		],
		'image' => [
			'accepted' => 'The image file is required.',
		],
		'pdf' => [
			'accepted' => 'The PDF file is required.',
		],
		'image.*' => [
			'file' => 'The image must be a file.',
			'dimensions' => 'The image must have a max width/height of 2400px.',
		],
		'files.*' => [
			'max' => 'The file may not be greater than :max kilobytes.',
		],
		'pdf.*' => [
			'file' => 'The pdf must be a file.',
		],
		'captcha_response' => [
			'required' => 'The captcha is required.',
			'string' => 'The captcha is invalid.',
		],
		'deleted_files' => [
			'array' => 'Les :attribute is not in a correct format.',
		],
		'langs_ids' => [
			'array' => 'The :attribute is not in a correct format.',
		],
		'users_ids' => [
			'array' => 'The :attribute is not in a correct format.',
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [
		'deleted_files' => 'deleted files',
		'deleted_files.*' => 'deleted file',
		'date' => 'date',
		'domain' => 'domain',
		'email' => 'email',
		'file' => 'file',
		'files.*' => 'file',
		'image' => 'image',
		'is_active' => 'is active',
		'is_admin' => 'administrator',
		'lang_id' => 'language',
		'langs_ids' => 'languages',
		'langs_ids.*' => 'language',
		'language_id' => 'language',
		'name' => 'name',
		'new_password' => 'new password',
		'password' => 'password',
		'phone' => 'phone',
		'position' => 'position',
		'status' => 'status',
		'time_end' => 'time end',
		'time_start' => 'time start',
		'title' => 'title',
		'type' => 'type',
		'url' => 'url',
		'user_id' => 'user',
		'username' => 'username',
		'users_ids' => 'users',
		'users_ids.*' => 'user',
		'website' => 'website',
	],

];
