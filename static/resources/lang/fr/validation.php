<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	'accepted'             => 'Le champ :attribute doit être accepté.',
	'active_url'           => 'Le champ :attribute n\'est pas une URL valide.',
	'after'                => 'Le champ :attribute doit être une date postérieur à :date.',
	'alpha'                => 'Le champ :attribute ne doit contenir que les lettres.',
	'alpha_dash'           => 'Le champ :attribute ne doit contenir que des letters, nombres et des tirets.',
	'alpha_num'            => 'Le champ :attribute ne doit contenir que des lettre et nombres.',
	'array'                => 'Le champ :attribute doit être un tableau.',
	'before'               => 'Le champ :attribute doit être une date antérieur à :date.',
	'between'              => [
		'numeric' => 'Le champ :attribute doit être entre :min et :max.',
		'file'    => 'Le champ :attribute doit être entre :min et :max kilobytes.',
		'string'  => 'Le champ :attribute doit être entre :min et :max caractèrs.',
		'array'   => 'Le champ :attribute doit contenir entre :min et :max éléments.',
	],
	'boolean'              => 'Le champ :attribute doit être vrai ou faux.',
	'confirmed'            => 'Le champ :attribute ne correspond pas à la confirmation.',
	'date'                 => 'Le champ :attribute n\'est pas une date valide.',
	'date_format'          => 'Le champ :attribute ne correspond pas au format :format.',
	'different'            => 'Le champ :attribute doit être différent du champ :other.',
	'digits'               => 'Le champ :attribute doit être un nombre à :digits chiffres.',
	'digits_between'       => 'Le champ :attribute doit être un nombre entre :min et :max chiffres.',
	'dimensions'           => 'Le champ :attribute a une dimension invalide.',
	'distinct'             => 'Le champ :attribute a une valeur double.',
	'email'                => 'Le champ :attribute doit être une adresse email valide.',
	'exists'               => 'Le champ :attribute sélectionné est invalide.',
	'file'                 => 'Le champ :attribute doit être un fichier.',
	'filled'               => 'Le champ :attribute est requis.',
	'image'                => 'Le champ :attribute doit être une image.',
	'in'                   => 'Le champ :attribute a un élément invalide.',
	'in_array'             => 'Le champ :attribute n\'existe pas dans le champ tableau :other.',
	'integer'              => 'Le champ :attribute doit être un entier.',
	'ip'                   => 'Le champ :attribute doit être une adresse IP valide.',
	'json'                 => 'Le champ :attribute doit être au format JSON valide.',
	'max'                  => [
		'numeric' => 'Le champ :attribute ne doit pas être plus grand que :max.',
		'file'    => 'Le champ :attribute ne doit pas être plus grand que :max kilobytes.',
		'string'  => 'Le champ :attribute ne doit pas être plus grand que :max caractèrs.',
		'array'   => 'Le champ :attribute ne doit pas avoir plus de :max éléments.',
	],
	'mimes'                => 'Le champ :attribute doit être un fichier de type :values.',
	'mimetypes'            => 'Le champ :attribute doit être un fichier de type :values.',
	'min'                  => [
		'numeric' => 'Le champ :attribute doit être au moins :min.',
		'file'    => 'Le champ :attribute doit être au moins :min kilobytes.',
		'string'  => 'Le champ :attribute doit être au moins :min caractèrs.',
		'array'   => 'Le champ :attribute doit avoir au moins :min éléments.',
	],
	'not_in'               => 'Le champ :attribute sélectionné est invalide.',
	'numeric'              => 'Le champ :attribute doit être au format numérique.',
	'present'              => 'Le champ :attribute doit être présent.',
	'regex'                => 'Le champ :attribute a un format invalide.',
	'required'             => 'Le champ :attribute est requis.',
	'required_if'          => 'Le champ :attribute est requis quand :other égale à :value.',
	'required_unless'      => 'Le champ :attribute est requis sauf si :other est dans :values.',
	'required_with'        => 'Le champ :attribute est requis quand :values est présent.',
	'required_with_all'    => 'Le champ :attribute est requis quand :values sont présents.',
	'required_without'     => 'Le champ :attribute est requis quand :values n\'est pas présent.',
	'required_without_all' => 'Le champ :attribute est requis quand :values ne sont pas présents.',
	'same'                 => 'Le champ :attribute et :other doivent correspondre.',
	'size'                 => [
		'numeric' => 'Le champ :attribute doit être de :size.',
		'file'    => 'Le champ :attribute doit être de :size kilobytes.',
		'string'  => 'Le champ :attribute doit être de :size caractèrs.',
		'array'   => 'Le champ :attribute doit contenir :size éléments.',
	],
	'string'               => 'Le champ :attribute doit être une chaine de caractères.',
	'timezone'             => 'Le champ :attribute doit être une zone valide.',
	'unique'               => 'Le champ :attribute est déjà utilisé.',
	'uploaded'             => 'Le champ :attribute a échoué lors de l\'upload.',
	'url'                  => 'Le champ :attribute a un format invalide.',

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'geocode_success'       => 'L\'adresse n\'a pas pu être géocodée.',
	'file_exists'           => 'Le fichier n\'existe pas.',
	'image_exists'          => 'L\'image n\'existe pas.',
	'lang_exists'           => 'La langue n\'existe pas.',
	'lang_code_exists'      => 'La langue n\'existe pas.',
	'time_format'           => 'L\'heure doit être au format "hh:mm".',

	'blog_exists'           => 'Le post n\'existe pas.',
	'blog_url_unique'       => 'L\'url est déjà assignée à un autre post.',
	'category_exists'       => 'La catégorie n\'existe pas.',
	'category_url_unique'   => 'L\'url est déjà assignée à une autre catégorie.',

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => [
		'lang' => [
			'accepted' => 'Au moins une langue doit être spécifiée.'
		],
		'image' => [
			'accepted' => 'Une image est requise.',
		],
		'pdf' => [
			'accepted' => 'Une fichier PDF est requis.',
		],
		'image.*' => [
			'file' => 'L\'image doit être un fichier image.',
			'dimensions' => 'L\'image doit avoir une largeur/hauter max de 2400px.',
		],
		'files.*' => [
			'max' => 'Le fichier ne doit pas être plus grand que :max kilobytes.',
		],
		'pdf.*' => [
			'file' => 'Le PDF doit être un fichier PDF.',
		],
		'captcha_response' => [
			'required' => 'Le captcha est requis.',
			'string' => 'Le captcha est invalide.',
		],
		'deleted_files' => [
			'array' => 'Le champ :attribute n\'est pas au bon format.',
		],
		'langs_ids' => [
			'array' => 'Le champ :attribute n\'est pas au bon format.',
		],
		'users_ids' => [
			'array' => 'Le champ :attribute n\'est pas au bon format.',
		],
	],

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => [
		'deleted_files' => 'fichiers supprimés',
		'deleted_files.*' => 'fichier supprimé',
		'date' => 'date',
		'domain' => 'domaine',
		'email' => 'email',
		'file' => 'fichier',
		'files.*' => 'fichier',
		'image' => 'image',
		'is_active' => 'actif',
		'is_admin' => 'administrateur',
		'lang_id' => 'langue',
		'langs_ids' => 'langues',
		'langs_ids.*' => 'langue',
		'language_id' => 'langue',
		'name' => 'nom',
		'new_password' => 'nouveau mot de passe',
		'password' => 'mot de passe',
		'phone' => 'téléphone',
		'position' => 'position',
		'status' => 'statut',
		'time_end' => 'heure de fin',
		'time_start' => 'heure de début',
		'title' => 'titre',
		'type' => 'type',
		'url' => 'url',
		'user_id' => 'utilisateur',
		'username' => 'nom d\'utilisateur',
		'users_ids' => 'utilisateurs',
		'users_ids.*' => 'utilisateur',
		'website' => 'site internet',
	],

];
