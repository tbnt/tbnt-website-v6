<?php

return [

	/*
	|--------------------------------------------------------------------------
	| APP DEFAULTS
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'meta_title' 			=> 'Agence de développement web & applications mobile à Lausanne - Thanks but no thanks',
	'meta_desc' 			=> 'Notre agence web basée à Lausanne conçoit et développe des expériences digitales uniques.  Contactez-nous pour nous faire part de votre projet !',

	];
