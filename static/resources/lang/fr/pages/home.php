<?php

return [

	/*
	|--------------------------------------------------------------------------
	| HOME
	|--------------------------------------------------------------------------
	|
	| Custom
	|
	*/

	'menu_item_1'					=> 'Version VR',
	'menu_item_2'					=> 'l\'avent UI kit',
	'menu_item_1_tag'				=> 'Disponible bientôt!',

	'meta_title' 					=> 'Agence de développement web & applications mobile à Lausanne - Thanks but no thanks ',
	'meta_desc' 					=> 'Notre agence web basée à Lausanne conçoit et développe des expériences digitales uniques.  Contactez-nous pour nous faire part de votre projet !',

	'title_element_1'				=> 'DIFF',
	'title_element_2'				=> 'ERENT',
	'title_element_3'				=> 'IS',
	'title_element_4'				=> 'BETTER',

	'intro_img_subtitle'			=> 'public/images/intro/subtitle_fr.svg',
	'intro_presentation_tbnt_1'		=> 'Nous sommes des milléniaux et nous avons grandi avec la transformation numérique. Nous avons évolué entourés d\'interfaces.',
	'intro_presentation_tbnt_2'		=> 'Nous contestons le statu quo et créons des expériences digitales uniques et sur mesure pour réaliser vos projets les plus ambitieux.',

	'reel_title_element_1'			=> 'SHOW',
	'reel_title_element_2'			=> 'REEL',

	'client_title_element_1'		=> 'NOS',
	'client_title_element_2'		=> 'CLIENTS',
	'client_many_more'				=> 'Ils nous font confiance',
	'client_working_for'			=> 'Nous travaillons sur',
	'client_cta'					=> 'Êtes-vous le suivant ?',

	'client_title_1'				=> 'Paleo',
	'client_desc_1'					=> 'Gestion médias des festivals à travers un outil simple et puissant',
	'client_tags_1'					=> 'Travail effectué : UX / UI, custom dev.',

	'client_title_2'				=> 'Ikentoo',
	'client_desc_2'					=> 'Fournir les performances des établissements en temps réel',
	'client_tags_2'					=> 'Travail effectué : UX / UI, react native dev.',

	'client_title_3'				=> 'Jumpfax ',
	'client_desc_3'					=> 'Une connexion simple et efficace entre les cavaliers et l’univers du jumping',
	'client_tags_3'					=> 'Travail effectué : UX / UI, custom dev., native iOS and Android',

	'client_title_4'				=> 'Gottex Brokers',
	'client_desc_4'					=> 'Découvrir la finance sous un autre angle.',
	'client_tags_4'					=> 'Travail effectué : Rebranding, UX/UI, content production, custom development',

	'client_title_5'				=> 'Timea b.',
	'client_desc_5'					=> 'Renforcer la présence digitale de la tenniswoman professionnelle',
	'client_tags_5'					=> 'Travail effectué : Rebranding, UX/UI, content production, custom development',

	'client_title_6'				=> 'MICR',
	'client_desc_6'					=> 'Une chaîne digitale pour la dignité humaine.',
	'client_tags_6'					=> 'Travail effectué : UX / UI, web and mobile dev.',

	'client_title_7'				=> 'Montreux Jazz Insider',
	'client_desc_7'					=> 'Refonte de Montreux Jazz Insider — une application pour les abonnés qui rassemble les fans du festival et donne accès à des avantages et des news exclusives.',
	'client_tags_7'					=> 'Travail effectué : UX, UI, react native development.',

	'service_title_element_1'		=> 'Nos',
	'service_title_element_2'		=> 'Services',

	'service_col_1_title'			=> 'Nous pensons',
	'service_col_2_title'			=> 'Nous concevons',
	'service_col_3_title'			=> 'Nous développons',
	'service_col_4_title'			=> 'Nous utilisons',

	'service_col_1_label_1'			=> 'Recherches sur les acteurs du marché et démographiques',
	'service_col_1_desc_1'			=> 'Afin de récolter le plus d’informations sur votre industrie et votre audience',
	'service_col_1_label_2'			=> 'Stratégie digitale',
	'service_col_1_desc_2'			=> 'Afin de planifier et communiquer correctement.',
	'service_col_1_label_3'			=> 'Approche Agile (Scrum)',
	'service_col_1_desc_3'			=> 'Afin de tester et valider vos hypothèses.',

	'service_col_2_label_1'			=> 'Votre identité visuelle',
	'service_col_2_desc_1'			=> 'Afin de donner envie.',
	'service_col_2_label_2'			=> 'Votre expérience utilisateur',
	'service_col_2_desc_2'			=> 'Pour convertir vos visiteurs en futurs clients.',
	'service_col_2_label_3'			=> 'Votre interface graphique',
	'service_col_2_desc_3'			=> 'La raison pour laquelle ils vont revenir.',

	'service_col_3_label_1'			=> 'Des sites web et des plateformes sur mesure',
	'service_col_3_desc_1'			=> 'Pour répondre parfaitement à vos besoins.',
	'service_col_3_label_2'			=> 'Des applications natives ou hybrides',
	'service_col_3_desc_2'			=> 'Parce que votre marché est mobile.',
	'service_col_3_label_3'			=> 'Des solutions de boutiques en ligne',
	'service_col_3_desc_3'			=> 'Pour rendre la prochaine commande à portée de clic.',

	'service_col_4_label_1'			=> 'Le SEO',
	'service_col_4_desc_1'			=> 'Afin de vous positionner sur les moteurs dès le début',
	'service_col_4_label_2'			=> 'La promotion sur les réseaux sociaux',
	'service_col_4_desc_2'			=> 'Afin de cibler votre audience et de mettre en avant votre marque',
	'service_col_4_label_3'			=> 'La publicité sur Google',
	'service_col_4_desc_3'			=> 'Afin de générer du trafic instantanément sur votre site web',

	'service_col_4_brandsup'		=> 'En collaboration avec',

	'team_title_element_1'			=> 'Notre',
	'team_title_element_2'			=> 'Equipes',

	'team_1_work'					=> 'Cofondateur & Gestion d\'équipes',
	'team_2_work'					=> 'Cofondateur & developpeur',
	'team_3_work'					=> 'Digital designer',
	'team_4_work'					=> 'Developpeur Full stack',
	'team_button'					=> 'Ecouter sur Spotify',

	'team_thanks'					=> 'Remerciements particuliers à toutes les personnes qui ont contribué au développement de TBNT.',

	'contact_title_element_1'		=> 'Say',
	'contact_title_element_2'		=> 'Hello?',
	'contact_col_1_label'			=> 'Voulez-vous passer ?',
	'contact_col_1_item_1'			=> 'Avenue d\'Ouchy 4',
	'contact_col_1_item_2'			=> '1006 Lausanne',
	'contact_col_1_item_3'			=> 'Suisse',

	'contact_col_2_label'			=> 'Nous serions heureux de vous rencontrer',
	'contact_col_2_item_1'			=> 'Demande générale',
	'contact_col_2_item_2'			=> 'Nouveau Business',

	'contact_col_3_label'			=> 'Suivez-nous sur :',
	'contact_col_4_label'			=> 'A très vite 👋',

];
