<?php

return [

	/*
	|--------------------------------------------------------------------------
	| URLS ADMIN
	|--------------------------------------------------------------------------
    |
    | Custom
    |
	*/

	'home' 					=> '/',

	'login' 				=> 'login',
	'logout' 				=> 'logout',

	'geocode' 				=> 'geocode',

	'categories' 			=> 'categories',
	'category_new' 			=> 'category/new',
	'category_edit' 		=> 'category/:id/edit',
	'category_delete' 		=> 'category/:id/delete',

	'blogs' 				=> 'blogs',
	'blog_new' 				=> 'blog/new',
	'blog_edit' 			=> 'blog/:id/edit',
	'blog_preview' 			=> 'blog/:id/preview',
	'blog_duplicate' 		=> 'blog/:id/duplicate',
	'blog_delete' 			=> 'blog/:id/delete',

	'settings' 				=> 'settings',
	'settings_edit' 		=> 'settings/edit',

];
