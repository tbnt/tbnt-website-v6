<?php

return [

	/*
	|--------------------------------------------------------------------------
	| DATES
	|
	| PHP - http://php.net/manual/en/function.strftime.php
	| JS - https://momentjs.com/docs/#/displaying
	|--------------------------------------------------------------------------
    |
    | Custom
    |
	*/

	'lmdhm' 				=> '%A %d.%m at %H:%M',
	'lmd' 					=> '%A %d.%m',
	'lmy' 					=> '%d %B %Y',
	'l' 					=> '%A',
	'md' 					=> '%d.%m',

	'js' => [
		'lmdhm' 			=> 'dddd DD.MM at HH:mm',
		'lmd' 				=> 'dddd DD.MM',
		'md' 				=> 'DD.MM',
		'mdy' 				=> 'DD.MM.YYYY',
	],

	'fp' => [
		'lmd' 				=> 'j F Y',
	],

	'time_format' 			=> 'Format "hh:mm", ex: "23:45"',

];
