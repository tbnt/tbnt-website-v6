@extends('emails.app', [
'festival' => $festival,
'subject' => trans('emails/user-notifications.subject', ['name' => $festival->name]),
])

@section('content')
{{ trans('emails/user-notifications.title') }}
<br><br>

{{ trans('emails/user-notifications.message', ['name' => $festival->name]) }}
<br>
{!! implode('<br>', array_map(function($notification) { return strip_tags($notification->message); }, $notifications)) !!}
<br><br>

@include('emails.app-button', ['url' => $redirect_url, 'text' => trans('emails/user-notifications.activity')])
@endsection
