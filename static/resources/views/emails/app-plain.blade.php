@yield('content')

{{ trans('emails.end_message', ['name' => $festival->name ?? '']) }}

{{ trans('emails.copyright') }}
