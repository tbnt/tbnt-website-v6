@extends('emails.app', [
'festival' => $festival,
'subject' => trans('emails/user-invite.subject', ['name' => $festival->name]),
])

@section('content')
{{ trans('emails/user-invite.title') }}
<br><br>

{{ trans('emails/user-invite.message', ['name' => $festival->name]) }}
{{ trans('emails/user-invite.link', ['duration' => $duration]) }}
<br><br>

@include('emails.app-button', ['url' => $activation_link, 'text' => trans('emails/user-invite.activate')])
@endsection
