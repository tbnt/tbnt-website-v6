<!-- Button : Begin -->
<table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" style="margin: auto;">
	<tr>
		<td style="border-radius: 3px; background: #1e87f0; text-align: center;" class="button-td">
			<a class="fallback-text" href="{{ $url }}" style="background: #1e87f0; border: 15px solid #1e87f0; font-family: 'Open Sans', sans-serif; font-size: 13px; line-height: 1.1; text-align: center; text-decoration: none; display: block; font-weight: bold;" class="button-a">
				<span style="color:#ffffff;" class="button-link">&nbsp;&nbsp;&nbsp;&nbsp;{{ $text }}&nbsp;&nbsp;&nbsp;&nbsp;</span>
			</a>
		</td>
	</tr>
</table>
<!-- Button : END -->
