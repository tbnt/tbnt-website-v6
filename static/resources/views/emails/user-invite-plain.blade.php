@extends('emails.app-plain', [
'festival' => $festival,
])

@section('content')
{{ trans('emails/user-invite.title') }}

{{ trans('emails/user-invite.message', ['name' => $festival->name]) }}

{{ trans('emails/user-invite.link', ['duration' => $duration]) }}
{{ $activation_link }}
@endsection
