@extends('emails.app', [
'festival' => $festival,
'subject' => trans('emails/admin-user-invite.subject', ['name' => $festival->name]),
])

@section('content')
{{ trans('emails/admin-user-invite.title') }}
<br><br>

{{ trans('emails/admin-user-invite.message', ['name' => $festival->name]) }}
{{ trans('emails/admin-user-invite.link', ['duration' => $duration]) }}
<br><br>

@include('emails.app-button', ['url' => $activation_link, 'text' => trans('emails/admin-user-invite.activate')])
@endsection
