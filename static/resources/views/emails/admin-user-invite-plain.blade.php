@extends('emails.app-plain', [
'festival' => $festival,
])

@section('content')
{{ trans('emails/admin-user-invite.title') }}

{{ trans('emails/admin-user-invite.message', ['name' => $festival->name]) }}

{{ trans('emails/admin-user-invite.link', ['duration' => $duration]) }}
{{ $activation_link }}
@endsection
