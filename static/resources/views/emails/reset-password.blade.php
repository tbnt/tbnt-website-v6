@extends('emails.app', [
'festival' => $festival,
'subject' => trans('emails/reset-password.subject', ['name' => $festival->name]),
])

@section('content')
{{ trans('emails/reset-password.title') }}
<br><br>

{{ trans('emails/reset-password.message', ['name' => $festival->name]) }}
{{ trans('emails/reset-password.link', ['duration' => $duration]) }}
<br><br>

@include('emails.app-button', ['url' => $recovery_link, 'text' => trans('emails/reset-password.reset')])
@endsection
