@extends('emails.app', [
'festival' => $festival,
'subject' => trans('emails/links-artist.subject', ['name' => $festival->name]),
])

@section('content')
@define(
	$is_link_artist_1 = trim($concert->link_artist_1) !== '';
	$is_link_artist_2 = trim($concert->link_artist_2) !== '';
)

{{ trans('emails/links-artist.title') }}
<br><br>

{{ trans('emails/links-artist.message', ['name' => $festival->name]) }}
<br>
{{ trans('emails/links-artist.deadline', ['date' => lang_date($concert->link_artist_download_date, 'dates.lmd')]) }}
<br><br>

@if ($is_link_artist_1 === true)
	{{ trans('emails/links-artist.link_concert') }}<br>
	<a href="{{ $concert->link_artist_1 }}">{{ $concert->link_artist_1 }}</a>
	@if ($is_link_artist_2 === true)<br><br>@endif
@endif

@if ($is_link_artist_2 === true)
	{{ trans('emails/links-artist.link_video') }}<br>
	<a href="{{ $concert->link_artist_2 }}">{{ $concert->link_artist_2 }}</a>
@endif
@endsection
