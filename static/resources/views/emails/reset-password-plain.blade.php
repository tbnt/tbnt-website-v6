@extends('emails.app-plain', [
'festival' => $festival,
])

@section('content')
{{ trans('emails/reset-password.title') }}

{{ trans('emails/reset-password.message') }}

{{ trans('emails/reset-password.link', ['duration' => $duration]) }}
{{ $recovery_link }}
@endsection
