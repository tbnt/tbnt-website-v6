@extends('emails.app', [
'festival' => $festival,
'subject' => trans('emails/links-tv-rights.subject', ['name' => $festival->name]),
])

@section('content')
@define(
	$is_link_tv_rights_1 = trim($concert->link_tv_rights_1) !== '';
	$is_link_tv_rights_2 = trim($concert->link_tv_rights_2) !== '';
	$are_links_songs = is_array($concert->songs ?? []) && count($concert->songs ?? []);
)

{{ trans('emails/links-tv-rights.title') }}
<br><br>

{{ trans('emails/links-tv-rights.message', ['name' => $festival->name]) }}
<br>
{{ trans('emails/links-tv-rights.deadline', ['date' => lang_date($concert->link_tv_rights_download_date, 'dates.lmd')]) }}
<br><br>

@if ($is_link_tv_rights_1 === true)
	{{ trans('emails/links-tv-rights.link_audio') }}<br>
	<a href="{{ $concert->link_tv_rights_1 }}">{{ $concert->link_tv_rights_1 }}</a>
	@if ($is_link_tv_rights_2 === true)<br><br>@endif
@endif

@if ($is_link_tv_rights_2 === true)
	{{ trans('emails/links-tv-rights.link_video') }}<br>
	<a href="{{ $concert->link_tv_rights_2 }}">{{ $concert->link_tv_rights_2 }}</a>
	@if ($are_links_songs === true)<br><br>@endif
@endif

@if ($are_links_songs === true)
	{{ trans('emails/links-tv-rights.link_songs') }}
	@foreach ($concert->songs as $song)
		<br><a href="{{ $song->url }}">{{ $song->title }}</a>
	@endforeach
@endif
@endsection
