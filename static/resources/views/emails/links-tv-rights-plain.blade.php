@extends('emails.app-plain', [
'festival' => $festival,
])

@section('content')
{{ trans('emails/links-tv-rights.title') }}

{{ trans('emails/links-tv-rights.message', ['name' => $festival->name]) }}
{{ trans('emails/links-tv-rights.deadline', ['date' => lang_date($concert->link_tv_rights_download_date, 'dates.lmd')]) }}
@if (trim($concert->link_tv_rights_1) !== '')

{{ trans('emails/links-tv-rights.link_audio') }}
{{ $concert->link_tv_rights_1 }}
@endif
@if (trim($concert->link_tv_rights_2) !== '')

{{ trans('emails/links-tv-rights.link_video') }}
{{ $concert->link_tv_rights_2 }}
@endif
@if (is_array($concert->songs ?? []) && count($concert->songs ?? []))

{{ trans('emails/links-tv-rights.link_songs') }}
@foreach ($concert->songs ?? [] as $song)
{{ $song->url }}
@endforeach
@endif
@endsection
