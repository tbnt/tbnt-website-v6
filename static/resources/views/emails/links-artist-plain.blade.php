@extends('emails.app-plain', [
'festival' => $festival,
])

@section('content')
{{ trans('emails/links-artist.title') }}

{{ trans('emails/links-artist.message', ['name' => $festival->name]) }}
{{ trans('emails/links-artist.deadline', ['date' => lang_date($concert->link_artist_download_date, 'dates.lmd')]) }}
@if (trim($concert->link_artist_1) !== '')

{{ trans('emails/links-artist.link_concert') }}
{{ $concert->link_artist_1 }}
@endif
@if (trim($concert->link_artist_2) !== '')

{{ trans('emails/links-artist.link_video') }}
{{ $concert->link_artist_2 }}
@endif
@endsection
