@extends('emails.app-plain', [
'festival' => $festival,
])

@section('content')
{{ trans('emails/user-notifications.title') }}

{{ trans('emails/user-notifications.message', ['name' => $festival->name]) }}
{{ implode(PHP_EOL, array_map(function($notification) { return strip_tags($notification->message); }, $notifications)) }}

{{ trans('emails/user-notifications.activity') }}
{{ $redirect_url }}
@endsection
