<!DOCTYPE html>
<html>
<head>
	<base href="{{ $app_url }}/" />

	<meta charset="utf-8" />
	<meta http-equiv="Content-Language" content="en">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="HandheldFriendly" content="true">
	<meta name="format-detection" content="telephone=no" />
	<meta name="google" content="notranslate">

	<title>{{ $app_name }} Admin | {{ $social['title'] or '' }}</title>

	{{-- FAVICON --}}
	<link rel="icon" type="image/png" href="{{ url('public/images/favicon/favicon-32x32.png') }}" sizes="32x32">
	<link rel="icon" type="image/png" href="{{ url('public/images/favicon/favicon-16x16.png') }}" sizes="16x16">

	{{-- CSS --}}
	<link rel="stylesheet" type="text/css" href="public/admin/styles/vendor.css?_={{ date('U') }}" />

	{{-- JS --}}
	@include('admin.includes.js')

	<script src="public/admin/scripts/vendor.js?_={{ date('U') }}"></script>
</head>
<body class="uk-background-light">
	@include('admin.includes.loader')

	<main id="app">
		@if ($is_logged_in ?? false)
			@include('admin.includes.menu')
		@endif

		@yield('content')

		<script>
			window.Laravel.js = 'app {{ $is_logged_in ? 'app.admin' : '' }} {{ $js ?? '' }}';
			window.Laravel.menu = '{{ $menu ?? '' }}';
			window.Laravel.version = '{{ env('PROJECT_VERSION', 0) }}';
		</script>
	</main>

	@if ($is_logged_in ?? false)
		@include('admin.modals.logged-out')
		@include('admin.modals.new-version')
	@endif

	<noscript>
		@include('admin.includes.nojs')
	</noscript>
	<script>
		if (Function('/*@cc_on return document.documentMode===10@*/')())
			document.write('{!! get_view('admin.includes.browsehappy') !!}');
	</script>
</body>
</html>
