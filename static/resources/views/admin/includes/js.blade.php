<script>
	window.Laravel = @define(
		$filename = pathinfo(resource_path().'/lang/'.$app_lang->code.'/urls_admin')['filename'];
		$trans = [$filename => trans($filename)];

		echo json_encode([
			'app' => ['name' => $app_name, 'url' => $app_url, 'base' => $app_base],
			'dir' => ['images' => dir_images(''), 'files' => dir_files('')],
			'token' => csrf_token(), 'lang' => $app_lang->toArray(), 'trans' => $trans,
		]);
	);

	window._scripts = {};
</script>
