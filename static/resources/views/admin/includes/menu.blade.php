<div id="navbar" class="uk-navbar-container" uk-sticky="media:1200">
	<div class="uk-container">
		<nav uk-navbar="mode:click; offset:-10; boundary:.uk-navbar-container;">
			<div class="uk-navbar-left uk-padding-remove-left uk-padding-right uk-width-1-1 uk-width-auto@m">
				<div class="uk-navbar-item uk-width-1-1 uk-width-auto@m uk-flex-left">
					<a pjax href="admin" class="logo-link uk-link-reset uk-height-1-1 uk-width-1-1 uk-width-auto@m">
						<div class="uk-flex uk-flex-middle uk-height-1-1 uk-width-1-1 uk-width-auto@m">
							<span class="uk-text-bold">{{ app_name() }} admin</span>
						</div>
					</a>
				</div>
				<ul class="uk-visible@m uk-navbar-nav">
					<li><a pjax href="{{ lang_admin_url('blogs') }}">Blog</a></li>
					<li><a pjax href="{{ lang_admin_url('categories') }}">Categories</a></li>
				</ul>
			</div>
			<div class="uk-navbar-right">
				<ul class="uk-navbar-nav">
					<li class="uk-hidden@m">
						<a href="#">
							<i class="ionicon ionicon-medium ion-drag"></i>
						</a>
						<div class="uk-navbar-dropdown">
							<ul class="uk-nav uk-navbar-dropdown-nav">
								<li><a pjax href="{{ lang_admin_url('blogs') }}">Blog</a></li>
								<li><a pjax href="{{ lang_admin_url('categories') }}">Categories</a></li>
							</ul>
						</div>
					</li>
					<li>
						<a href="{{ $app_url }}" target="_blank">
							{{ preg_replace('/(http(s)?:\/\/)/i', '', $app_url) }}
						</a>
					</li>
					<li>
						<a href="#">
							<i class="ionicon ionicon-medium ion-person"></i>
						</a>
						<div class="uk-navbar-dropdown">
							<ul class="uk-nav uk-navbar-dropdown-nav">
								<li><a pjax href="{{ lang_admin_url('settings') }}">Settings</a></li>
								<li class="uk-nav-divider"></li>
								<li><a href="{{ lang_admin_url('logout') }}" data-confirm="Would you like to logout?">Logout</a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</nav>
	</div>
</div>
