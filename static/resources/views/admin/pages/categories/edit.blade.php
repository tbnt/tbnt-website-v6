@extends('admin.app', [
'js' => 'category.update',
'social' => [
	'title' => 'Edit category',
],
])

@section('content')
<div id="page-category-edit">
	<div class="uk-padding-small-top uk-background-default">
		<div class="uk-container">
			<div class="uk-margin-medium-bottom uk-grid-small" uk-grid>
				<div class="uk-width-1-1 uk-width-expand@s">
					<div class="uk-flex uk-flex-row">
						<div class="uk-padding-small-right">
							<i class="ionicon ionicon-big ion-android-arrow-back uk-text-primary uk-icon-link" data-href="{{ lang_admin_url('categories') }}"></i>
						</div>
						<div>
							<h1 class="uk-margin-remove">Edit category</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="uk-container uk-padding-vertical">
		<div class="uk-form-stacked">
			<form method="post" enctype="multipart/form-data" data-form-category>
				<div class="uk-margin-medium uk-grid-small" uk-grid>
					<div class="uk-width-1-1 uk-width-1-2@m">
						@foreach ($app_langs as $lang)
							<div class="uk-margin-bottom">
								<label class="uk-form-label">Name ({{ $lang->code }}) @include('fields.required')</label>
								<div class="uk-form-controls">
									@include('fields.card-input', [
										'input' => [
											'name' => 'langs['.$lang->code.'][name]',
											'value' => $category->langs[$lang->code]->name,
										],
									])
								</div>
							</div>
						@endforeach
					</div>
					<div class="uk-width-1-1 uk-width-1-2@m">
						@foreach ($app_langs as $lang)
							<div class="uk-margin-bottom">
								<label class="uk-form-label">URL ({{ $lang->code }}) @include('fields.required')</label>
								<div class="uk-form-controls">
									@include('fields.card-input', [
										'input' => [
											'name' => 'langs['.$lang->code.'][url]',
											'value' => $category->langs[$lang->code]->url,
										],
									])
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</form>
			<div class="uk-text-right">
				<a pjax href="{{ lang_admin_url('categories') }}" type="button" class="uk-button uk-button-default">Cancel</a>
				<button type="submit" class="uk-button uk-button-primary" data-submit-category="edit">Update</button>
				<button type="submit" class="uk-button uk-button-primary" data-submit-category="back">Update and exit</button>
			</div>
		</div>
	</div>
</div>

@script($category, 'CATEGORY')
@endsection
