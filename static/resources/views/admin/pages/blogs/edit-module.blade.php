<div class="module-edit" data-module-edit>
	@if (isset($label))
		<span class="label">{{ $label }}</span>
	@endif

	<span class="ionicon ion-chevron-up" data-title="Move to top" title="Move to top" uk-tooltip data-module-push-up></span>
	<span class="ionicon ion-chevron-down" data-title="Move to bottom" title="Move to bottom" uk-tooltip data-module-push-down></span>
	<span class="ionicon ion-trash-b uk-text-danger" data-title="Delete" title="Delete" uk-tooltip data-module-delete></span>
</div>
