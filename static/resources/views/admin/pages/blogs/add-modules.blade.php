@define(
	$modules_images = [
		'wysiwyg' => 'public/images/icons/blog/wysiwyg.png',
		'wysiwyg_2' => 'public/images/icons/blog/wysiwyg-2-columns.png',
		'wysiwyg_image' => 'public/images/icons/blog/wysiwyg-and-image.png',
		'image_wysiwyg' => 'public/images/icons/blog/image-and-wysiwyg.png',
		'image' => 'public/images/icons/blog/image-full-width.png',
		'image_2' => 'public/images/icons/blog/image-2-columns.png',
		'image_3' => 'public/images/icons/blog/image-3-columns.png',
		'video' => 'public/images/icons/blog/video-full-width.png',
		'quote' => 'public/images/icons/blog/quote.png',
	];
)

<div class="module-add" data-module-add>
	<span class="ion-plus-circled ionicon" data-modules-types-hover></span>

	<div class="modules-types" data-modules-types hidden>
		<div class="modules-types-content">
			@foreach ($modules_types as $module_type)
				<div data-module-type="{{ str_slug($module_type->type) }}">
					<img src="{{ $modules_images[$module_type->type] }}" />
					<span>{{ $module_type->name }}</span>
				</div>
			@endforeach
		</div>
	</div>
</div>
