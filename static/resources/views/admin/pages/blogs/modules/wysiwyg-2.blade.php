@define(
	$text_name_1 = $text_name_1 ?? '';
	$text_value_1 = $text_value_1 ?? '';

	$text_name_2 = $text_name_2 ?? '';
	$text_value_2 = $text_value_2 ?? '';
)

@include('admin.pages.blogs.modules._wysiwyg', [
	'name' => $text_name_1,
	'value' => $text_value_1,
])

@include('admin.pages.blogs.modules._wysiwyg', [
	'name' => $text_name_2,
	'value' => $text_value_2,
])
