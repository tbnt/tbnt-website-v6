@define(
	$text_name_1 = $text_name_1 ?? '';
	$text_value_1 = $text_value_1 ?? '';

	$image_name_1 = $image_name_1 ?? '';
	$image_value_1 = $image_value_1 ?? null;

	$caption_name_1 = $caption_name_1 ?? '';
	$caption_value_1 = $caption_value_1 ?? null;

	$recommended_size = 'XXXxXXX';
)

@include('admin.pages.blogs.modules._wysiwyg', [
	'name' => $text_name_1,
	'value' => $text_value_1,
])

@include('admin.pages.blogs.modules._image', [
	'image_name' => $image_name_1,
	'image_value' => $image_value_1,
	'caption_name' => $caption_name_1,
	'caption_value' => $caption_value_1,
	'recommended_size' => $recommended_size,
])
