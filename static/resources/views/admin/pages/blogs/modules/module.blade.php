@define(
	$module_name = isset($module->id) ? 'modules['.$module->id.']' : 'modules[NEWID]';
	$module_type = $module->module_type->type;

	$module_text_1_name = $module_name.'[text_1]';
	$module_text_1_value = $module->text_1 ?? '';

	$module_text_2_name = $module_name.'[text_2]';
	$module_text_2_value = $module->text_2 ?? '';

	$module_image_1_name = $module_name.'[image_1]';
	$module_image_1_value = $module->image_1 ?? null;

	$module_image_2_name = $module_name.'[image_2]';
	$module_image_2_value = $module->image_2 ?? null;

	$module_image_3_name = $module_name.'[image_3]';
	$module_image_3_value = $module->image_3 ?? null;

	$module_caption_1_name = $module_name.'[caption_1]';
	$module_caption_1_value = $module->caption_1 ?? '';

	$module_caption_2_name = $module_name.'[caption_2]';
	$module_caption_2_value = $module->caption_2 ?? '';

	$module_caption_3_name = $module_name.'[caption_3]';
	$module_caption_3_value = $module->caption_3 ?? '';
)

<div class="modules module-{{ str_slug($module_type) }}">
	<div class="module-content-parent">
		<input type="hidden" name="{{ $module_name }}[type]" value="{{ $module_type }}" />

		@include('admin.pages.blogs.modules.'.str_slug($module_type), [
			'text_name_1' => $module_text_1_name,
			'text_value_1' => $module_text_1_value,
			'text_name_2' => $module_text_2_name,
			'text_value_2' => $module_text_2_value,
			'image_name_1' => $module_image_1_name,
			'image_value_1' => $module_image_1_value,
			'image_name_2' => $module_image_2_name,
			'image_value_2' => $module_image_2_value,
			'image_name_3' => $module_image_3_name,
			'image_value_3' => $module_image_3_value,
			'caption_name_1' => $module_caption_1_name,
			'caption_value_1' => $module_caption_1_value,
			'caption_name_2' => $module_caption_2_name,
			'caption_value_2' => $module_caption_2_value,
			'caption_name_3' => $module_caption_3_name,
			'caption_value_3' => $module_caption_3_value,
		])
	</div>

	@include('admin.pages.blogs.edit-module', ['label' => $module->module_type->name])
	@include('admin.pages.blogs.add-modules')
</div>
