<div class="module-content-item module-content-image">
	<div class="module-tooltip">
		<span class="ionicon ion-information-circled" uk-tooltip="pos:left" data-title="Maximum image dimension is {{ $image_max_dim }}x{{ $image_max_dim }}px<br>Recommended image dimension is {{ $recommended_size or '' }}px"></span>
	</div>

	@include('fields.upload', [
		'name' => $image_name,
		'file' => $image_value,
		'delete' => false,
	])
	@include('fields.card-input', [
		'card' => [
			'class' => 'card-froala',
		],
		'input' => [
			'name' => $caption_name,
			'textarea' => $caption_value,
		],
	])
</div>
