<div class="module-content-item module-content-wysiwyg">
	@include('fields.card-input', [
		'card' => [
			'class' => 'card-froala',
		],
		'input' => [
			'name' => $name,
			'textarea' => $value,
		],
	])
</div>
