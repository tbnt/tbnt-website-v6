<div class="module-content-item module-content-text">
	@include('fields.card-input', [
		'input' => [
			'name' => $name,
			'value' => $value,
			'placeholder' => $placeholder,
		],
	])
</div>
