@define(
	$text_name_1 = $text_name_1 ?? '';
	$text_value_1 = $text_value_1 ?? '';
)

@include('admin.pages.blogs.modules._text', [
	'name' => $text_name_1,
	'value' => $text_value_1,
	'placeholder' => 'Add video url',
])
