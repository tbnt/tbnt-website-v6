@define(
	$edit = $edit ?? false;
)

<div class="sticky-bottom uk-padding-horizontal uk-padding-small-vertical uk-flex uk-flex-between">
	<div class="uk-flex uk-flex-row uk-flex-middle">
		@if ($edit === true)
			<button type="button" class="uk-button uk-button-primary" data-preview-blog>Preview</button>
		@endif

		@include('fields.card-checkbox', [
			'card' => [
				'class' => 'uk-margin-remove-bottom uk-margin-small-left',
			],
			'label' => [
				'active' => 'Published',
				'inactive' => 'Draft',
			],
			'input' => [
				'name' => 'is_active',
				'value' => 1,
				'checked' => $checked,
				'data' => 'data-submit-blog=edit',
			],
		])
	</div>
	<div>
		<a pjax href="{{ lang_admin_url('blogs') }}" type="button" class="uk-button uk-button-default">Cancel</a>

		@if ($edit === false)
			<button type="button" class="uk-button uk-button-primary" data-submit-blog="back">Save</button>
			<button type="button" class="uk-button uk-button-primary" data-submit-blog="edit">Save and edit</button>
		@else
			<button type="button" class="uk-button uk-button-primary" data-submit-blog="edit">Save</button>
			<button type="button" class="uk-button uk-button-primary" data-submit-blog="back">Save and exit</button>
		@endif
	</div>
</div>
