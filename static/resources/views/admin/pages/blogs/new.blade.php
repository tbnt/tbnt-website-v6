@extends('admin.app', [
'js' => 'blog.create',
'social' => [
	'title' => 'New post',
],
])

@section('content')
@define(
	$image_width = 'XXX';
	$image_height = 'XXX';
)

<div id="page-blog-edit">
	<form method="post" enctype="multipart/form-data" data-form-blog>
		<div class="uk-padding-small-top uk-background-default">
			<div class="uk-container">
				<div class="uk-margin-medium-bottom uk-grid-small" uk-grid>
					<div class="uk-width-1-1 uk-width-expand@s">
						<div class="uk-flex uk-flex-row">
							<div class="uk-padding-small-right">
								<i class="ionicon ionicon-big ion-android-arrow-back uk-text-primary uk-icon-link" data-href="{{ lang_admin_url('blogs') }}"></i>
							</div>
							<div>
								<h1 class="uk-margin-remove">New post</h1>
							</div>
						</div>
					</div>
				</div>
				<ul class="uk-border-none uk-tab-padding uk-flex-nowrap uk-overflow-auto" uk-tab="connect:[data-switch]; swiping:false; active:0" data-switcher-id="users-management">
					<li><a href="#">Details</a></li>
				</ul>
			</div>
		</div>
		<div class="uk-container uk-padding-vertical">
			<ul class="uk-switcher uk-margin" data-switch>
				<li>
					<div class="uk-form-stacked">
						<div class="uk-margin-medium uk-grid-small" uk-grid>
							<div class="uk-width-1-1 uk-width-1-2@m">
								<div class="uk-margin-bottom">
									<label class="uk-form-label">Title @include('fields.required')</label>
									<div class="uk-form-controls">
										@include('fields.card-input', [
											'input' => [
												'name' => 'langs[title]',
											],
										])
									</div>
								</div>
								<div class="uk-margin-bottom">
									<label class="uk-form-label">Meta description @include('fields.required')</label>
									<div class="uk-form-controls">
										@include('fields.card-input', [
											'input' => [
												'name' => 'langs[meta_description]',
											],
										])
									</div>
								</div>
								<div class="uk-margin-bottom">
									<label class="uk-form-label">Description</label>
									<div class="uk-form-controls">
										@include('fields.card-input', [
											'card' => [
												'class' => 'card-froala',
											],
											'input' => [
												'name' => 'langs[description]',
												'textarea' => '',
											],
										])
									</div>
								</div>
								<div class="uk-margin-bottom">
									<label class="uk-form-label">Thumbnail @include('fields.required')</label>
									<div class="uk-alert-primary uk-margin-remove-top" uk-alert>
										<p>Maximum image dimension is <span class="uk-text-bold">{{ $image_max_dim }}x{{ $image_max_dim }}px</span></p>
										<p>Recommended image dimension is <span class="uk-text-bold">{{ $image_width }}x{{ $image_height }}px</span></p>
									</div>
									<div class="uk-form-controls">
										@include('fields.upload', [
											'name' => 'thumbnail',
											'delete' => false,
										])
									</div>
								</div>
								<div class="uk-margin-bottom">
									<label class="uk-form-label">Cover @include('fields.required')</label>
									<div class="uk-alert-primary uk-margin-remove-top" uk-alert>
										<p>Maximum image dimension is <span class="uk-text-bold">{{ $image_max_dim }}x{{ $image_max_dim }}px</span></p>
										<p>Recommended image dimension is <span class="uk-text-bold">{{ $image_width }}x{{ $image_height }}px</span></p>
									</div>
									<div class="uk-form-controls">
										@include('fields.upload', [
											'name' => 'cover',
											'delete' => false,
										])
									</div>
								</div>
							</div>
							<div class="uk-width-1-1 uk-width-1-2@m">
								<div class="uk-margin-bottom">
									<label class="uk-form-label">Url @include('fields.required')</label>
									<div class="uk-form-controls">
										@include('fields.card-input', [
											'input' => [
												'name' => 'langs[url]',
											],
										])
									</div>
								</div>
								<div class="uk-margin-bottom">
									<label class="uk-form-label">Language @include('fields.required')</label>
									<div class="uk-form-controls">
										@include('fields.card-input', [
											'input' => [
												'name' => 'lang_id',
												'select' => array_pluck($app_langs, 'name', 'id'),
											],
										])
									</div>
								</div>
								<div class="uk-margin-bottom" name="categories">
									<label class="uk-form-label">Categories</label>
									@foreach ($categories as $category)
										<div class="uk-form-controls">
											@include('fields.card-checkbox', [
												'label' => [
													'active' => $category->langs->name,
												],
												'input' => [
													'name' => 'categories[]',
													'value' => $category->id,
												],
											])
										</div>
									@endforeach
								</div>
								<div class="uk-margin-bottom">
									<label class="uk-form-label">Visible from @include('fields.required')</label>
									<div class="uk-form-controls">
										@include('fields.card-input', [
											'input' => [
												'name' => 'visible_at',
												'type' => 'date',
												'value' => date('Y-m-d'),
											],
										])
									</div>
								</div>
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>

		@include('admin.pages.blogs.sticky-bar', ['edit' => false, 'checked' => false])
	</form>
</div>
@endsection
