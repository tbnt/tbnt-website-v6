@extends('admin.app', [
'js' => 'categories',
'social' => [
	'title' => 'Categories',
],
])

@section('content')
<div id="page-categories">
	<div class="uk-padding-small-top uk-background-default">
		<div class="uk-container">
			<div class="uk-margin-medium-bottom uk-grid-small" uk-grid>
				<div class="uk-width-1-1 uk-width-expand@s">
					<h1 class="uk-margin-remove">Categories</h1>
				</div>
				<div class="uk-width-1-1 uk-width-auto@s">
					<a pjax href="{{ lang_admin_url('category_new') }}" class="uk-button uk-button-primary">New</a>
				</div>
			</div>
		</div>
	</div>
	<div class="uk-container">
		<div id="page-categories-list" class="uk-padding-vertical">
			<div class="uk-form-stacked">
				<div data-categories>
					@forelse ($categories as $category)
						@include('fields.card-input', [
							'card' => [
								'clickable' => false,
							],
							'inputs' => [[
								'text' => $category->langs->name,
							]],
							'icons' => [[
								'icon' => 'ion-edit',
								'href' => lang_admin_url('category_edit', ['id' => $category->id]),
							], [
								'icon' => 'ion-trash-b',
								'data' => 'data-category-delete='.$category->id,
								'class' => 'uk-text-danger',
							]],
						])
					@empty
						@include('fields.card-input', [
							'input' => [
								'text' => 'There is no category.',
							],
						])
					@endforelse
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
