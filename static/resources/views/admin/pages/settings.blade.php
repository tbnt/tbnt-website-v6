@extends('admin.app', [
'js' => 'settings',
'social' => [
	'title' => 'Edit settings',
],
])

@section('content')
<div id="page-settings-edit">
	<div class="uk-padding-small-top uk-background-default">
		<div class="uk-container">
			<div class="uk-margin-medium-bottom uk-grid-small" uk-grid>
				<div class="uk-width-1-1 uk-width-expand@s">
					<h1 class="uk-margin-remove">Edit settings</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="uk-container uk-padding-vertical">
		<div class="uk-form-stacked">
			<form method="post" enctype="multipart/form-data" data-form>
				<div class="uk-margin-medium">
					<div class="uk-width-1-1 uk-width-1-2@m">
						<div class="uk-margin-bottom">
							<label class="uk-form-label">New password</label>
							<div class="uk-form-controls">
								@include('fields.card-input', [
									'input' => [
										'name' => 'password',
										'type' => 'password',
									],
								])
							</div>
						</div>
						<div class="uk-margin-bottom">
							<label class="uk-form-label">Repeat new password</label>
							<div class="uk-form-controls">
								@include('fields.card-input', [
									'input' => [
										'name' => 'password_confirmation',
										'type' => 'password',
									],
								])
							</div>
						</div>
					</div>
				</div>
			</form>
			<div class="uk-text-right">
				<a pjax href="{{ lang_admin_url('settings') }}" type="button" class="uk-button uk-button-default">Cancel</a>
				<button type="submit" class="uk-button uk-button-primary" data-submit>Update</button>
			</div>
		</div>
	</div>
</div>
@endsection
