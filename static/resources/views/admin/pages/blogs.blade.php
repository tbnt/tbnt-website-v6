@extends('admin.app', [
'js' => 'blogs',
'social' => [
	'title' => 'Blog',
],
])

@section('content')
@define(
	$app_langs_by_id = array_index($app_langs, 'id');
)

<div id="page-blogs">
	<div class="uk-padding-small-top uk-background-default">
		<div class="uk-container">
			<div class="uk-margin-medium-bottom uk-grid-small" uk-grid>
				<div class="uk-width-1-1 uk-width-expand@s">
					<h1 class="uk-margin-remove">Blog</h1>
				</div>
				<div class="uk-width-1-1 uk-width-auto@s">
					<a pjax href="{{ lang_admin_url('blog_new') }}" class="uk-button uk-button-primary">New</a>
				</div>
			</div>
		</div>
	</div>
	<div class="uk-container">
		<div id="page-blogs-list" class="uk-padding-vertical">
			<div class="uk-form-stacked">
				<div class="uk-width-1-1 uk-width-auto@l uk-margin-bottom">
					<div class="uk-flex uk-flex-right uk-child-width-1-1 uk-child-width-auto@s uk-grid-tiny" uk-grid>
						<div class="uk-margin-small-top">
							<button type="button" class="uk-button uk-width-1-1 uk-button-default">Language</button>
							@include('fields.dropdown', [
								'dropdown' => [
									'data' => 'data-filters data-filters-languages data-filter-group=languages',
								],
								'items' => array_merge([[
									'label' => [
										'text' => trans('modules/buttons.reset'),
										'data' => 'type=reset',
										'class' => 'uk-link',
									],
								]], array_map(function($lang) { return [
									'label' => [
										'text' => $lang->name,
									],
									'input' => [
										'value' => '.filter-lang-'.$lang->id,
									],
								]; }, $app_langs)
							)])
						</div>
						<div class="uk-margin-small-top">
							<button type="button" class="uk-button uk-width-1-1 uk-button-default">Category</button>
							@include('fields.dropdown', [
								'dropdown' => [
									'data' => 'data-filters data-filters-categories data-filter-group=categories',
								],
								'items' => array_merge([[
									'label' => [
										'text' => trans('modules/buttons.reset'),
										'data' => 'type=reset',
										'class' => 'uk-link',
									],
								]], array_map(function($category) { return [
									'label' => [
										'text' => $category->langs->name,
									],
									'input' => [
										'value' => '.filter-category-'.$category->id,
									],
								]; }, $categories->toArray())
							)])
						</div>
						<div class="uk-margin-small-top">
							<button type="button" class="uk-button uk-width-1-1 uk-button-default">Status</button>
							@include('fields.dropdown', [
								'dropdown' => [
									'data' => 'data-filters data-filters-status data-filter-group=status',
								],
								'items' => [[
									'label' => [
										'text' => trans('modules/buttons.reset'),
										'data' => 'type=reset',
										'class' => 'uk-link',
									],
								], [
									'label' => [
										'text' => 'Draft',
									],
									'input' => [
										'value' => '.filter-status-0',
									],
								], [
									'label' => [
										'text' => 'Published',
									],
									'input' => [
										'value' => '.filter-status-1',
									],
								]]
							])
						</div>
					</div>
				</div>
				<div data-blogs>
					@forelse ($blogs as $blog)
						@include('fields.card-input', [
							'card' => [
								'clickable' => false,
								'data' => 'data-blog-id='.$blog->id,
								'class' => 'tbnt-pointer mix filter-post filter-lang-'.$blog->lang_id.' filter-status-'.$blog->is_active.' '.implode(' ', array_map(function($category) { return 'filter-category-'.$category->id; }, $blog->categories)),
							],
							'inputs' => [[
								'text' => $blog->langs->title,
								'width' => 'medium',
							], [
								'text' => "{$app_langs_by_id[$blog->lang_id]->emoji} ".strtoupper($app_langs_by_id[$blog->lang_id]->code),
								'width' => 'small',
							], [
								'text' => $blog->is_active ? '<span class="ion-record uk-text-success"></span> Published' : '<span class="ion-record uk-text-warning"></span> Draft',
								'width' => 'small',
							], [
								'text' => implode(', ', array_pluck($blog->categories, 'langs.name')),
							]],
							'icons' => [[
								'icon' => 'ion-edit',
								'data' => 'data-blog-id='.$blog->id,
								'tooltip' => 'Edit',
							], [
								'icon' => 'ion-ios-copy',
								'data' => 'data-blog-duplicate='.$blog->id,
								'tooltip' => 'Duplicate',
							], [
								'icon' => 'ion-trash-b',
								'data' => 'data-blog-delete='.$blog->id,
								'class' => 'uk-text-danger',
								'tooltip' => 'Delete',
							]],
						])
					@empty
						@include('fields.card-input', [
							'input' => [
								'text' => 'There is no post',
							],
						])
					@endforelse

				</div>

				@include('fields.card-input', [
					'card' => [
						'data' => 'data-blogs-none hidden',
					],
					'input' => [
						'text' => 'There is no post with the selected filters',
					],
				])
			</div>
		</div>
	</div>
</div>
@endsection
