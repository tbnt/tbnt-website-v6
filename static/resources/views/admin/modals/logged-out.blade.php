<div uk-modal="esc-close:false; bg-close:false; center:true; container:body" data-modal="logged-out">
	<div class="uk-form-stacked uk-modal-dialog uk-background-light">
		<div class="uk-modal-body" uk-overflow-auto>
			{{ trans('errors.logged_out') }}
		</div>
		<div class="uk-modal-footer uk-text-right">
			<a href="{{ trans('urls_admin.login') }}" class="uk-button uk-button-primary">Sign In</a>
		</div>
	</div>
</div>
