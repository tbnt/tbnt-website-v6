<div uk-modal="esc-close:false; bg-close:false; center:true; container:body" data-modal="new-version">
	<div class="uk-form-stacked uk-modal-dialog uk-background-light">
		<div class="uk-modal-body" uk-overflow-auto>
			{{ trans('errors.new_version') }}
		</div>
		<div class="uk-modal-footer uk-text-right">
			<a href="javascript:window.location.reload()" class="uk-button uk-button-primary">Refresh</a>
		</div>
	</div>
</div>
