@extends('public.app', [
	'js' => '404',
	'page_langs' => [
	'url' => 'urls.home'
	],
	'social' => [
	'title' => trans('pages/404.meta_title'),
	'description' => trans('pages/404.meta_desc'),
	],
	])

	@section('content')
	<section class="page404">
		<div class="composition">
			<div class="container">
				<div class="composition-404">
					<div class="title-element" data-element-1>
						{{ trans('pages/404.title_element_1')}}
					</div>
					<div class="title-element is-purple" data-element-2>
						{{ trans('pages/404.title_element_2')}}
					</div>
				</div>
			</div>
		</div>
		<div class="container text-container">
			<p>	<strong>{{ trans('pages/404.paragraph_information_1')}}</strong></p>
			<p>	{{ trans('pages/404.paragraph_information_2')}}</p>
			<br>
			<a href="" target="_blank" class="primary-link active">
				{{ trans('pages/404.back_to_home')}}
			</a>
		</div>
	</section>

	@endsection