<div class="navbar" data-intro-hidden>
	<div class="navbar-inner container">
		<div class="brand">
			<img src="public/images/brand/tbnt-purple.svg" alt="">
		</div>
		<nav>
			{{--<div class="nav-item">
				<a href="#" class="primary-link taged">{{ trans('pages/home.menu_item_1')}}</a>
				<div class="tag">{{ trans('pages/home.menu_item_1_tag')}}</div>
			</div>--}}
			<div class="nav-item advent-link">
				<a href="http://tbnt.digital/advent/" class="primary-link" target="_blank">{{ trans('pages/home.menu_item_2')}}</a>
				<svg width="16px" height="18px" viewBox="0 0 16 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				    <!-- Generator: Sketch 47.1 (45422) - http://www.bohemiancoding.com/sketch -->
				    <title>christmas tree</title>
				    <defs>christmas tree</defs>
				    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round">
				        <polygon id="avent" stroke="#2D2D2A" points="8.00406205 0 3 7 5 7 2 11 4 11 1 15 6 15 6 17 10 17 10 15 15 15 12 11 14 11 11 7 13.0039801 7"></polygon>
				    </g>
				</svg>
			</div>
		</nav>
	</div>
</div>