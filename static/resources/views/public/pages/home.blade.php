@extends('public.app', [
	'js' => 'home',
	'menu' => 'home',
	'page_langs' => [
	'url' => 'urls.home'
	],
	'social' => [
	'title' => trans('pages/home.meta_title'),
	'description' => trans('pages/home.meta_desc'),
	],
	])

	@section('content')
	<header class="">
		<div class="composition">
			<div class="container intro-container">
				<div class="intro-composition" data-intro>
					<div class="title-element" data-element-intro data-element-1>{{ trans('pages/home.title_element_1') }}</div>
					<div class="title-element is-purple" data-element-intro data-element-2>{{ trans('pages/home.title_element_2') }}</div>
					<div class="title-element is-grey" data-element-intro data-element-3>{{ trans('pages/home.title_element_3') }}</div>
					<div class="title-element" data-element-intro data-element-4>{{ trans('pages/home.title_element_4') }}</div>
					<div class="point-element point-medium" data-point data-point-1></div>
					<div class="point-element point-big" data-point data-point-2></div>
					<div class="point-element point-small" data-point data-point-3></div>
					<div class="point-element point-big" data-point data-point-4></div>
					<div class="point-element point-small" data-point data-point-5></div>
				</div>
				<div class="intro-subtitle" data-intro-hidden>
					<div class="shining-effect" data-shining-effect></div>
					<img src="{{ trans('pages/home.intro_img_subtitle') }}">
				</div>
				<div class="intro-content container grid grid-of-2" data-intro-hidden>
					<div class="grid-element intro-content-item">{{ trans('pages/home.intro_presentation_tbnt_1') }}</div>
					<div class="grid-element intro-content-item">{{ trans('pages/home.intro_presentation_tbnt_2') }}</div>
					<div class="point-element point-small" data-point data-point-6></div>
					<div class="point-element point-medium" data-point data-point-7></div>
					<div class="point-element point-large" data-point data-point-8></div>
					<div class="point-element point-large" data-point data-point-9></div>
				</div>
			</div>
		</div>
	</header>
	<section>
		<div class="reel">
			<div class="container">
				<div class="composition ">
					<div class="container">
						<div class=" reel-composition scroll-anim fadeInUp">
							<div class="title-element is-grey" data-element-1>{{ trans('pages/home.reel_title_element_1') }}</div>
							<div class="video-container">
								<video class="reel-video" >
									<source src="public/video/reel-720-opti.mp4" type="video/mp4">
									Your browser does not support the video tag.
								</video>
								<div class="video-controls">
									<button class="video-play-button">
										<svg viewBox="0 0 123 150" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
										    <title>Play icon</title>
										    <desc>Play icon</desc>
										    <defs></defs>
										    <g id="video-section" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" transform="translate(-656.000000, -438.000000)">
										        <path d="M664.129282,508.882127 L774.375125,438.854608 C777.172254,437.077888 780.880091,437.905093 782.656811,440.702222 C783.267729,441.664002 783.592161,442.779853 783.592161,443.919258 L783.592161,581.303003 C783.592161,584.616712 780.90587,587.303003 777.592161,587.303003 C776.488351,587.303003 775.405933,586.99851 774.46401,586.423029 L664.218167,519.066803 C661.390455,517.339173 660.498662,513.646338 662.226292,510.818625 C662.704325,510.036201 663.35532,509.373743 664.129282,508.882127 Z" id="Rectangle-6" fill="#FFFFFF" transform="translate(719.796081, 512.500000) scale(-1, 1) translate(-719.796081, -512.500000) "></path>
										    </g>
										</svg>
									</button>
								</div>
							</div>
							{{--<iframe src="https://player.vimeo.com/video/218588746?color=ffffff&title=0&byline=0&portrait=0" width="1025" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>--}}
							<div class="title-element is-purple" data-element-2>{{ trans('pages/home.reel_title_element_2') }}</div>
							<span class="music-copyright">Music by <a href="http://www.ceeroo.ch/" target="_blank">Cee-Roo</a></span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="clients">
			<div class="container">
				<div class="composition ">
					<div class="container">
						<div class=" client-composition scroll-anim fadeInUp">
							<div class="title-element is-grey" data-element-1>{{ trans('pages/home.client_title_element_1') }}</div>
							<div class="title-element" data-element-2>{{ trans('pages/home.client_title_element_2') }}</div>
						</div>
					</div>
				</div>
				<div class="grid grid-of-2">
					<div class="grid-element">
						<a href="https://montreuxjazzinsider.com" target="_blank" class="client-element left scroll-anim fadeInUp">
							<div class="card" data-client-mjf>
								<div class="content">
									<div class="client-name"><img src="public/images/brand/mjf.svg" alt=""></div>
								</div>
							</div>
							<div class="client-description">
								<div class="legend" data-legend>
									{{ trans('pages/home.client_desc_7') }}
								</div>
								<div class="tags" data-tags>
									{{ trans('pages/home.client_tags_7') }}
								</div>
							</div>
						</a>
						<a href="https://itunes.apple.com/fr/app/ikentoo-live/id600797418?mt=8" target="_blank" class="client-element left scroll-anim fadeInUp">
							<div class="card" data-client-ikentoo>
								<div class="content">
									<div class="client-name"><img src="public/images/brand/ikentoo.svg" alt=""></div>
								</div>
							</div>
							<div class="client-description">
								<div class="legend" data-legend>
									{{ trans('pages/home.client_desc_2') }}
								</div>
								<div class="tags" data-tags>
									{{ trans('pages/home.client_tags_2') }}
								</div>
							</div>
						</a>
						<a href="http://timea-b.com/fr/" target="_blank" class="client-element left scroll-anim fadeInUp">
							<div class="card" data-client-timea>
								<div class="content">
									<div class="client-name"><img src="public/images/brand/timea.svg" alt=""></div>
								</div>
							</div>
							<div class="client-description">
								<div class="legend" data-legend>
									{{ trans('pages/home.client_desc_5') }}
								</div>
								<div class="tags" data-tags>
									{{ trans('pages/home.client_tags_5') }}
								</div>
							</div>
						</a>
					</div>
					<div class="grid-element">
						<a href="https://mease.media" target="_blank" class="client-element right offset-y scroll-anim fadeInUp">
							<div class="card" data-client-paleo>
								<div class="content">
									<div class="client-name"><img src="public/images/brand/paleo.svg" alt=""></div>
								</div>
							</div>
							<div class="client-description">
								<div class="legend" data-legend>
									{{ trans('pages/home.client_desc_1') }}
								</div>
								<div class="tags" data-tags>
									{{ trans('pages/home.client_tags_1') }}
								</div>
							</div>
						</a>
						<a href="http://www.gottexbrokers.com/" target="_blank" class="client-element right offset-y scroll-anim fadeInUp">
							<div class="card" data-client-gottex>
								<div class="content">
									<div class="client-name"><img src="public/images/brand/gottex.svg" alt="" width="240px"></div>
								</div>
							</div>
							<div class="client-description">
								<div class="legend" data-legend>
									{{ trans('pages/home.client_desc_4') }}
								</div>
								<div class="tags" data-tags>
									{{ trans('pages/home.client_tags_4') }}
								</div>
							</div>
						</a>
						<a href="https://30ans.redcrossmuseum.ch" target="_blank" class="client-element right offset-y scroll-anim fadeInUp">
							<div class="card" data-client-micr >
								<div class="content">
									<div class="client-name"><img src="public/images/brand/micr.png" alt=""></div>
								</div>
							</div>
							<div class="client-description">
								<div class="legend" data-legend>
									{{ trans('pages/home.client_desc_6') }}
								</div>
								<div class="tags" data-tags>
									{{ trans('pages/home.client_tags_6') }}
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
		{{-- <div class="composition ">
			<div class="container">
				<div class=" client-composition">
					<div class="title-element is-grey scroll-anim fadeInUp" data-element-1>{{ trans('pages/home.client_title_element_1') }}</div>
					<div class="title-element scroll-anim fadeInUp" data-element-2>{{ trans('pages/home.client_title_element_2') }}</div>
				</div>
			</div>
		</div> --}}
		<div class="clients">
			<div class="container">
				<div class="grid grid-of-2">
					<div class="grid-element">

					</div>
					<div class="grid-element">

					</div>
				</div>
			</div>
			<div class="container">
				<div class="grid grid-of-2">
					<div class="grid-element">
						<div class="clients-list left offset-y scroll-anim fadeInUp">
							<h3 class="">{{ trans('pages/home.client_working_for') }}</h3>
							{{-- <a target="_blank" href="javascript:void(0)" class="client-link">Croix Rouge Suisse</a> --}}
							<a target="_blank" href="https://www.montreuxjazzfestival.com/" class="client-link">Montreux Jazz</a>
							<a target="_blank" href="https://www.tamoil.ch/" class="client-link">Tamoil</a>
							<a target="_blank" href="https://equestrio.com/" class="client-link">Equestrio</a>
							<a target="_blank" href="http://tatadam.com" class="client-link">Tatadam</a>
							<a target="_blank" href="javascript:void(0)" class="client-link">Pro Confort</a>
							<a target="_blank" href="javascript:void(0)" class="client-link">Kryb</a>
							<a target="_blank" href="javascript:void(0)" class="client-link">Adrenaline Hunter</a>
							<div class="are-u-next">
								<a href="mailto:hello@tbnt.digital" class="primary-link">
									{{ trans('pages/home.client_cta') }}
								</a>
							</div>
						</div>
					</div>
					<div class="grid-element">
						<div class="clients-list right offset-y scroll-anim fadeInUp">
							<h3 class="">{{ trans('pages/home.client_many_more') }}</h3>
							<a target="_blank" href="" class="client-link">Swisscom</a>
							<a target="_blank" href="javascript:void(0)" class="client-link">Dada Sport</a>
							<a target="_blank" href="http://totup.ch/en" class="client-link">TotUP</a>
							<a target="_blank" href="http://brands-up.ch/" class="client-link">Brands Up</a>
							<a target="_blank" href="https://many-ways.ch/" class="client-link">Many Ways</a>
							<a target="_blank" href="https://probst-maveg.ch/fr" class="client-link">Probst Maveg</a>
							<a target="_blank" href="http://www.wagner-betontechnik.ch/fr" class="client-link">Wagner Betontechnik</a>
							<a target="_blank" href="http://www.peregocars.com/en" class="client-link">Perego Cars</a>
							<a target="_blank" href="http://www.massy-vins.ch/" class="client-link">Clos du Boux, Luc massy</a>
							<a target="_blank" href="http://elemelons.digital/" class="client-link">Elemelons Game</a>
							<a target="_blank" href="http://www.cyclesricca.ch/" class="client-link">Cycles Ricca</a>
							<a target="_blank" href="https://askwhich1.com/" class="client-link">Askwhich1</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="composition ">
			<div class="container">
				<div class="service-composition scroll-anim fadeInUp">
					<div class="title-element is-grey" data-element-1>{{ trans('pages/home.service_title_element_1') }}</div>
					<div class="title-element is-purple" data-element-2>{{ trans('pages/home.service_title_element_2') }}</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="services-list grid grid-of-4">
				<div class="service-element grid-element scroll-anim fadeInUp">
					<h3>{{ trans('pages/home.service_col_1_title') }}</h3>
					<ul>
						<li>
							<div class="label">{{ trans('pages/home.service_col_1_label_1') }}</div>
							<small>{{ trans('pages/home.service_col_1_desc_1') }}</small></li>
							<li>

								<div class="label"> {{ trans('pages/home.service_col_1_label_2') }}</div>
								<small>{{ trans('pages/home.service_col_1_desc_2') }}</small>
							</li>
							<li>
								<div class="label">{{ trans('pages/home.service_col_1_label_3') }}</div>
								<small>{{ trans('pages/home.service_col_1_desc_3') }}</small>
							</li>
						</ul>
					</div>
					<div class="service-element grid-element scroll-anim fadeInUp">
						<h3>{{ trans('pages/home.service_col_2_title') }}</h3>
						<ul>
							<li>

								<div class="label">{{ trans('pages/home.service_col_2_label_1') }}  </div>
								<small>{{ trans('pages/home.service_col_2_desc_1') }}</small>
							</li>
							<li>

								<div class="label">{{ trans('pages/home.service_col_2_label_2') }} </div>
								<small>{{ trans('pages/home.service_col_2_desc_2') }}</small>
							</li>
							<li>

								<div class="label">{{ trans('pages/home.service_col_2_label_3') }} </div>
								<small>{{ trans('pages/home.service_col_2_desc_3') }}</small>
							</li>
						</ul>
					</div>
					<div class="service-element grid-element scroll-anim fadeInUp">
						<h3>{{ trans('pages/home.service_col_3_title') }}</h3>
						<ul>
							<li>
								<div class="label">{{ trans('pages/home.service_col_3_label_1') }}  </div>
								<small>{{ trans('pages/home.service_col_3_desc_1') }}  </small>
							</li>
							<li>
								<div class="label">{{ trans('pages/home.service_col_3_label_2') }}  </div>
								<small>{{ trans('pages/home.service_col_3_desc_2') }}  </small>
							</li>
							<li>
								<div class="label">{{ trans('pages/home.service_col_3_label_3') }}  </div>
								<small>{{ trans('pages/home.service_col_3_desc_3') }}  </small>
							</li>
						</ul>
					</div>
					<div class="service-element grid-element scroll-anim fadeInUp">
						<h3>{{ trans('pages/home.service_col_4_title') }}</h3>
						<ul>
							<li>
								<div class="label">{{ trans('pages/home.service_col_4_label_1') }}</div>
								<small>{{ trans('pages/home.service_col_4_desc_1') }}</small>
							</li>
							<li>
								<div class="label">{{ trans('pages/home.service_col_4_label_2') }}</div>
								<small>{{ trans('pages/home.service_col_4_desc_2') }}</small>
							</li>
							<li>
								<div class="label">{{ trans('pages/home.service_col_4_label_3') }}</div>
								<small>{{ trans('pages/home.service_col_4_desc_3') }}</small>
							</li>
							<span class="brands-up-badge">{{ trans('pages/home.service_col_4_brandsup') }}<a href="http://brands-up.ch/" target="_blank"><img src="public/images/services/brandsup-logo.png"></h3></a></span>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="team-section">
			<div class="teams">
				<div class="container">
					<div class="grid grid-of-2">
						<div class="grid-element">
							<div class="team-element left tilt scroll-anim fadeInUp">
								<div class="card">
									<img src="public/images/team/anthony.png" alt="" class="tilt-child-1">
									<div class="team-name">
										<div class="name tilt-child-2">
											Anthony
										</div>
									</div>
								</div>
								<div class="content">
									<p>{{ trans('pages/home.team_1_work')}}👟</p>
									<a href="mailto:anthony@tbnt.digital" class="primary-link">anthony@tbnt.digital</a>
								</div>
							</div>
						</div>
						<div class="grid-element">
							<div class="team-element right offset-y tilt scroll-anim fadeInUp">
								<div class="card">
									<img src="public/images/team/mathias.png" alt="" class="tilt-child-1">
									<div class="team-name">
										<div class="name tilt-child-2">
											Mathias
										</div>
									</div>
								</div>
								<div class="content">
									<p>{{ trans('pages/home.team_2_work')}}🤘</p>
									<a href="mailto:mathias@tbnt.digital" class="primary-link">mathias@tbnt.digital</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="composition ">
				<div class="container">
					<div class="team-composition">
						<div class="title-element" data-element-1>TEAM</div>
						<div class="title-element is-grey" data-element-2>PLAYERS</div>
					</div>
				</div>
			</div>
			<div class="teams">
				<div class="container">
					<div class="grid grid-of-2">
						<div class="grid-element">
							<div class="team-element left tilt scroll-anim fadeInUp">
								<div class="card">
									<img src="public/images/team/ali.png" alt="" class="tilt-child-1" style="width:80%">
									<div class="team-name">
										<div class="name tilt-child-2">
											Ali
										</div>
									</div>
								</div>
								<div class="content">
									<p>{{ trans('pages/home.team_3_work')}} ✏️ </p>
									<a href="mailto:alison@tbnt.digital" class="primary-link">alison@tbnt.digital</a>
								</div>
							</div>
						</div>
						<div class="grid-element">
							<div class="team-element right offset-y tilt scroll-anim fadeInUp">
								<div class="card">
									<img src="public/images/team/alex.png" alt="" class="tilt-child-1">
									<div class="team-name">
										<div class="name tilt-child-2">
											Alex
										</div>
									</div>
								</div>
								<div class="content">
									<p>{{ trans('pages/home.team_4_work')}} 🖥 </p>
									<a href="mailto:alex@tbnt.digital" class="primary-link">alex@tbnt.digital</a>
								</div>
							</div>
						</div>
					</div>
					<div class="team-list scroll-anim fadeInUp">
						<h3>{{ trans('pages/home.team_thanks')}}</h3>
						<ul>
							<li>Stefanie</li>
							<li>Raphael</li>
							<li>Nicolas</li>
							<li>Philippe</li>
							<li>Lorène</li>
							<li>Timon</li>
							<li>Julien</li>
							<li>Blerta</li>
							<li>Jerôme</li>
							<li>Angelo</li>
							<li>Vanessa</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="composition ">
				<div class="container">
					<div class="contact-composition">
						<div class="title-element" data-element-1>
							{{ trans('pages/home.contact_title_element_1')}}
						</div>
						<div class="title-element is-purple" data-element-2>
							{{ trans('pages/home.contact_title_element_2')}}
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="contact-list grid grid-of-4">
					<div class="contact-element grid-element scroll-anim fadeInUp">
						<h3>{{ trans('pages/home.contact_col_1_label')}}</h3>
						<ul>
							<li>
								<a href="https://goo.gl/maps/VoDvy2ovVzA2" target="_blank" class="primary-link">{{ trans('pages/home.contact_col_1_item_1')}}</a>
							</li>
							<li>{{ trans('pages/home.contact_col_1_item_2')}}</li>
							<li>{{ trans('pages/home.contact_col_1_item_3')}}</li>
						</ul>
					</div>
					<div class="contact-element grid-element scroll-anim fadeInUp">
						<h3>{{ trans('pages/home.contact_col_2_label')}}</h3>
						<ul>
							<li><span class="contact-subtitle">{{ trans('pages/home.contact_col_2_item_1')}}</span></li>
							<li><a href="mailto:hello@tbnt.digital" class="primary-link">hello@tbnt.digital</a></li>
							<li><span class="contact-subtitle">{{ trans('pages/home.contact_col_2_item_2')}}</span></li>
							<li>
								<a href="mailto:anthony@tbnt.digital" class="primary-link">anthony@tbnt.digital</a>
							</li>
							<li><a href="phone:+41-78-825-99-52" class="primary-link">+41 78 825 99 52</a></li>
						</ul>
					</div>
					<div class="contact-element grid-element scroll-anim fadeInUp">
						<h3>{{ trans('pages/home.contact_col_3_label')}}</h3>
						<ul>
							<li><a href="https://www.instagram.com/tbnt_/" target="_blank" class="primary-link">Instagram</a></li>
							<li><a href="https://dribbble.com/tbnt" target="_blank" class="primary-link">Dribbble</a></li>
							<li><a href="https://www.linkedin.com/company-beta/5342361/" target="_blank" class="primary-link">Linkedin</a></li>
						</ul>
					</div>
					<div class="contact-element grid-element scroll-anim fadeInUp">
						<h3 class="see-u">{{ trans('pages/home.contact_col_4_label')}}</h3>
						<ul>
							<li class="langs">
								<a href="en" class="lang primary-link @if ($app_lang->code === 'en') active @endif">EN</a>
								<a href="fr" class="lang primary-link @if ($app_lang->code === 'fr') active @endif">FR </a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		@endsection
