<!DOCTYPE html>
<html>
<head>
	<base href="{{ $app_url }}/" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="HandheldFriendly" content="true">
	<meta name=”viewport” content=”width=devicewidth”>
	<meta name="google" content="notranslate">
	<meta http-equiv="Content-Language" content="en">

	{{-- FAVICON --}}
	<link rel="apple-touch-icon" sizes="180x180" href="{{ url('public/images/favicon//apple-touch-icon.png') }}">
	<link rel="icon" type="image/png" href="{{ url('public/images/favicon/favicon-32x32.png') }}" sizes="32x32">
	<link rel="icon" type="image d/png" href="{{ url('public/images/favicon/favicon-16x16.png') }}" sizes="16x16">
	<link rel="manifest" href="{{ url('public/images/favicon/manifest.json') }}">
	<link rel="mask-icon" href="{{ url('public/images/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
	<meta name="theme-color" content="#ffffff">

	{{-- META --}}
	<title>{{ $social['title'] ?? trans('app.meta_title') }}</title>
	<meta name="description" content="{{ $social['description'] ?? trans('app.meta_desc') }}" />

	{{-- CSS --}}
	<link rel="stylesheet" href="public/app/styles/vendor.css?_={{ date('U') }}" />

	{{-- TWITTER --}}
	<meta name="twitter:title" content="{{ $social['title'] ?? trans('app.meta_title') }}" />
	<meta name="twitter:description" content="{{ $social['description'] ?? trans('app.meta_desc') }}" />
	<meta name="twitter:image:src" content="{{ $social['image']['twitter'] ?? $social['image']['app'] ?? url('public/images/share/twitter-share.png') }}" />
	<meta name="twitter:card" content="summary_large_image" />
	{{-- <meta name="twitter:site" content="{{ env('TWITTER_ACCOUNT') }}" /> --}}
	{{-- <meta name="twitter:creator" content="{{ env('TWITTER_ACCOUNT') }}" /> --}}

	{{-- FACEBOOK --}}
	<meta property="og:locale" content="{{ $app_lang->locale }}" />
	<meta property="fb:app_id" content="208640846137885" />
	{{-- <meta property="og:url" content="https://tbnt.digital" /> --}}
	<meta property="og:title" content="{{ $social['title'] ?? trans('app.meta_title') }}" />
	<meta property="og:description" content="{{ $social['description'] ?? trans('app.meta_desc') }}" />
	<meta property="og:image:width" content="880" />
	<meta property="og:image:height" content="280" />
	<meta property="og:image" itemprop="image" content="{{ $social['image']['facebook'] ?? $social['image']['app'] ?? url('public/images/share/facebook-share.png') }}" />
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="{{ env('APP_NAME') }}" />

	{{-- FONTS --}}
	<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,900,900i" rel="stylesheet">

	{{-- SCRIPTS --}}
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-49780635-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-49780635-1');

		@include('public.includes.js')
	</script>
	<noscript>
		<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1773620589626742&ev=PageView&noscript=1"/>
	</noscript>
	{{-- <script src='https://www.google.com/recaptcha/api.js'></script> --}}
</head>
<body>
	<main id="app">
		@include('public.modules.navigation_bar', ['nav_relative' => 0])
		@include('public.modules.sidebar')

		@yield('content')
		<script>
			window.Laravel.js = 'app {{ $js ?? '' }}';
			window.Laravel.menu = '{{ $menu ?? '' }}';
			window.Laravel.version = '{{ env('PROJECT_VERSION', 0) }}';
		</script>
	</main>

	<noscript>
		@include('public.modules.nojs')
	</noscript>
	<script>
		if (Function('/*@cc_on return document.documentMode===10@*/')())
			document.write('{!! get_view('public.modules.browsehappy') !!}');
	</script>

	<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANNMyvQge4htNbjtLNsUyc_GSMGLfCDQo&callback=initMap" type="text/javascript"></script> -->
	<script src="public/app/scripts/vendor.js?_={{ date('U') }}"></script>
</body>
</html>
