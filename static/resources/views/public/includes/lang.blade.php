@foreach ($app_langs as $lang)
	@if (isset($page_langs['url']) === true)
		@define(
			$page_lang = Lang::get($page_langs['url'], [], $lang->code);
		)

		@if (isset($page_langs['urls'][$lang->code]) === true)
			@define(
				$page_lang = $page_lang.'/'.$page_langs['urls'][$lang->code];
			)
		@endif
	@else
		@define(
			$page_lang = $lang->code;
		)
	@endif

	<a href="{{ $page_lang }}" class="lang primary-link">{{ strtoupper($lang->code) }}</a>
@endforeach
