<?php

namespace App\Http\Middleware;

use Closure;

class AuthWebLoggedOutMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($request->auth->web->is_logged_in === true)
			return redirect()->to(lang_url('home'));

		return $next($request);
	}
}
