<?php

namespace App\Http\Middleware;

use Auth;
use Config;
use Closure;

class AuthMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$request->auth = (object) [
			'is_logged_in' => false,
		];

		$guards = [
			'web' => '\App\Project\User',
			'admin' => '\App\Project\UserAdmin',
		];

		foreach ($guards as $guard => $UserClass) {
			$request->auth->{$guard} = (object) [
				'user' => null,
				'is_logged_in' => false,
			];

			if (Auth::guard($guard)->check() === true) {
				$user = Auth::guard($guard)->user();

				if ($user->is_active === 0) {
					$user->logout();
				}
				else {
					$request->auth->{$guard}->user = $UserClass::one($user->id);
					$request->auth->{$guard}->is_logged_in = true;

					$request->auth->is_logged_in = true;
				}
			}
		}

		return $next($request);
	}
}
