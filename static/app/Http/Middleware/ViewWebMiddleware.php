<?php

namespace App\Http\Middleware;

use View;
use Closure;

class ViewWebMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        $is_logged_in = $request->auth->web->is_logged_in;

		View::share('is_logged_in', $is_logged_in);

		return $next($request);
	}
}
