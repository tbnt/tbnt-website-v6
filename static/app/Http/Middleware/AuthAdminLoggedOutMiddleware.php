<?php

namespace App\Http\Middleware;

use Closure;

class AuthAdminLoggedOutMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($request->auth->admin->is_logged_in === true)
			return redirect()->to(lang_admin_url('home'));

		return $next($request);
	}
}
