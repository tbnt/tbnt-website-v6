<?php

namespace App\Http\Middleware;

use View;
use Closure;

class ViewAjaxMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $is_logged_in = $request->auth->is_logged_in;

        View::share('is_logged_in', $is_logged_in);

        return $next($request);
    }
}
