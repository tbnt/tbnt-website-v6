<?php

namespace App\Http\Middleware;

use App\Project\Lang;

use App;
use Closure;

class LangMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Lang::setCurrent('en');

        return $next($request);
    }
}
