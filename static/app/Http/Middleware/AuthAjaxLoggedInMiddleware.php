<?php

namespace App\Http\Middleware;

use Closure;

class AuthAjaxLoggedInMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($request->is_logged_in === false)
			return response()->logged_out();

		return $next($request);
	}
}
