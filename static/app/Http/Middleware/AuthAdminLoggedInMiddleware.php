<?php

namespace App\Http\Middleware;

use Closure;

class AuthAdminLoggedInMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ($request->auth->admin->is_logged_in === false)
			return redirect()->to(lang_admin_url('login'))->with('request_path', $request->path());

		return $next($request);
	}
}
