<?php

namespace App\Http\Middleware;

use App\Project\Lang;

use Closure;
use Session;

class LangWebMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $local = config('langs.default');
        $local_request_empty = config('langs.empty');
        $locals = Lang::allCodes();

        // Init browser language
        $lang_nav = null;

        if ($request->segment(1) === null && Session::get('language') === null && isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $lang_nav = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);

            if (strpos($lang_nav, '_') !== false) {
                $lang_nav = explode('_', $lang_nav, 2);
                $lang_nav = $lang_nav[0];
            }

            if (in_array($lang_nav, $locals) === true)
                $local = $lang_nav;

            Session::put('language', $local);
            Session::save();

            return redirect('/'.($local === $local_request_empty ? '' : $local));
        }

        // Set browser language
        Session::put('language', Lang::getDefaultCode());

        return $next($request);
    }
}
