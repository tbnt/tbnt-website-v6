<?php

namespace App\Http\Middleware;

use Request;
use Closure;

class VersionMiddleware
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$excludes = [
			'ajax/app-config'
		];

		// if (in_array(Request::route()->uri(), $excludes) === false && ($request->all()['version'] ?? 0) !== env('PROJECT_VERSION', 0))
		// 	return response()->need_reload();

		return $next($request);
	}
}
