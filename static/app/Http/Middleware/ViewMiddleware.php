<?php

namespace App\Http\Middleware;

use App\Project\Category;
use App\Project\Lang;

use View;
use Closure;

class ViewMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->is_logged_in = false;
        $request->is_ajax = $request->ajax();
        $request->is_pjax = $request->hasHeader('X-Ich-Bin-Ein-Pjax');
        $request->lang = Lang::getCurrent();

        View::share('is_logged_in', $request->is_logged_in);
        View::share('is_ajax', $request->is_ajax);
        View::share('is_pjax', $request->is_pjax);

        View::share('app_name', app_name());
        View::share('app_url', app_url());
        View::share('app_base', base_url());

        View::share('app_lang', $request->lang);
        View::share('app_langs', Lang::all());

        return $next($request);
    }
}
