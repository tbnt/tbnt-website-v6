<?php

namespace App\Http\Controllers\Admin;

use App\Project\Notification;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Schema\Blueprint;

use DB;
use Schema;

class UpdateController extends Controller
{
	/**
	 * Update
	 *
	 * @param \Illuminate\Http\Request
	 * @return void
	 */
	public function update(Request $request)
	{
		exit('nope');

		// $this->addConcertsRequestsStatusColumn();
		// $this->addNotificationAwaitLangIdColumn();
		// $this->addMailgunColumn();
		// $this->addRequestsIsDeletedColumn();
	}

	/**
	 * Concerts
	 * Add column: requests_interviews_enabled, requests_tv_rights_enabled, requests_photographies_enabled
	 *
	 */
	public function addConcertsRequestsStatusColumn()
	{
		if (Schema::hasColumn('concerts', 'requests_interviews_enabled') === true)
			return;

		Schema::table('concerts', function (Blueprint $table)
		{
			$table->unsignedTinyInteger('requests_interviews_enabled')->after('requests_disabled');
			$table->unsignedTinyInteger('requests_tv_rights_enabled')->after('requests_interviews_enabled');
			$table->unsignedTinyInteger('requests_photographies_enabled')->after('requests_tv_rights_enabled');
		});

		DB::table('concerts')->update([
			'requests_interviews_enabled' => 1,
			'requests_tv_rights_enabled' => 1,
			'requests_photographies_enabled' => 1,
		]);
	}

	/**
	 * Notification await
	 * Add column: lang_id
	 *
	 */
	public function addNotificationAwaitLangIdColumn()
	{
		if (Schema::hasColumn('notifications_await', 'lang_id') === true)
			return;

		Schema::table('notifications_await', function (Blueprint $table)
		{
			$table->unsignedInteger('lang_id')->after('notification_type_id');
		});
	}

	/**
	 * Festival
	 * Add column: mailgun_domain, mailgun_key
	 *
	 */
	public function addMailgunColumn()
	{
		if (Schema::hasColumn('festivals', 'mailgun_domain') === true)
			return;

		Schema::table('festivals', function (Blueprint $table)
		{
			$table->string('mailgun_domain', 255)->after('email');
			$table->string('mailgun_key', 255)->after('mailgun_domain');
		});
	}

	/**
	 * Requests
	 * Add column: is_deleted
	 *
	 */
	public function addRequestsIsDeletedColumn()
	{
		if (Schema::hasColumn('requests', 'is_deleted') === true)
			return;

		Schema::table('requests', function (Blueprint $table)
		{
			$table->unsignedTinyInteger('is_deleted')->after('status');
			$table->index('is_deleted');
		});
	}
}
