<?php

namespace App\Http\Controllers\Admin;

use App\Project\UserAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
	/**
	 * Page edit settings
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getSettings(Request $request)
	{
		return view('admin.pages.settings');
	}

	/**
	 * Ajax edit settings
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postEditSettings(Request $request)
	{
		$args = $request->all();
		$user = $request->auth->admin->user;

		// Validate form
		$validate = UserAdmin::validateUpdatePassword($args);

		if ($validate->any())
			return response()->error($validate->messages());

		$user->updatePassword($args['password']);

		return response()->success();
	}
}
