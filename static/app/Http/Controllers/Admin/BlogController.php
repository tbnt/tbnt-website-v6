<?php

namespace App\Http\Controllers\Admin;

use App\Project\Blog;
use App\Project\Category;
use App\Project\Lang;
use App\Project\Module;
use App\Project\Module\Type as ModuleType;
use App\Project\Utils\Image as UtilsImage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Session;

class BlogController extends Controller
{
	/**
	 * Page blogs
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getBlogs(Request $request)
	{
		$lang = $request->lang;

		$blogs = Blog::all();
		$categories = Category::all($lang->id);

		return view('admin.pages.blogs', ['blogs' => $blogs, 'categories' => $categories]);
	}

	/**
	 * Page new blog
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getNewBlog(Request $request)
	{
		$lang = $request->lang;
		$categories = Category::all($lang->id);

		$image_max_dim = UtilsImage::getMaxWidth();

		return view('admin.pages.blogs.new', ['categories' => $categories, 'image_max_dim' => $image_max_dim]);
	}

	/**
	 * Page edit blog
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getEditBlog(Request $request, $blog_id)
	{
		$lang = $request->lang;
		$categories = Category::all($lang->id);

		$image_max_dim = UtilsImage::getMaxWidth();

		// Get modules types
		$modules_types = ModuleType::all();

		// Get blog
		$blog = Blog::one($blog_id);
		$blog->modules = $blog->getModules();

		return view('admin.pages.blogs.edit', ['blog' => $blog, 'categories' => $categories, 'modules_types' => $modules_types, 'image_max_dim' => $image_max_dim]);
	}

	/**
	 * Page preview blog
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getPreviewBlog(Request $request, $blog_id)
	{
		// Get preview data
		$session = Session::get('blog_preview');

		if (isset($session[$blog_id]) === false)
			return redirect()->route('admin.blog.edit', ['blog_id' => $blog_id]);

		$session = $session[$blog_id];

		// Get blog
		$blog = Blog::one($blog_id);
		$blog->modules = $blog->getModules();

		$blog_next = Blog::next($blog);

		// Format blog
		if (isset($session['data']['lang_id']))
			$blog->lang_id = $session['data']['lang_id'];

		if (isset($session['data']['is_active']))
			$blog->is_active = $session['data']['is_active'];

		if (isset($session['data']['visible_at'])) {
			$blog->visible_at = $session['data']['visible_at'].' 00:00:00';
			$blog->visible_at_utc = to_utc_date($blog->visible_at_utc);
		}

		if (isset($session['data']['langs']))
			$blog->langs = (object) $session['data']['langs'];

		if (isset($session['data']['cover']))
			$blog->cover = (object) ['filepath' => $session['data']['cover']];

		if (isset($session['data']['thumbnail']))
			$blog->thumbnail = (object) ['filepath' => $session['data']['thumbnail']];

		if (isset($session['data']['categories'])) {
			$blog->categories = array_map(function($category_id) use ($blog) {
				return Category::one($category_id, $blog->lang_id);
			}, $session['data']['categories']);
		}

		if (isset($session['data']['modules'])) {
			$blog->modules = array_map(function($module, $index) use ($blog) {
				$blog_module = array_first($blog->modules, function($module) use ($index) { return $module->id == $index; });

				return (object) [
					'text_1' => $module['text_1'] ?? '',
					'text_2' => $module['text_2'] ?? '',
					'caption_1' => $module['caption_1'] ?? '',
					'caption_2' => $module['caption_2'] ?? '',
					'caption_3' => $module['caption_3'] ?? '',
					'image_1' => isset($module['image_1']) ? (object) ['filepath' => $module['image_1']] : $blog_module->image_1 ?? false,
					'image_2' => isset($module['image_2']) ? (object) ['filepath' => $module['image_2']] : $blog_module->image_2 ?? false,
					'image_3' => isset($module['image_3']) ? (object) ['filepath' => $module['image_3']] : $blog_module->image_3 ?? false,
					'module_type' => (object) [
						'type' => $module['type'] ?? '',
					],
				];
			}, $session['data']['modules'], array_keys($session['data']['modules']));
		}

		return view('public.pages.post', ['blog' => $blog, 'blog_next' => $blog_next, 'is_preview' => true]);
	}

	/**
	 * Ajax new blog
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postNewBlog(Request $request)
	{
		$args = $request->all();

		// Validate form
		$validate = Blog::validateInsert($args);

		if ($validate->any())
			return response()->error($validate->messages());

		// Insert blog
		$blog_id = Blog::insert($args);

		// Get blog
		$blog = Blog::one($blog_id);

		return response()->success(['blog' => get_data($blog)]);
	}

	/**
	 * Ajax edit blog
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postEditBlog(Request $request, $blog_id)
	{
		$args = $request->all();

		// Validate form
		$validate = Blog::validateUpdate($blog_id, $args);

		if ($validate->any())
			return response()->error($validate->messages());

		// Get blog
		$blog = Blog::one($blog_id);

		// Update blog
		$blog->update($args);

		return response()->success(['blog' => get_data($blog)]);
	}

	/**
	 * Ajax preview blog
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postPreviewBlog(Request $request, $blog_id)
	{
		$args = $request->all();

		// Validate form
		$validate = Blog::validateUpdate($blog_id, $args);

		if ($validate->any())
			return response()->error($validate->messages());

		// Get blog
		$blog = Blog::one($blog_id);
		$blog_upload_dir = dir_images(Blog::getUploadsDirBaseName($blog).'/preview/');

		recursive_rmdir($blog_upload_dir);
		init_dir($blog_upload_dir);

		// Format preview data
		if (isset($args['cover'])) {
			$filepath = $blog_upload_dir.str_random(10);

			$args['cover']->move($filepath);
			$args['cover'] = $filepath.'/'.$args['cover']->getFileName();
		}

		if (isset($args['thumbnail'])) {
			$filepath = $blog_upload_dir.str_random(10);

			$args['thumbnail']->move($filepath);
			$args['thumbnail'] = $filepath.'/'.$args['thumbnail']->getFileName();
		}

		if (isset($args['modules'])) {
			foreach ($args['modules'] as &$module) {
				if (isset($module['image_1'])) {
					$filepath = $blog_upload_dir.str_random(10);

					$module['image_1']->move($filepath);
					$module['image_1'] = $filepath.'/'.$module['image_1']->getFileName();
				}

				if (isset($module['image_2'])) {
					$filepath = $blog_upload_dir.str_random(10);

					$module['image_2']->move($filepath);
					$module['image_2'] = $filepath.'/'.$module['image_2']->getFileName();
				}

				if (isset($module['image_3'])) {
					$filepath = $blog_upload_dir.str_random(10);

					$module['image_3']->move($filepath);
					$module['image_3'] = $filepath.'/'.$module['image_3']->getFileName();
				}
			}
		}

		// Save preview data
		$session = Session::get('blog_preview', []);
		$session[$blog_id] = ['id' => $blog_id, 'data' => $args];

		Session::put('blog_preview', $session);

		return response()->success(['blog_id' => $blog_id]);
	}

	/**
	 * Ajax duplicate blog
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postDuplicateBlog(Request $request, $blog_id)
	{
		// Validate form
		$validate = Blog::validateExists($blog_id);

		if ($validate->any())
			return response()->error($validate->messages());

		// Get blog
		$blog = Blog::one($blog_id);

		// Delete blog
		$blog->duplicate();

		return response()->success(['blog' => get_data($blog)]);
	}

	/**
	 * Ajax delete blog
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postDeleteBlog(Request $request, $blog_id)
	{
		// Validate form
		$validate = Blog::validateExists($blog_id);

		if ($validate->any())
			return response()->error($validate->messages());

		// Get blog
		$blog = Blog::one($blog_id);

		// Delete blog
		$blog->delete();

		return response()->success(['blog' => get_data($blog)]);
	}
}
