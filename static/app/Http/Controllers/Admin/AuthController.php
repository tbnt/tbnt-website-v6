<?php

namespace App\Http\Controllers\Admin;

use App\Project\UserAdmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
	/**
	 * Page login
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getLogin(Request $request)
	{
		return view('admin.pages.login');
	}

	/**
	 * Page login
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getLogout(Request $request)
	{
		UserAdmin::logout();

		return redirect()->to(lang_admin_url('login'));
	}

	/**
	 * Ajax login
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postLogin(Request $request)
	{
		$args = $request->all();

		// Validate login
		$validate = UserAdmin::validateLogin($args);

		if ($validate->any())
			return redirect()->back()->withErrors($validate);

		// Login user
		if (UserAdmin::login($args) === false)
			return redirect()->back()->withErrors(message_bag('login_failed', 'These credentials do not match our records.'));

		return redirect()->to(lang_admin_url('home'));
	}
}
