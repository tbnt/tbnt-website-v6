<?php

namespace App\Http\Controllers\Admin;

use App\Project\Category;
use App\Project\Lang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
	/**
	 * Page categories
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getCategories(Request $request)
	{
		$lang = Lang::getCurrent();
		$categories = Category::all($lang->id);

		return view('admin.pages.categories', ['categories' => $categories]);
	}

	/**
	 * Page new category
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getNewCategory(Request $request)
	{
		return view('admin.pages.categories.new');
	}

	/**
	 * Page edit category
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getEditCategory(Request $request, $category_id)
	{
		// Get category
		$category = Category::one($category_id);

		return view('admin.pages.categories.edit', ['category' => $category]);
	}

	/**
	 * Ajax new category
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postNewCategory(Request $request)
	{
		$args = $request->all();

		// Validate form
		$validate = Category::validateInsert($args);

		if ($validate->any())
			return response()->error($validate->messages());

		// Insert category
		$category_id = Category::insert($args);

		// Get category
		$category = Category::one($category_id);

		return response()->success(['category' => get_data($category)]);
	}

	/**
	 * Ajax edit category
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postEditCategory(Request $request, $category_id)
	{
		$args = $request->all();

		// Validate form
		$validate = Category::validateUpdate($category_id, $args);

		if ($validate->any())
			return response()->error($validate->messages());

		// Get category
		$category = Category::one($category_id);

		// Update category
		$category->update($args);

		return response()->success(['category' => get_data($category)]);
	}

	/**
	 * Ajax delete category
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postDeleteCategory(Request $request, $category_id)
	{
		// Validate form
		$validate = Category::validateExists($category_id);

		if ($validate->any())
			return response()->error($validate->messages());

		// Get category
		$category = Category::one($category_id);

		// Delete category
		$category->delete();

		return response()->success(['category' => get_data($category)]);
	}
}
