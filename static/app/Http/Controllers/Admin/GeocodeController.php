<?php

namespace App\Http\Controllers\Admin;

use App\Project\Utils\Google;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GeocodeController extends Controller
{
	/**
	 * Geocode address
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function geocode(Request $request)
	{
		$args = $request->all();
		$location = trim($args['location'] ?? '');

		if ($location === '')
			return response()->error(message_bag('geocode_success', trans('validation.required', ['attribute' => 'location'])));

		$geocode = Google::geocode($location);

		if ($geocode === false)
			return response()->error(message_bag('geocode_success', trans('validation.geocode_success')));

		return response()->success(['geocode' => $geocode]);
	}
}
