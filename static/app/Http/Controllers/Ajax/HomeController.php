<?php

namespace App\Http\Controllers\Ajax;

use App\Project\Lang;

use App;
use File;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
	/**
	 * Get app config
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getConfig(Request $request)
	{
		$lang = $request->lang;

		// Get langs
		$langs = Lang::allWithCodes();

		// Get translations
		$resource_path = resource_path().'/lang/'.App::getLocale();
		$trans = [];

		$lang_files = File::files($resource_path);
		$lang_dirs = File::directories($resource_path);

		$include_files = [
			'dates',
			'errors',
			'urls',
		];

		if ($request->auth->admin->is_logged_in === true)
			$include_files[] = 'urls_admin';

		foreach ($lang_files as &$file) {
			$filename = pathinfo($file)['filename'];

			if (in_array($filename, $include_files) === false)
				continue;

			$trans[$filename] = trans($filename);
		}

		foreach ($lang_dirs as &$dir_path) {
			$dirname = basename($dir_path);

			if (in_array($dirname, $include_files) === false)
				continue;

			$trans[$dirname] = [];
			$lang_files = File::files($dir_path);

			foreach ($lang_files as &$file) {
				$filename = pathinfo($file)['filename'];

				if (in_array($filename, $include_files) === false)
					continue;

				$trans[$dirname][$filename] = trans($dirname.'/'.$filename);
			}
		}

		// Get constants
		$constants = config('constants');

		// Format config
		$config = [
			'lang' => $lang,
			'langs' => $langs,
			'trans' => $trans,
			'const' => $constants,
			'csrfToken' => csrf_token(),
			'app' => [
				'base' => base_url(),
				'name' => app_name(),
				'url' => app_url(),
			],
			// 'debug' => [
			// 	'root' => $request->root(),
			// 	'url' => $request->url(),
			// 	'fullUrl' => $request->fullUrl(),
			// 	'path' => $request->path(),
			// 	'segments' => $request->segments(),
			// 	'header' => $request->header(),
			// 	'server' => $request->server(),
			// ],
		];

		return response()->success($config);
	}

	/**
	 * Get csrf token
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getCSRFToken(Request $request)
	{
		return response()->success(['token' => csrf_token()]);
	}
}
