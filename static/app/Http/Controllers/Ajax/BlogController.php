<?php

namespace App\Http\Controllers\Ajax;

use App\Project\Blog;
use App\Project\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
	/**
	 * Get articles
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function postBlogArticles(Request $request)
	{
		$lang = $request->lang;
		$args = $request->all();

		if (Category::exists($args['category'] ?? 0) === false)
			return response()->success(['blogs' => []]);

		$blogs = Blog::all($lang->id, $args);

		foreach ($blogs as $i => &$blog)
			$blog->html = get_view('public.modules.post-preview', ['blog' => $blog, 'latest_post' => intval($args['start']) === 0 && $i === 0]);

		return response()->success(get_data(['blogs' => $blogs]));
	}
}
