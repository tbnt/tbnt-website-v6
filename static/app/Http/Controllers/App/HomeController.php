<?php

namespace App\Http\Controllers\App;

use App\Project\Blog;
use App\Project\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
	/**
	 * Get home page
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getHome(Request $request)
	{
		$lang = $request->lang;


			return view('public.pages.home');

	}

	/**
	 * Get blogs page
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getBlogsArticles(Request $request, $category_page, $category_slug)
	{
		$lang = $request->lang;
		$args = $request->all();
		$now = date('Y-m-d H:i:s');

		$category_id = Category::slugId($category_slug, $lang->id);
		$category = Category::one($category_id);

		if ($category === null)
			return view('errors.404');

		$blogs_config = ['category' => $category_id, 'count' => 10, 'from' => $now, 'lang' => $lang->id];
		$blogs = Blog::all($lang->id, $blogs_config);

		return view('public.pages.blogs', ['category' => $category, 'blogs' => $blogs, 'blogs_config' => $blogs_config]);
	}

	/**
	 * Get blog details page
	 *
	 * @param \Illuminate\Http\Request
	 * @return \Illuminate\Http\Response
	 */
	public function getBlogArticle(Request $request, $blog_page, $blog_slug)
	{
		$lang = $request->lang;

		$blog_id = Blog::slugId($blog_slug, $lang->id);

		if (Blog::exists($blog_id) === false)
			return view('errors.404');

		$blog = Blog::one($blog_id, $lang->id);
		$blog->modules = $blog->getModules();

		$blog_next = Blog::next($blog);

		return view('public.pages.post', ['blog' => $blog, 'blog_next' => $blog_next]);
	}
}
