<?php

use App\Project\Lang;
use App\Project\Utils\DateFormat;

use ForceUTF8\Encoding;
use Illuminate\Support\Debug\Dumper;
use Illuminate\Support\MessageBag;

/*
|--------------------------------------------------------------------------
| String
|--------------------------------------------------------------------------
*/

function generate_hash()
{
	return sha1(str_random(7).microtime().str_random(22));
}

function str_bool($str)
{
	return $str === true || $str === 'true' || $str === 1 || $str === '1' ? true : false;
}

function trim_end($str, $trim_char)
{
	return rtrim($str, $trim_char).$trim_char;
}

function trim_before($str, $trim_char)
{
	return $trim_char.ltrim($str, $trim_char);
}

function trim_all($str, $trim_char)
{
	return $trim_char.trim($str, $trim_char).$trim_char;
}

function slash_end($str)
{
	return trim_end($str, '/');
}

function slash_before($str)
{
	return trim_before($str, '/');
}

function slash_all($str)
{
	return trim_all($str, '/');
}

function str_url($str)
{
	$str = trim($str);

	if (strpos($str, 'https') === 0 || strpos($str, 'http') === 0 || $str === '')
		return $str;

	return 'http://'.ltrim($str, '://');
}

function replace_multiple($search, $str)
{
	return preg_replace('/'.$search.'{2,}/', $search, $str);
}

function snake_case_upper($str)
{
	$str_after = replace_multiple('_', preg_replace('/(?<!^|\d)[\d]+/', '_$0', $str));
	$str_after = str_replace(['[_'], ['['], $str_after);

	return $str_after;
}

function remove_accents($str)
{
	if (!preg_match('/[\x80-\xff]/', $str))
		return $str;

	$chars = [
		// Decompositions for Latin-1 Supplement
		chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
		chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
		chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
		chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
		chr(195).chr(137) => 'E', chr(195).chr(138) => 'E',
		chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
		chr(195).chr(141) => 'I', chr(195).chr(142) => 'I',
		chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
		chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
		chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
		chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
		chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
		chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
		chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
		chr(195).chr(161) => 'a', chr(195).chr(162) => 'a',
		chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
		chr(195).chr(165) => 'a', chr(195).chr(167) => 'c',
		chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
		chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
		chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
		chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
		chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
		chr(195).chr(179) => 'o', chr(195).chr(180) => 'o',
		chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
		chr(195).chr(182) => 'o', chr(195).chr(185) => 'u',
		chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
		chr(195).chr(188) => 'u', chr(195).chr(189) => 'y',
		chr(195).chr(191) => 'y',
		// Decompositions for Latin Extended-A
		chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
		chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
		chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
		chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
		chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
		chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
		chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
		chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
		chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
		chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
		chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
		chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
		chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
		chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
		chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
		chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
		chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
		chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
		chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
		chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
		chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
		chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
		chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
		chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
		chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
		chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
		chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
		chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
		chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
		chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
		chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
		chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
		chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
		chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
		chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
		chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
		chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
		chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
		chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
		chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
		chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
		chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
		chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
		chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
		chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
		chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
		chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
		chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
		chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
		chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
		chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
		chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
		chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
		chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
		chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
		chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
		chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
		chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
		chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
		chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
		chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
		chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
		chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
		chr(197).chr(190) => 'z', chr(197).chr(191) => 's',
	];

	$str = strtr($str, $chars);

	return $str;
}


/*
|--------------------------------------------------------------------------
| Lists
|--------------------------------------------------------------------------
*/

function init_list($d, $count, $replacement = null, $reverse = false)
{
	return array_pad($d, $reverse ? 0 - $count : $count, $replacement);
}



/*
|--------------------------------------------------------------------------
| Arrays
|--------------------------------------------------------------------------
*/

function init_array($d)
{
	return is_array($d) === false ? [$d] : $d;
}

function array_to_object($d)
{
	if (is_array($d) === false && is_object($d) === false)
		return $d;

	return json_decode(json_encode($d));
}

function array_prefix($d, $prefix)
{
	return array_combine(array_map(function($k) use ($prefix) { return $prefix.$k; }, array_keys($d)), $d);
}

function array_index($d, $index)
{
	return array_combine(array_pluck($d, $index), $d);
}

function array_reduce_empty($d)
{
	return count(array_filter(array_flatten($d))) === 0;
}

function array_filter_key($d, $k)
{
	return array_intersect_key($d, array_flip($k));
}

function array_delete(&$d, $k)
{
	if (($key = array_search($k, $d)) !== false) unset($d[$key]);
}

function sort_by($d, $k)
{
	return array_values(array_sort($d, function ($v) use ($k) {
		if (is_array($v) && isset($v[$k])) return $v[$k];
		else if (is_object($v) && isset($v->{$k})) return $v->{$k};
	}));
}

function sort_by_nat($d, $k)
{
	usort($d, function($a, $b) use (&$k) {
		if (is_array($a) && isset($a[$k])) return strnatcmp($a[$k], $b[$k]);
		else if (is_object($a) && isset($a->{$k})) return strnatcmp($a->{$k}, $b->{$k});
	});

	return $d;
}



/*
|--------------------------------------------------------------------------
| Objects
|--------------------------------------------------------------------------
*/

function object_to_array($d)
{
	if (is_array($d) === false && is_object($d) === false)
		return $d;

	if (is_object($d) === true)
		$d = get_object_vars($d);

	return is_array($d) === true ? array_map(__FUNCTION__, $d) : $d;
}

function object_merge()
{
	$args = func_get_args();
	$obj = [];

	foreach ($args as &$arg) {
		try {
			$obj = array_replace_recursive($obj, object_to_array($arg));
		}
		catch (ErrorException $e) {
			continue;
		}
	}

	return array_to_object($obj);
}



/*
|--------------------------------------------------------------------------
| Collections
|--------------------------------------------------------------------------
*/

function is_collection($d)
{
	return is_object($d) === true && get_class($d) === 'Illuminate\Support\Collection';
}



/*
|--------------------------------------------------------------------------
| Project
|--------------------------------------------------------------------------
*/

function is_project($d)
{
	return is_object($d) === true && get_parent_class($d) === 'App\Project';
}

function get_data($d)
{
	if (is_project($d) === true || is_collection($d) === true) {
		return array_map(__FUNCTION__, $d->toArray());
	}
	else if (is_array($d) === true) {
		return array_map(__FUNCTION__, $d);
	}
	else if (is_object($d) === true) {
		foreach ($d as $key => $value) $d->{$key} = get_data($value);
		return $d;
	}
	else {
		return $d;
	}
}



/*
|--------------------------------------------------------------------------
| Json
|--------------------------------------------------------------------------
*/

function stringify($d)
{
	$escapers =     ["\\",   "/",   "\"",   "\n",  "\r",  "\t",  "\x08", "\x0c", "\u0022"];
	$replacements = ["\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f",  "\\b",  "\\\\\""];

	return str_replace($escapers, $replacements, json_encode($d, JSON_HEX_QUOT | JSON_HEX_APOS));
}



/*
|--------------------------------------------------------------------------
| Dates
|--------------------------------------------------------------------------
*/

function short_time($time)
{
	return preg_replace("/:[^:]+$/", '', $time);
}

function to_utc_date($date)
{
	return trim_end(str_replace(' ', 'T', $date).'Z', 'Z');
}

function format_date($date, $format = 'm F, H:i')
{
	return with(new \DateTime($date))->format($format);
}

function format_date_strf($date, $format = '%d %m %Y')
{
	setlocale(LC_TIME, Lang::getCurrent()->locale.'.UTF-8');

	return strftime($format, format_date($date, 'U'));
}

function lang_date($date, $format)
{
	return format_date_strf($date, trans($format));
}



/*
|--------------------------------------------------------------------------
| App
|--------------------------------------------------------------------------
*/

function is_cli()
{
	return PHP_SAPI === 'cli';
}

function is_ajax_group()
{
	return Request::instance()->is_ajax === true;
}

function app_name()
{
	return env('APP_NAME', 'APP_NAME');
}

function app_url()
{
	if (is_cli() === true)
		return 'http://localhost';

	return url('');
}

function base_url()
{
	return trim(explode('?', str_replace(Request::path(), '/', Request::server('REQUEST_URI')))[0], '/').'/';
}

function lang_url($url, $data = [])
{
	return \Lang::get('urls.'.$url, $data);
}

function lang_admin_url($url, $data = [])
{
	return 'admin'.slash_before(\Lang::get('urls_admin.'.$url, $data));
}

function dir_files($filepath)
{
	return \App\Project\Utils\File::getDir($filepath);
}

function dir_images($filepath)
{
	return \App\Project\Utils\Image::getDir($filepath);
}

function dir_thumbs($filepath)
{
	return \App\Project\Utils\Image::getThumbsDir($filepath);
}



/*
|--------------------------------------------------------------------------
| Print
|--------------------------------------------------------------------------
*/

function ddd()
{
	if (PHP_SAPI !== 'cli') {
		echo(
			'<style>'.
				'.sf-dump { font-size: 10px !important; }'.
				'.sf-dump .sf-dump-compact { display: inline !important; font-size: inherit!important; font-family: inherit!important; color: inherit!important; white-space: inherit!important; padding: inherit!important; background: inherit!important; }'.
			'</style>'
		);
	}

	array_map(function ($x) { (new Dumper)->dump($x); }, func_get_args());
}



/*
|--------------------------------------------------------------------------
| Views
|--------------------------------------------------------------------------
*/

function get_view($view, $data = [])
{
	$view = (string) view($view, $data);
	$view = str_replace(["\n", "\t"], '', $view);

	return $view;
}



/*
|--------------------------------------------------------------------------
| Sessions
|--------------------------------------------------------------------------
*/

function init_post($d, $session = 'post')
{
	return object_merge($d, session($session) === null ? [] : session($session));
}

function clean_post($d)
{
	if (is_array($d) === true) return array_map(__FUNCTION__, $d);
	else return is_uploaded_file($d) ? null : Encoding::fixUTF8($d);
}



/*
|--------------------------------------------------------------------------
| Message bag
|--------------------------------------------------------------------------
*/

function message_bag($key = '', $message = '')
{
	$errors = new MessageBag;
	return $key === '' ? $errors : $errors->add($key, $message);
}



/*
|--------------------------------------------------------------------------
| Files
|--------------------------------------------------------------------------
*/

function init_dir($dir)
{
	if (is_dir($dir) === false && is_file($dir) === false)
		mkdir($dir, 0755, true);
}

function recursive_rmdir($dir)
{
	if (is_dir($dir)) {
		$objects = scandir($dir);

		foreach ($objects as $object) {
			if ($object !== '.' && $object !== '..') {
				if (is_dir($dir.'/'.$object)) recursive_rmdir($dir.'/'.$object);
				else unlink($dir.'/'.$object);
			}
		}

		rmdir($dir);
	}
}



/*
|--------------------------------------------------------------------------
| PDF
|--------------------------------------------------------------------------
*/

function make_pdf($view, $data)
{
	$mpdf = new \mPDF();

	$mpdf->simpleTables = true;
	$mpdf->packTableData = true;
	$mpdf->CSSselectMedia = 'screen';
	$mpdf->ignore_invalid_utf8 = true;

	$mpdf->setFooter('{PAGENO} / {nb}');
	$mpdf->WriteHTML(get_view($view, $data));

	return $mpdf;
}

function save_pdf($view, $data, $output)
{
	$mpdf = make_pdf($view, $data);
	$mpdf->Output($output, 'F');

	return true;
}

function download_pdf($view, $data, $output)
{
	$mpdf = make_pdf($view, $data);
	$mpdf->Output($output, 'D');

	return true;
}

function print_pdf($view, $data)
{
	$mpdf = make_pdf($view, $data);
	$mpdf->Output();

	return true;
}
