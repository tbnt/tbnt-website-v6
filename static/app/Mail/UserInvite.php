<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Lang;

class UserInvite extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * User
     *
     * @var object
     */
    public $user;

    /**
     * Festival
     *
     * @var object
     */
    public $festival;

    /**
     * Activation link
     *
     * @var string
     */
    public $activation_link;

    /**
     * Activation token duration
     *
     * @var integer
     */
    public $duration;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->user = $data['data']->user ?? null;
        $this->festival = $data['data']->festival ?? null;
        $this->activation_link = str_url($this->festival->domain).'/'.Lang::get('urls.activate_account').'?activation_token='.($data['data']->activation_token ?? '');
        $this->duration = config('project.activation_token_duration');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from($this->festival->email, $this->festival->name)
            ->subject(Lang::get('emails/user-invite.subject', ['name' => $this->festival->name]))
            ->view('emails.user-invite')
            ->text('emails.user-invite-plain');
    }
}
