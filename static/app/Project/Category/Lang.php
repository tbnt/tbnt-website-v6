<?php

namespace App\Project\Category;

use App\Project\Lang as AppLang;

use DB;
use Validator;

class Lang
{
	/**
	 * Get id from slug
	 *
	 * @param string $category_slug Category slug
	 * @param integer $lang_id Lang id
	 * @return integer
	 */
	public static function slugId($category_slug, $lang_id = 0)
	{
		$lang = DB::table('categories_langs')
			->where('lang_id', $lang_id)
			->where('url', $category_slug)
			->first();

		if ($lang === null)
			return 0;

		return $lang->category_id;
	}

	/**
	 * Get langs
	 *
	 * @param integer $category_id Category id
	 * @param integer $lang_id Lang id
	 * @return array|object
	 */
	public static function get($category_id, $lang_id = 0)
	{
		$langs = AppLang::allWithIds();

		// Get langs
		$categories_langs = DB::table('categories_langs')
			->where('category_id', $category_id);

		if ($lang_id !== 0) {
			$categories_langs = $categories_langs
				->where('lang_id', $lang_id);
		}

		$categories_langs = $categories_langs
			->get();

		// Format langs
		$category = [];

		foreach ($categories_langs as &$category_lang)
			$category[$langs[$category_lang->lang_id]->code] = $category_lang;

		if ($lang_id !== 0)
			$category = $category[$langs[$lang_id]->code];

		return $category;
	}

	/**
	 * Update langs
	 *
	 * @param object $category Category
	 * @param array $data Langs
	 * @return void
	 */
	public static function update($category, $data)
	{
		$langs = AppLang::all();

		// Format langs
		$insert_values = [];

		foreach ($langs as &$lang) {
			$insert_values[] = [
				'category_id' => $category->id,
				'lang_id' => $lang->id,
				'name' => $data[$lang->code]['name'] ?? '',
				'url' => str_slug($data[$lang->code]['url'] ?? ''),
			];
		}

		// Delete langs
		self::deleteAll($category);

		// Insert langs
		DB::table('categories_langs')->insert($insert_values);
	}

	/**
	 * Delete all langs
	 *
	 * @param object $category Category
	 * @return void
	 */
	public static function deleteAll($category)
	{
		DB::table('categories_langs')
			->where('category_id', $category->id)
			->delete();
	}



	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate insert
	 *
	 * @param array $data Array to validate
	 * @param integer $category_id Category id
	 * @return MessageBag
	 */
	public static function validateInsert($data, $category_id = 0)
	{
		$errors = Validator::make(['keys' => array_keys($data)], [
			'keys.*' => 'string|lang_code_exists',
		])->errors();

		foreach ($data as $lang_code => &$items) {
			$errors = Validator::make($data, [
				'*.name' => 'required|string',
				'*.url' => 'required|string|category_url_unique:'.$category_id.','.AppLang::getId($lang_code),
			])->errors()->merge($errors);
		}

		return $errors;
	}

	/**
	 * Validate update
	 *
	 * @param integer $category_id Category id
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateUpdate($category_id, $data)
	{
		return self::validateInsert($data, $category_id);
	}
}
