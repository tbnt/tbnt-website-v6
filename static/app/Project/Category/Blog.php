<?php

namespace App\Project\Category;

use DB;

class Blog
{
	/**
	 * Delete all blogs
	 *
	 * @param object $category Category
	 * @return void
	 */
	public static function deleteAll($category)
	{
		DB::table('blogs_categories')
			->where('category_id', $category->id)
			->delete();
	}
}
