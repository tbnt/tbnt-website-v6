<?php

namespace App\Project;

use App\Project;

use Auth;
use DB;
use Hash;
use Validator;

class UserAdmin extends Project
{
	/**
	 * Constructor
	 *
	 * @param array $data Data
	 * @return void
	 */
	public function __construct($data)
	{
		parent::__construct($data);

		$this->format();
	}

	/**
	 * Format user
	 *
	 * @return void
	 */
	public function format()
	{
		$this->created_at_utc = to_utc_date($this->created_at);
		$this->updated_at_utc = to_utc_date($this->updated_at);
	}

	/**
	 * Update user
	 *
	 * @param array $data User data
	 * @return void
	 */
	public function update($data)
	{
		$this->_update($this, $data);
		$this->refreshData();
	}

	/**
	 * Update user password
	 *
	 * @param srting $password Password
	 * @return void
	 */
	public function updatePassword($password)
	{
		$this->_updatePassword($this, $password);
		$this->refreshData();
	}

	/**
	 * Refresh user data
	 *
	 * @return void
	 */
	public function refreshData()
	{
		$this->updateData($this->one($this->id));
	}

	/**
	 * Delete user
	 *
	 * @return void
	 */
	public function delete()
	{
		$this->_delete($this);
	}



	/****************************************************************
	 * Static
	 ***************************************************************/

	/**
	 * Login user
	 *
	 * @param array $data Login data
	 * @return integer
	 */
	public static function login($data)
	{
		return Auth::guard('admin')->attempt([
			'username' => $data['username'] ?? '',
			'password' => $data['password'] ?? '',
			'is_active' => 1
		]);
	}

	/**
	 * Logout user
	 *
	 * @return void
	 */
	public static function logout()
	{
		Auth::guard('admin')->logout();
	}

	/**
	 * Check if user exists
	 *
	 * @param integer $user_id User id
	 * @return boolean
	 */
	public static function exists($user_id)
	{
		return DB::table('users_admin')
			->where('id', $user_id)
			->exists();
	}

	/**
	 * Get user
	 *
	 * @param integer $user_id User id
	 * @return object
	 */
	public static function one($user_id)
	{
		$user = DB::table('users_admin')
			->where('id', $user_id)
			->first();

		return self::initialize(__CLASS__, $user);
	}

	/**
	 * Get all users
	 *
	 * @return array
	 */
	public static function all()
	{
		$users = DB::table('users_admin')
			->get();

		return self::initialize(__CLASS__, $users);
	}

	/**
	 * Insert user
	 *
	 * @param array $data User data
	 * @return integer
	 */
	public static function insert($data)
	{
		$now = date('Y-m-d H:i:s');

		// Format insert
		$insert_values = [
			'username' => $data['username'] ?? '',
			'type' => $data['type'] ?? 0,
			'is_active' => $data['is_active'] ?? 0,
			'created_at' => $now,
		];

		// Insert user
		$user_id = DB::table('users_admin')
			->insertGetId($insert_values);

		return $user_id;
	}

	/**
	 * Update user
	 *
	 * @param object $user User
	 * @param array $data User data
	 * @return void
	 */
	private static function _update($user, $data)
	{
		$now = date('Y-m-d H:i:s');

		// Format update
		$update_values = [
			'username' => $data['username'] ?? '',
			'type' => $data['type'] ?? 0,
			'is_active' => $data['is_active'] ?? 0,
			'updated_at' => $now,
		];

		// Update user
		DB::table('users_admin')
			->where('id', $user->id)
			->update($update_values);
	}

	/**
	 * Update user password
	 *
	 * @param object $user User
	 * @param string $password Password
	 * @return void
	 */
	private static function _updatePassword($user, $password)
	{
		$now = date('Y-m-d H:i:s');

		// Format update
		$update_values = [
			'password' => Hash::make($password),
			'updated_at' => $now,
		];

		// Update user
		DB::table('users_admin')
			->where('id', $user->id)
			->update($update_values);
	}

	/**
	 * Delete user
	 *
	 * @param object $user User
	 * @return void
	 */
	private static function _delete($user)
	{
		DB::table('users_admin')
			->where('id', $user->id)
			->delete();
	}



	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate exists
	 *
	 * @param integer $user_id User id
	 * @return MessageBag
	 */
	public static function validateExists($user_id)
	{
		$errors = Validator::make(['user_id' => $user_id], [
			'user_id' => 'required|numeric|user_admin_exists',
		])->errors();

		return $errors;
	}

	/**
	 * Validate insert
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateInsert($data)
	{
		$errors = Validator::make($data, [
			'username' => 'required|string',
			'type' => 'numeric',
			'is_active' => 'boolean',
		])->errors();

		return $errors;
	}

	/**
	 * Validate update
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateUpdate($data)
	{
		$errors = Validator::make($data, [
			'username' => 'required|string',
			'type' => 'numeric',
			'is_active' => 'boolean',
		])->errors();

		return $errors;
	}

	/**
	 * Validate update password
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateUpdatePassword($data)
	{
		$errors = Validator::make($data, [
			'password' => 'required|string|confirmed|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/',
		])->errors();

		return $errors;
	}

	/**
	 * Validate login
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateLogin($data)
	{
		$errors = Validator::make($data, [
			'username' => 'required|string',
			'password' => 'required|string',
		])->errors();

		return $errors;
	}
}
