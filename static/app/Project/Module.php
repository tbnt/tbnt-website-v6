<?php

namespace App\Project;

use App\Project;
use App\Project\Module\Type as ModuleType;
use App\Project\Image;
use App\Project\Utils\Video as UtilsVideo;

use DB;
use Validator;

class Module extends Project
{
	/**
	 * Constructor
	 *
	 * @param array $data Data
	 * @return void
	 */
	public function __construct($data)
	{
		parent::__construct($data);

		$this->format();
	}

	/**
	 * Format module
	 *
	 * @return void
	 */
	public function format()
	{
		$this->created_at_utc = to_utc_date($this->created_at);
		$this->updated_at_utc = to_utc_date($this->updated_at);

		$this->module_type = ModuleType::one($this->module_type_id);

		if (Image::exists($this->image_id_1)) $this->image_1 = Image::one($this->image_id_1);
		if (Image::exists($this->image_id_2)) $this->image_2 = Image::one($this->image_id_2);
		if (Image::exists($this->image_id_3)) $this->image_3 = Image::one($this->image_id_3);
	}

	/**
	 * Update module
	 *
	 * @param array $data Module data
	 * @return void
	 */
	public function update($data)
	{
		$this->_update($this, $data);
		$this->refreshData();
	}

	/**
	 * Update module
	 *
	 * @return void
	 */
	public function duplicate()
	{
		$module_id = $this->_duplicate($this);
		$this->refreshData();

		return self::one($module_id);
	}

	/**
	 * Refresh module data
	 *
	 * @return void
	 */
	public function refreshData()
	{
		$this->updateData($this->one($this->id));
	}

	/**
	 * Delete module
	 *
	 * @return void
	 */
	public function delete()
	{
		$this->_delete($this);
	}



	/****************************************************************
	 * Static
	 ***************************************************************/

	/**
	 * Get uploads dir base name
	 *
	 * @param object $blog Blog
	 * @return string
	 */
	public static function getUploadsDirBaseName()
	{
		return 'blogs/modules';
	}

	/**
	 * Check if module exists
	 *
	 * @param integer $module_id Module id
	 * @return boolean
	 */
	public static function exists($module_id)
	{
		return DB::table('modules')
			->where('id', $module_id)
			->exists();
	}

	/**
	 * Get module
	 *
	 * @param integer $module_id Module id
	 * @param integer $lang_id Lang id
	 * @return object
	 */
	public static function one($module_id, $lang_id = 0)
	{
		$module = DB::table('modules')
			->where('id', $module_id)
			->first();

		return self::initialize(__CLASS__, $module, $lang_id);
	}

	/**
	 * Get all modules
	 *
	 * @param integer $lang_id Lang id
	 * @return array
	 */
	public static function all($lang_id = 0)
	{
		$modules = DB::table('modules')
			->get();

		return self::initialize(__CLASS__, $modules, $lang_id);
	}

	/**
	 * Insert module
	 *
	 * @param array $data Module data
	 * @return integer
	 */
	public static function insert($data)
	{
		$now = date('Y-m-d H:i:s');

		// Format insert
		$insert_values = self::_formatModuleByType($data);
		$insert_values['created_at'] = $now;

		// Insert module
		$module_id = DB::table('modules')
			->insertGetId($insert_values);

		return $module_id;
	}

	/**
	 * Update module
	 *
	 * @param object $module Module
	 * @param array $data Module data
	 * @return void
	 */
	private static function _update($module, $data)
	{
		$now = date('Y-m-d H:i:s');

		// Format update
		$update_values = self::_formatModuleByType($data, $module);
		$update_values['updated_at'] = $now;

		// Update module
		DB::table('modules')
			->where('id', $module->id)
			->update($update_values);
	}

	/**
	 * Duplicate module
	 *
	 * @param object $module Module
	 * @return void
	 */
	private static function _duplicate($module)
	{
		$now = date('Y-m-d H:i:s');

		// Duplicate blog
		$module_duplicated_id = DB::table('modules')
			->insertGetId([
				'module_type_id' => $module->module_type_id,
				'text_1' => $module->text_1,
				'text_2' => $module->text_2,
				'caption_1' => $module->caption_1,
				'caption_2' => $module->caption_2,
				'caption_3' => $module->caption_3,
				'created_at' => $now,
			]);

		// Get duplicated module
		$module_duplicated = self::one($module_duplicated_id);

		// Duplicate images
		$images_options = ['path' => self::getUploadsDirBaseName()];

		DB::table('modules')
			->where('id', $module_duplicated->id)
			->update([
				'image_id_1' => $module->image_1 ? $module->image_1->duplicate($images_options)->id : 0,
				'image_id_2' => $module->image_2 ? $module->image_2->duplicate($images_options)->id : 0,
				'image_id_3' => $module->image_3 ? $module->image_3->duplicate($images_options)->id : 0,
			]);

		return $module_duplicated_id;
	}

	/**
	 * Delete module
	 *
	 * @param array $data Module data
	 * @param object $module Module
	 * @return void
	 */
	private static function _formatModuleByType($data, $module = null)
	{
		$module_type = $data['type'] ?? '';

		$values = [
			'module_type_id' => ModuleType::id($module_type),
		];

		if (in_array($module_type, ['wysiwyg', 'wysiwyg_2', 'wysiwyg_image', 'image_wysiwyg', 'quote']) === true)
			$values['text_1'] = $data['text_1'] ?? '';

		if (in_array($module_type, ['video']) === true)
			$values['text_1'] = UtilsVideo::convert_embed($data['text_1'] ?? '');

		if (in_array($module_type, ['wysiwyg_2', 'quote']) === true)
			$values['text_2'] = $data['text_2'] ?? '';

		if (in_array($module_type, ['wysiwyg_image', 'image_wysiwyg', 'image', 'image_2', 'image_3']) === true) {
			if (Image::isUploaded($data['image_1'] ?? null) === true) {
				$values['image_id_1'] = Image::upload($module->image_id_1 ?? 0, $data['image_1'], ['path' => self::getUploadsDirBaseName()]);
			}

			$values['caption_1'] = $data['caption_1'] ?? '';
		}

		if (in_array($module_type, ['image_2', 'image_3']) === true) {
			if (Image::isUploaded($data['image_2'] ?? null) === true) {
				$values['image_id_2'] = Image::upload($module->image_id_2 ?? 0, $data['image_2'], ['path' => self::getUploadsDirBaseName()]);
			}

			$values['caption_2'] = $data['caption_2'] ?? '';
		}

		if (in_array($module_type, ['image_3']) === true) {
			if (Image::isUploaded($data['image_3'] ?? null) === true) {
				$values['image_id_3'] = Image::upload($module->image_id_3 ?? 0, $data['image_3'], ['path' => self::getUploadsDirBaseName()]);
			}

			$values['caption_3'] = $data['caption_3'] ?? '';
		}

		return $values;
	}

	/**
	 * Delete module
	 *
	 * @param object $module Module
	 * @return void
	 */
	private static function _delete($module)
	{
		// Delete images
		if ($module->image_1) $module->image_1->delete();
		if ($module->image_2) $module->image_2->delete();
		if ($module->image_3) $module->image_3->delete();

		// Delete module
		DB::table('modules')
			->where('id', $module->id)
			->delete();
	}



	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate exists
	 *
	 * @param integer $module_id Module id
	 * @return MessageBag
	 */
	public static function validateExists($module_id)
	{
		$errors = Validator::make(['module_id' => $module_id], [
			'module_id' => 'required|numeric|module_exists',
		])->errors();

		return $errors;
	}

	/**
	 * Validate insert
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateInsert($data)
	{
		$errors = Validator::make($data, [
			'type' => 'required|string|module_type_exists',
			'text_1' => 'string',
			'text_2' => 'string',
			'caption_1' => 'string',
			'caption_2' => 'string',
			'caption_3' => 'string',
		])->errors();

		$errors = Image::validateUpload($data['image_1'] ?? '', false, true, 'image_1')->merge($errors);
		$errors = Image::validateUpload($data['image_2'] ?? '', false, true, 'image_2')->merge($errors);
		$errors = Image::validateUpload($data['image_3'] ?? '', false, true, 'image_3')->merge($errors);

		return $errors;
	}

	/**
	 * Validate update
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateUpdate($data)
	{
		return self::validateInsert($data);
	}
}
