<?php

namespace App\Project\Blog;

use App\Project\Lang as AppLang;

use DB;
use Validator;

class Lang
{
	/**
	 * Get id from slug
	 *
	 * @param string $blog_slug Blog slug
	 * @param integer $lang_id Lang id
	 * @return integer
	 */
	public static function slugId($blog_slug, $lang_id = 0)
	{
		$lang = DB::table('blogs_langs')
			->where('lang_id', $lang_id)
			->where('url', $blog_slug)
			->first();

		if ($lang === null)
			return 0;

		return $lang->blog_id;
	}

	/**
	 * Get langs
	 *
	 * @param integer $blog_id Blog id
	 * @param integer $lang_id Lang id
	 * @return array|object
	 */
	public static function get($blog_id, $lang_id = 0)
	{
		$langs = AppLang::allWithIds();

		// Get langs
		$blogs_langs = DB::table('blogs_langs')
			->where('blog_id', $blog_id);

		if ($lang_id !== 0) {
			$blogs_langs = $blogs_langs
				->where('lang_id', $lang_id);
		}

		$blogs_langs = $blogs_langs
			->get();

		// Format langs
		$blog = [];

		foreach ($blogs_langs as &$blog_lang)
			$blog[$langs[$blog_lang->lang_id]->code] = $blog_lang;

		if ($lang_id !== 0 && isset($blog[$langs[$lang_id]->code]))
			$blog = $blog[$langs[$lang_id]->code];

		return $blog;
	}

	/**
	 * Update langs
	 *
	 * @param object $blog Blog
	 * @param array $data Langs
	 * @return void
	 */
	public static function update($blog, $data)
	{
		// Format langs
		$insert_values = [
			'blog_id' => $blog->id,
			'lang_id' => $blog->lang_id,
			'title' => $data['title'] ?? '',
			'meta_description' => $data['meta_description'] ?? '',
			'description' => $data['description'] ?? '',
			'url' => str_slug($data['url'] ?? ''),
		];

		// Delete old langs
		self::deleteAll($blog);

		// Insert new langs
		DB::table('blogs_langs')->insert($insert_values);
	}

	/**
	 * Duplicate all langs
	 *
	 * @param object $blog Blog
	 * @param object $blog_duplicated Blog duplicated
	 * @return void
	 */
	public static function duplicateAll($blog, $blog_duplicated)
	{
		$test_i = 1;
		$while_security = true;
		$while_security_count = 10000;

		$name = $blog->langs->title.' - copy';
		$url = $blog->langs->url.'-'.$test_i;

		function test_url($lang_id, $url) {
			return DB::table('blogs_langs')->where(['lang_id' => $lang_id, 'url' => $url])->exists();
		}

		while (test_url($blog->lang_id, $url) && $while_security) {
			$url = $blog->langs->url.'-'.++$test_i;
			$while_security = $test_i < $while_security_count;
		}

		// Format langs
		$insert_values = [
			'blog_id' => $blog_duplicated->id,
			'lang_id' => $blog->lang_id,
			'title' => $name,
			'meta_description' => $blog->langs->meta_description,
			'description' => $blog->langs->description,
			'url' => $url,
		];

		// Insert new langs
		DB::table('blogs_langs')->insert($insert_values);
	}

	/**
	 * Delete all langs
	 *
	 * @param object $blog Blog
	 * @return void
	 */
	public static function deleteAll($blog)
	{
		DB::table('blogs_langs')
			->where('blog_id', $blog->id)
			->delete();
	}



	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate insert
	 *
	 * @param integer $lang_id Lang id
	 * @param array $data Array to validate
	 * @param integer $blog_id Blog id
	 * @return MessageBag
	 */
	public static function validateInsert($lang_id, $data, $blog_id = 0)
	{
		$errors = Validator::make($data, [
			'title' => 'required|string',
			'meta_description' => 'required|string',
			'description' => 'string',
			'url' => 'required|string|blog_url_unique:'.$blog_id.','.$lang_id,
		])->errors();

		return $errors;
	}

	/**
	 * Validate update
	 *
	 * @param integer $blog_id Blog id
	 * @param integer $lang_id Lang id
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateUpdate($blog_id, $lang_id, $data)
	{
		return self::validateInsert($lang_id, $data, $blog_id);
	}
}
