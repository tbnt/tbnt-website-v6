<?php

namespace App\Project\Blog;

use App\Project\Category as AppCategory;

use DB;
use Validator;

class Category
{
	/**
	 * Get all categories ids
	 *
	 * @param integer $blog_id Blog id
	 * @return array
	 */
	public static function allIds($blog_id)
	{
		$categories_ids = DB::table('blogs_categories')
			->where('blog_id', $blog_id)
			->pluck('category_id')
			->toArray();

		return array_filter($categories_ids ?? []);
	}

	/**
	 * Get all blogs ids from category id
	 *
	 * @param integer $category_id Category id
	 * @return array
	 */
	public static function allBlogsIds($category_id)
	{
		$blogs_ids = DB::table('blogs_categories')
			->where('category_id', $category_id)
			->pluck('blog_id')
			->toArray();

		return array_filter($blogs_ids ?? []);
	}

	/**
	 * Get all categories
	 *
	 * @param integer $blog_id Blog id
	 * @param integer $lang_id Lang id
	 * @return array
	 */
	public static function all($blog_id, $lang_id = 0)
	{
		$categories_ids = self::allIds($blog_id);

		foreach ($categories_ids as &$category_id)
			$category_id = AppCategory::one($category_id, $lang_id);

		return $categories_ids;
	}

	/**
	 * Update categories
	 *
	 * @param object $blog Blog
	 * @param array $categories_ids Categories ids
	 * @return void
	 */
	public static function update($blog, $categories_ids)
	{
		$insert_values = [];

		foreach ($categories_ids as &$category_id) {
			$insert_values[] = [
				'blog_id' => $blog->id,
				'category_id' => $category_id,
			];
		}

		// Delete old categories
		self::deleteAll($blog);

		// Insert new categories
		DB::table('blogs_categories')->insert($insert_values);
	}

	/**
	 * Duplicate all langs
	 *
	 * @param object $blog Blog
	 * @param object $blog_duplicated Blog duplicated
	 * @return void
	 */
	public static function duplicateAll($blog, $blog_duplicated)
	{
		$categories_ids = self::allIds($blog->id);
		$insert_values = [];

		foreach ($categories_ids as &$category_id) {
			$insert_values[] = [
				'blog_id' => $blog_duplicated->id,
				'category_id' => $category_id,
			];
		}

		// Insert new categories
		DB::table('blogs_categories')->insert($insert_values);
	}

	/**
	 * Delete all categories
	 *
	 * @param object $blog Blog
	 * @return void
	 */
	public static function deleteAll($blog)
	{
		DB::table('blogs_categories')
			->where('blog_id', $blog->id)
			->delete();
	}



	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate insert
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateInsert($data)
	{
		$errors = Validator::make(['categories' => $data], [
			'categories' => 'array',
			'categories.*' => 'required|numeric|category_exists',
		])->errors();

		return $errors;
	}

	/**
	 * Validate update
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateUpdate($data)
	{
		return self::validateInsert($data);
	}
}
