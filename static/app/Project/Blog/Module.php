<?php

namespace App\Project\Blog;

use App\Project\Module as AppModule;

use DB;
use Validator;

class Module
{
	/**
	 * Get all modules ids
	 *
	 * @param integer $blog_id Blog id
	 * @return array
	 */
	public static function allIds($blog_id)
	{
		$modules_ids = DB::table('blogs_modules')
			->where('blog_id', $blog_id)
			->orderBy('position')
			->pluck('module_id')
			->toArray();

		return array_filter($modules_ids ?? []);
	}

	/**
	 * Get all modules
	 *
	 * @param integer $blog_id Blog id
	 * @return array
	 */
	public static function all($blog_id)
	{
		$modules_ids = self::allIds($blog_id);

		foreach ($modules_ids as &$module_id)
			$module_id = AppModule::one($module_id);

		return $modules_ids;
	}

	/**
	 * Update modules
	 *
	 * @param object $blog Blog
	 * @param array $modules Modules
	 * @return void
	 */
	public static function update($blog, $modules)
	{
		$insert_values = [];
		$position = 0;

		$old_modules_ids = self::allIds($blog->id);
		$new_modules_ids = [];

		// Insert/update new modules
		foreach ($modules ?? [] as $module_id => &$module) {
			if (AppModule::exists($module_id) === false)
				$module_id = AppModule::insert($module);
			else
				AppModule::one($module_id)->update($module);

			$position++;

			$insert_values[] = [
				'blog_id' => $blog->id,
				'module_id' => $module_id,
				'position' => $position,
			];

			$new_modules_ids[] = $module_id;
		}

		// Delete old modules
		$delete_modules_ids = array_diff($old_modules_ids, $new_modules_ids);

		foreach ($delete_modules_ids as &$module_id)
			AppModule::one($module_id)->delete();

		// Delete old assigned modules
		self::deleteAll($blog);

		// Insert new assigned modules
		DB::table('blogs_modules')->insert($insert_values);
	}

	/**
	 * Duplicate all modules
	 *
	 * @param object $blog Blog
	 * @param object $blog_duplicated Blog duplicated
	 * @return void
	 */
	public static function duplicateAll($blog, $blog_duplicated)
	{
		$insert_values = [];

		// Duplicate new modules
		$modules = array_map(function($module) use ($blog) { return $module->duplicate($blog->id); }, self::all($blog->id));

		foreach ($modules as $position => &$module) {
			$insert_values[] = [
				'blog_id' => $blog_duplicated->id,
				'module_id' => $module->id,
				'position' => $position + 1,
			];
		}

		// Insert new assigned modules
		DB::table('blogs_modules')->insert($insert_values);
	}

	/**
	 * Delete all modules
	 *
	 * @param object $blog Blog
	 * @param boolean $force Delete modules
	 * @return void
	 */
	public static function deleteAll($blog, $force = false)
	{
		// Delete modules
		if ($force === true) {
			$modules_ids = self::allIds($blog->id);

			foreach ($modules_ids as &$module_id)
				AppModule::one($module_id)->delete();
		}

		DB::table('blogs_modules')
			->where('blog_id', $blog->id)
			->delete();
	}



	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate insert
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateInsert($data)
	{
		$errors = message_bag();

		foreach ($data as &$module)
			$errors = AppModule::validateInsert($module)->merge($errors);

		return $errors;
	}

	/**
	 * Validate update
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateUpdate($data)
	{
		$errors = message_bag();

		foreach ($data as &$module)
			$errors = AppModule::validateUpdate($module)->merge($errors);

		return $errors;
	}
}
