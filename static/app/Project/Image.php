<?php

namespace App\Project;

use App\Project;
use App\Project\Utils\Image as UtilsImage;

use DB;
use Validator;

class Image extends Project
{
	/**
	 * Constructor
	 *
	 * @param array $data Data
	 * @return void
	 */
	public function __construct($data)
	{
		parent::__construct($data);

		$this->format();
	}

	/**
	 * Format image
	 *
	 * @return void
	 */
	public function format()
	{
		$this->created_at_utc = to_utc_date($this->created_at);
		$this->updated_at_utc = to_utc_date($this->updated_at);

		$this->is_image = true;

		$this->filepath = dir_images($this->filename);
		$this->thumbpath = dir_thumbs($this->filename);

		$this->name = array_last(explode('/', $this->filepath));
	}

	/**
	 * Update image
	 *
	 * @param string $name Image name
	 * @return void
	 */
	public function update($name)
	{
		$this->_update($this, $name);
		$this->refreshData();
	}

	/**
	 * Duplicate image
	 *
	 * @param array $options Image options
	 * @return object
	 */
	public function duplicate($options = [])
	{
		$image_id = $this->_duplicate($this, $options);
		$this->refreshData();

		return self::one($image_id);
	}

	/**
	 * Refresh image data
	 *
	 * @return void
	 */
	public function refreshData()
	{
		$this->updateData($this->one($this->id));
	}

	/**
	 * Delete image
	 *
	 * @return void
	 */
	public function delete()
	{
		$this->_delete($this);
	}



	/****************************************************************
	 * Static
	 ***************************************************************/

	/**
	 * Check if image exists
	 *
	 * @param integer $image_id Image id
	 * @return boolean
	 */
	public static function exists($image_id)
	{
		return DB::table('files_images')
			->where('id', $image_id)
			->exists();
	}

	/**
	 * Get image
	 *
	 * @param integer $image_id Image id
	 * @return object
	 */
	public static function one($image_id)
	{
		$image = DB::table('files_images')
			->where('id', $image_id)
			->first();

		return self::initialize(__CLASS__, $image);
	}

	/**
	 * Get all images
	 *
	 * @return array
	 */
	public static function all()
	{
		$images = DB::table('files_images')
			->get();

		return self::initialize(__CLASS__, $images);
	}

	/**
	 * Upload image file
	 *
	 * @param integer $image_id Image id to replace
	 * @param array $image Image to upload
	 * @param array $options Upload options
	 * @return integer
	 */
	public static function upload($image_id, $image, $options = [])
	{
		// Remove image if exists
		$image_exists = self::exists($image_id);

		if ($image_exists === true)
			UtilsImage::delete(self::one($image_id)->filename);

		// Upload image
		$image_name = UtilsImage::upload($image, $options);

		// Update database
		if ($image_exists === true)
			self::one($image_id)->update($image_name);
		else
			$image_id = self::insert($image_name);

		return $image_id;
	}

	/**
	 * Insert image file
	 *
	 * @param string $image_name Image name
	 * @return integer
	 */
	public static function insert($image_name)
	{
		$now = date('Y-m-d H:i:s');

		// Format insert
		$insert_values = [
			'filename' => $image_name,
			'created_at' => $now,
		];

		// Insert database
		$image_id = DB::table('files_images')
			->insertGetId($insert_values);

		return $image_id;
	}

	/**
	 * Update image
	 *
	 * @param object $image Image
	 * @param string $name Image name
	 * @return void
	 */
	private static function _update($image, $name)
	{
		$now = date('Y-m-d H:i:s');

		// Format update
		$update_values = [
			'filename' => $name,
			'updated_at' => $now,
		];

		// Update database
		DB::table('files_images')
			->where('id', $image->id)
			->update($update_values);
	}

	/**
	 * Duplicate image
	 *
	 * @param object $image Image
	 * @param array $options Image options
	 * @return integer
	 */
	private static function _duplicate($image, $options = [])
	{
		$now = date('Y-m-d H:i:s');

		// Format duplicate
		$insert_values = [
			'filename' => UtilsImage::copy($image->filename, $options),
			'updated_at' => $now,
		];

		// Duplicate database
		$image_id = DB::table('files_images')
			->insertGetId($insert_values);

		return $image_id;
	}

	/**
	 * Delete image
	 *
	 * @param object $image Image
	 * @return void
	 */
	private static function _delete($image)
	{
		// Delete file
		UtilsImage::delete($image->filename);

		// Delete image
		DB::table('files_images')
			->where('id', $image->id)
			->delete();
	}



	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate uploads
	 *
	 * @param array $image Image to validate
	 * @param boolean $is_required File is required
	 * @param string $image_name File error name
	 * @return MessageBag
	 */
	public static function validateUpload($image, $is_required = false, $is_size = true, $image_name = 'image')
	{
		$image = gettype($image) !== 'array' ? [$image_name => $image] : $image;

		$max_size = UtilsImage::getMaxSize();
		$max_width = UtilsImage::getMaxWidth();

		$errors = Validator::make($image, [
			$image_name => ($is_required ? 'required|' : '').'image|max:'.$max_size.($is_size ? '|dimensions:max_width='.$max_width.',max_height='.$max_width : ''),
		])->errors();

		return $errors;
	}

	/**
	 * Check if uploaded
	 *
	 * @param array $image Image to validate
	 * @return boolean
	 */
	public static function isUploaded($image, $image_name = 'image')
	{
		return self::validateUpload($image, true, false, $image_name)->any() === false;
	}
}
