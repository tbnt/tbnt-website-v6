<?php

namespace App\Project\Utils;

use Image as FacadesImage;

class Image
{
	/**
	 * Image max size
	 *
	 * @var integer
	 */
	private static $image_max_size = 25 * 1024;

	/**
	 * Image max width
	 *
	 * @var integer
	 */
	private static $image_max_width = 2400;

	/**
	 * Thumbs max width
	 *
	 * @var integer
	 */
	private static $thumb_max_width = 800;

	/**
	 * Images upload dir
	 *
	 * @var string
	 */
	private static $image_uploads_dir = 'uploads/images';

	/**
	 * EXIF rotations complement
	 *
	 * @var string
	 */
	private static $exif_rotations = [
		1 => 0,
		2 => 0,
		3 => 180,
		4 => 180,
		5 => -90,
		6 => -90,
		7 => 90,
		8 => 90,
	];

	/**
	 * EXIF needs image flip
	 *
	 * @var string
	 */
	private static $exif_flip = [
		1 => false,
		2 => true,
		3 => false,
		4 => true,
		5 => true,
		6 => false,
		7 => true,
		8 => false,
	];

	/**
	 * Get image max size
	 *
	 * @return integer
	 */
	public static function getMaxSize()
	{
		return self::$image_max_size;
	}

	/**
	 * Get image max width
	 *
	 * @return integer
	 */
	public static function getMaxWidth()
	{
		return self::$image_max_width;
	}

	/**
	 * Get thumb max width
	 *
	 * @return integer
	 */
	public static function getThumbsMaxWidth()
	{
		return self::$thumb_max_width;
	}

	/**
	 * Format uploads dir images
	 * ex: $image_uploads_dir/image-folder
	 *
	 * @param string $dir Image dir
	 * @return string
	 */
	private static function formatDir($dir = '')
	{
		return slash_end(slash_end(self::$image_uploads_dir).trim($dir, '/'));
	}

	/**
	 * Format uploads dir thumbs
	 * ex: $image_uploads_dir/image-folder/thumbs
	 *
	 * @param string $dir Image dir
	 * @return string
	 */
	private static function formatThumbsDir($dir = '')
	{
		return slash_end(self::formatDir($dir).'thumbs');
	}

	/**
	 * Format absolute uploads dir images
	 * ex: //user_path_to_project/$image_uploads_dir/image-folder
	 *
	 * @param string $dir Image dir
	 * @return string
	 */
	private static function formatDirPath($dir = '')
	{
		return slash_end(base_path()).self::formatDir($dir);
	}

	/**
	 * Format absolute uploads dir thumbs
	 * ex: //user_path_to_project/$image_uploads_dir/image-folder/thumbs
	 *
	 * @param string $dir Image dir
	 * @return string
	 */
	private static function formatThumbsDirPath($dir = '')
	{
		return slash_end(base_path()).self::formatThumbsDir($dir);
	}

	/**
	 * Format image path
	 * ex: $image_uploads_dir/image-folder/image-filnename
	 *
	 * @param string $filepath Image filepath
	 * @return string
	 */
	public static function getDir($filepath)
	{
		$file_parts = self::getFileParts($filepath);

		return self::formatDir($file_parts['dir']).$file_parts['basename'];
	}

	/**
	 * Format thumb path
	 * ex: $image_uploads_dir/image-folder/thumbs/image-filnename
	 *
	 * @param string $filepath Image filepath
	 * @return string
	 */
	public static function getThumbsDir($filepath)
	{
		$file_parts = self::getFileParts($filepath);

		return self::formatThumbsDir($file_parts['dir']).$file_parts['basename'];
	}

	/**
	 * Format absolut image path
	 * ex: //user_path_to_project/$image_uploads_dir/image-folder/image-filnename
	 *
	 * @param string $filepath Image filepath
	 * @return string
	 */
	public static function getDirPath($filepath)
	{
		$file_parts = self::getFileParts($filepath);

		return self::formatDirPath($file_parts['dir']).$file_parts['basename'];
	}

	/**
	 * Format absolute thumb path
	 * ex: //user_path_to_project/$image_uploads_dir/image-folder/thumbs/image-filnename
	 *
	 * @param string $filepath Image filepath
	 * @return string
	 */
	public static function getThumbsDirPath($filepath)
	{
		$file_parts = self::getFileParts($filepath);

		return self::formatThumbsDirPath($file_parts['dir']).$file_parts['basename'];
	}

	/**
	 * Get filepath parts
	 *
	 * @param string $filepath Image filepath
	 * @return array
	 */
	private static function getFileParts($filepath)
	{
		$file_parts = explode('/', $filepath);

		$basename = array_pop($file_parts);
		$basename_parts = explode('.', $basename);

		$extension = array_pop($basename_parts);
		$filename = implode('.', $basename_parts);
		$dir = implode('/', $file_parts);

		return [
			'dir' => $dir,
			'extension' => $extension,
			'filename' => $filename,
			'basename' => $basename,
		];
	}

	/**
	 * Get image filename
	 *
	 * @param file $image Image file
	 * @param string $filename Image name
	 * @param string $ext Image extension
	 * @return string
	 */
	private static function formatName($image, $filename = '', $append = '', $ext = '')
	{
		$image_ext = $ext ?: $image->getClientOriginalExtension();

		return str_slug(trim($filename) ?: rtrim($image->getClientOriginalName(), '.'.$image->getClientOriginalExtension())).$append.'.'.$image_ext;
	}

	/**
	 * Upload image file
	 *
	 * @param file $image Image file
	 * @param array $options Image options
	 * @return string
	 */
	public static function upload($image, $options = [])
	{
		$options = array_merge([
			'name' => '',
			'path' => '',
		], $options);

		// Format image
		$basename = self::formatName($image, $options['name'], '');
		$image_dir_path = self::formatDirPath($options['path']);
		$thumbs_dir_path = self::formatThumbsDirPath($options['path']);

		// Init directories
		init_dir($image_dir_path);
		init_dir($thumbs_dir_path);

		// Check image name
		if (is_file($image_dir_path.$basename) === true)
			$basename = self::formatName($image, $options['name'], '-'.microtime(true));

		// Upload image
		$image->move($image_dir_path, $basename);
		$image_ext = $image->getClientOriginalExtension();

		// Format image
		$image = FacadesImage::make($image_dir_path.$basename);

		$exif_infos = $image->exif();
		$exif_orientation = $exif_infos['Orientation'] ?? 1;
		$exif_rotation = self::$exif_rotations[$exif_orientation] ?? 0;
		$exif_flip = self::$exif_flip[$exif_orientation] ?? false;

		$image->encode($image_ext, 80);
		// $image->rotate($exif_rotation);

		// if ($exif_flip === true)
		// 	$image->flip('h');

		$image->save($image_dir_path.$basename);

		// Create thumb
		FacadesImage::make($image_dir_path.$basename)
			->widen(min([$image->width(), $options['thumb'] ?? self::$thumb_max_width]))
			->encode($image_ext, 80)
			->save($thumbs_dir_path.$basename);

		return trim(slash_end($options['path']).$basename, '/');
	}

	/**
	 * Copy image file
	 *
	 * @param string $filepath Image filepath
	 * @param array $options Image options
	 * @return void
	 */
	public static function copy($filepath, $options = [])
	{
		$options = array_merge([
			'name' => '',
			'path' => '',
		], $options);

		$file_parts = self::getFileParts($filepath);

		$source_path = self::getDirPath($filepath);
		$source_path_thumb = self::getThumbsDirPath($filepath);

		$destination_name = $options['name'] ?: preg_replace('/(\-[0-9]{10}\.[0-9]{2})/i', '', $file_parts['filename']).'-'.microtime(true).'.'.$file_parts['extension'];
		$destination_dir = self::formatDirPath($options['path']);
		$destination_dir_thumb = self::formatThumbsDirPath($options['path']);

		init_dir($destination_dir);
		init_dir($destination_dir_thumb);

		$destination_path = $destination_dir.$destination_name;
		$destination_path_thumb = $destination_dir_thumb.$destination_name;

		copy($source_path, $destination_path);
		copy($source_path_thumb, $destination_path_thumb);

		return trim(slash_end($options['path']).$destination_name, '/');
	}

	/**
	 * Delete image file
	 *
	 * @param string $filepath Image filepath
	 * @return void
	 */
	public static function delete($filepath)
	{
		// Delete image
		$image_path = self::getDirPath($filepath);
		if (is_file($image_path) === true) unlink($image_path);

		// Delete thumb
		$thumb_path = self::getThumbsDirPath($filepath);
		if (is_file($thumb_path) === true) unlink($thumb_path);
	}
}
