<?php

namespace App\Project\Utils;

class Domain
{
	/**
	 * Check if DNS respond
	 *
	 * @param string $domain Domain
	 * @return boolean
	 */
	static public function check($domain)
	{
		try {
			$dns = $domain !== '' && dns_get_record($domain, DNS_A);
		}
		catch (\Exception $e) {
			return false;
		}

		return $dns !== false && count($dns);
	}
}
