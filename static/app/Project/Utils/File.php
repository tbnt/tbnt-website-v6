<?php

namespace App\Project\Utils;

class File
{
	/**
	 * Files max size in kb
	 *
	 * @var integer
	 */
	private static $file_max_size = 2 * 1024;

	/**
	 * Files extensions authorized
	 *
	 * @var integer
	 */
	private static $file_ext_valid = [
		'pdf',
		'csv',
		'xsl',
		'xlsx',
		'doc',
		'docx',
		'ppt',
		'pptx',
	];

	/**
	 * Files mimes types authorized
	 *
	 * @var integer
	 */
	private static $file_mt_valid = [
		'application/pdf',
		'text/csv',
		'application/vnd.ms-excel',
		'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		'application/msword',
		'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
		'application/vnd.ms-powerpoint',
		'application/vnd.openxmlformats-officedocument.presentationml.presentation',
	];

	/**
	 * Files upload dir
	 *
	 * @var string
	 */
	private static $file_uploads_dir = 'uploads/files';

	/**
	 * Get files max size
	 *
	 * @return integer
	 */
	public static function getMaxSize()
	{
		return self::$file_max_size;
	}

	/**
	 * Get files max size
	 *
	 * @return array
	 */
	public static function getValidExtensions()
	{
		return self::$file_ext_valid;
	}

	/**
	 * Get files max size
	 *
	 * @return array
	 */
	public static function getValidMimesTypes()
	{
		return self::$file_mt_valid;
	}

	/**
	 * Check if file is image
	 *
	 * @param string $filepath Filepath
	 * @return boolean
	 */
	public static function isImage($filepath)
	{
		return @is_array(getimagesize($filepath));
	}

	/**
	 * Check if extension is authorized
	 *
	 * @return boolean
	 */
	public static function isExtenstionValid($ext)
	{
		return in_array($ext, $file_ext_valid);
	}

	/**
	 * Check if mime type is authorized
	 *
	 * @return boolean
	 */
	public static function isMimeTypeValid($mime_type)
	{
		return in_array($mime_type, $file_mt_valid);
	}

	/**
	 * Format uploads dir files
	 * ex: $file_uploads_dir/file-folder
	 *
	 * @param string $dir File dir
	 * @return string
	 */
	private static function formatDir($dir = '')
	{
		return slash_end(slash_end(self::$file_uploads_dir).trim($dir, '/'));
	}

	/**
	 * Format absolute uploads dir files
	 * ex: //user_path_to_project/$file_uploads_dir/file-folder
	 *
	 * @param string $dir File dir
	 * @return string
	 */
	private static function formatDirPath($dir = '')
	{
		return slash_end(base_path()).self::formatDir($dir);
	}

	/**
	 * Format file path
	 * ex: $file_uploads_dir/file-folder/file-filnename
	 *
	 * @param string $filepath Filepath
	 * @return string
	 */
	public static function getDir($filepath)
	{
		$file_parts = self::getFileParts($filepath);

		return self::formatDir($file_parts['dir']).$file_parts['basename'];
	}

	/**
	 * Format absolute file path
	 * ex: //user_path_to_project/$file_uploads_dir/file-folder/file-filnename
	 *
	 * @param string $filepath Filepath
	 * @return string
	 */
	public static function getDirPath($filepath)
	{
		$file_parts = self::getFileParts($filepath);

		return self::formatDirPath($file_parts['dir']).$file_parts['basename'];
	}

	/**
	 * Get filepath parts
	 *
	 * @param string $filepath Filepath
	 * @return array
	 */
	private static function getFileParts($filepath)
	{
		$file_parts = explode('/', $filepath);

		$basename = array_pop($file_parts);
		$basename_parts = explode('.', $basename);

		$extension = array_pop($basename_parts);
		$filename = implode('.', $basename_parts);
		$dir = implode('/', $file_parts);

		return [
			'dir' => $dir,
			'extension' => $extension,
			'filename' => $filename,
			'basename' => $basename,
		];
	}

	/**
	 * Get filename
	 *
	 * @param file $file File
	 * @param string $filename File name
	 * @return string
	 */
	private static function formatName($file, $filename = '', $append = '')
	{
		$file_ext = $file->getClientOriginalExtension();

		return str_slug(trim($filename) ?: rtrim($file->getClientOriginalName(), '.'.$file_ext)).$append.'.'.$file_ext;
	}

	/**
	 * Upload file
	 *
	 * @param file $file File
	 * @param array $options File options
	 * @return string
	 */
	public static function upload($file, $options = [])
	{
		$options = array_merge([
			'name' => '',
			'path' => '',
		], $options);

		// Format file
		$basename = self::formatName($file, $options['name']);
		$file_dir_path = self::formatDirPath($options['path']);

		// Init directories
		init_dir($file_dir_path);

		// Upload file
		if (is_file($file_dir_path.$basename) === true)
			$basename = self::formatName($file, $options['name'], '-'.microtime(true));

		$file->move($file_dir_path, $basename);

		return trim(slash_end($options['path']).$basename, '/');
	}

	/**
	 * Delete file
	 *
	 * @param string $filepath Filepath
	 * @return void
	 */
	public static function delete($filepath)
	{
		$file_path = self::getDirPath($filepath);
		if (is_file($file_path) === true) unlink($file_path);
	}
}
