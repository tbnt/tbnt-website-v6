<?php

namespace App\Project\Utils;

class Google
{
	/**
	 * Get lat/lng from address
	 *
	 * @param string $address Address to geocode
	 * @return array
	 */
	static public function geocode($address)
	{
		if (is_array($address) === true)
			$address = self::formatAddress($address);

		$address = remove_accents($address);

		// Geocode
		$url = 'http://maps.google.com/maps/api/geocode/json';
		$data = ['query' => ['sensor' => false, 'address' => urlencode($address)]];

		$client = new \GuzzleHttp\Client(['defaults' => ['exceptions' => false]]);
		$response = $client->get($url, $data);

		$response = json_decode($response->getBody());

		// Check status
		if ($response->status != 'OK')
			return false;

		// Get lat lng
		$geocode_result = $response->results[0];

		$lat = $geocode_result->geometry->location->lat;
		$lng = $geocode_result->geometry->location->lng;

		$formatted_address = $geocode_result->formatted_address;
		$address_components = self::componentAddress($geocode_result->address_components);

		if (($lat && $lng && $formatted_address) === false)
			return false;

		return array_to_object(['lat' => $lat, 'lng' => $lng, 'address' => $formatted_address, 'components' => $address_components]);
	}

	/**
	 * Format address
	 *
	 * @param array $address Address to format
	 * @return string
	 */
	static public function formatAddress($address)
	{
		$address_formated = '';

		if (is_array($address) === false)
			return $address_formated;

		if (in_array(false, [isset($address['street']), isset($address['zip']), isset($address['city'])]) === false)
			$address_formated = $address['street'].', '.$address['zip'].' '.$address['city'];

		if (in_array(false, [isset($address[0]), isset($address[1]), isset($address[2])]) === false)
			$address_formated = $address[0].', '.$address[1].' '.$address[2];

		return $address_formated;
	}

	/**
	 * Format components address
	 *
	 * @param array $components Components to format
	 * @return array
	 */
	static private function componentAddress($components)
	{
		$address_components = array_to_object(['street' => '', 'zip' => '', 'city' => '', 'country' => '']);

		array_map(function ($value) use (&$address_components)
		{
			if (in_array('street_number', $value->types)) $address_components->street = trim($address_components->street).' '.trim($value->long_name);
			if (in_array('route', $value->types)) $address_components->street = trim($value->long_name).' '.trim($address_components->street);
			if (in_array('postal_code', $value->types)) $address_components->zip = trim($value->long_name);
			if (in_array('locality', $value->types)) $address_components->city = trim($value->long_name);
			if (in_array('country', $value->types)) $address_components->country = trim($value->long_name);
		}, $components);

		return $address_components;
	}
}
