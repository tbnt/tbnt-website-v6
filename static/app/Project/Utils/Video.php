<?php

namespace App\Project\Utils;

class Video
{
	static public function convert_embed($link)
	{
		if (strpos($link, 'youtu') !== false)
			return self::convert_youtube($link);

		if (strpos($link, 'vimeo') !== false)
			return self::convert_vimeo($link);

		if (strpos($link, 'daily') !== false)
			return self::convert_dailymotion($link);

		return $link;
	}

	static public function convert_youtube($link)
	{
		$regex = '/(?:http?s?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?(.+)/i';
		$replacement = 'https://www.youtube.com/embed/$1';

		return self::convert_link($regex, $replacement, $link);
	}

	static public function convert_vimeo($link)
	{
		$regex = '/(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(.+)/i';
		$replacement = 'https://player.vimeo.com/video/$1';

		return self::convert_link($regex, $replacement, $link);
	}

	static public function convert_dailymotion($link)
	{
		$regex = '/(?:http?s?:\/\/)?(?:www\.)?(?:dailymotion\.com)\/(?:video\/)?(.+)/i';
		$replacement = 'https://www.dailymotion.com/embed/video/$1';

		return self::convert_link($regex, $replacement, $link);
	}

	static private function convert_link($regex, $replacement, $link)
	{
		if (preg_match($regex, $link) !== 0
			&& preg_match('/'.str_replace(['/', ':', '.', '$1'], ['\\/', '\\:', '\\.', '(.+)'], $replacement).'/i', $link) === 0)
		{
			$link = preg_replace($regex, $replacement, $link);
		}

		$link = str_replace('&', '?', $link);
		$pos = strpos($link, '?');

		if ($pos !== false)
			$link = substr($link, 0, $pos + 1).str_replace('?', '&', substr($link, $pos + 1));

		return $link;
	}
}
