<?php

namespace App\Project;

use App\Project;
use App\Project\Category\Blog as CategoryBlog;
use App\Project\Category\Lang as CategoryLang;

use DB;
use Validator;

class Category extends Project
{
	/**
	 * Constructor
	 *
	 * @param array $data Data
	 * @param integer $lang_id Lang id
	 * @return void
	 */
	public function __construct($data, $lang_id = 0)
	{
		parent::__construct($data);

		$this->lang_id = $lang_id;
		$this->format();
	}

	/**
	 * Format category
	 *
	 * @return void
	 */
	public function format()
	{
		$this->created_at_utc = to_utc_date($this->created_at);
		$this->updated_at_utc = to_utc_date($this->updated_at);

		$this->langs = CategoryLang::get($this->id, $this->lang_id);
	}

	/**
	 * Update category
	 *
	 * @param array $data Category data
	 * @return void
	 */
	public function update($data)
	{
		$this->_update($this, $data);
		$this->refreshData();
	}

	/**
	 * Refresh category data
	 *
	 * @return void
	 */
	public function refreshData()
	{
		$this->updateData($this->one($this->id));
	}

	/**
	 * Delete category
	 *
	 * @return void
	 */
	public function delete()
	{
		$this->_delete($this);
	}



	/****************************************************************
	 * Static
	 ***************************************************************/

	/**
	 * Check if category exists
	 *
	 * @param integer $category_id Category id
	 * @return boolean
	 */
	public static function exists($category_id)
	{
		return DB::table('categories')
			->where('id', $category_id)
			->exists();
	}

	/**
	 * Get category id from slug
	 *
	 * @param string $category_slug Category slug
	 * @param integer $lang_id Lang id
	 * @return integer
	 */
	public static function slugId($category_slug, $lang_id = 0)
	{
		return CategoryLang::slugId($category_slug, $lang_id);
	}

	/**
	 * Get category
	 *
	 * @param integer $category_id Category id
	 * @param integer $lang_id Lang id
	 * @return object
	 */
	public static function one($category_id, $lang_id = 0)
	{
		$category = DB::table('categories')
			->where('id', $category_id)
			->first();

		return self::initialize(__CLASS__, $category, $lang_id);
	}

	/**
	 * Get first category
	 *
	 * @param integer $lang_id Lang id
	 * @return object
	 */
	public static function first($lang_id = 0)
	{
		$category = DB::table('categories')
			->first();

		return self::initialize(__CLASS__, $category, $lang_id);
	}

	/**
	 * Get all categories
	 *
	 * @param integer $lang_id Lang id
	 * @return array
	 */
	public static function all($lang_id = 0)
	{
		$categories = DB::table('categories')
			->get();

		return self::initialize(__CLASS__, $categories, $lang_id);
	}

	/**
	 * Insert category
	 *
	 * @param array $data Category data
	 * @return integer
	 */
	public static function insert($data)
	{
		$now = date('Y-m-d H:i:s');

		// Format insert
		$insert_values = [
			'created_at' => $now,
		];

		// Insert category
		$category_id = DB::table('categories')
			->insertGetId($insert_values);

		// Get category
		$category = self::one($category_id);

		// Insert langs
		CategoryLang::update($category, $data['langs'] ?? []);

		return $category_id;
	}

	/**
	 * Update category
	 *
	 * @param object $category Category
	 * @param array $data Category data
	 * @return void
	 */
	private static function _update($category, $data)
	{
		$now = date('Y-m-d H:i:s');

		// Format update
		$update_values = [
			'updated_at' => $now,
		];

		// Update category
		DB::table('categories')
			->where('id', $category->id)
			->update($update_values);

		// Update langs
		CategoryLang::update($category, $data['langs'] ?? []);
	}

	/**
	 * Delete category
	 *
	 * @param object $category Category
	 * @return void
	 */
	private static function _delete($category)
	{
		// Delete langs
		CategoryLang::deleteAll($category);

		// Delete blogs
		CategoryBlog::deleteAll($category);

		// Delete category
		DB::table('categories')
			->where('id', $category->id)
			->delete();
	}



	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate exists
	 *
	 * @param integer $category_id Category id
	 * @return MessageBag
	 */
	public static function validateExists($category_id)
	{
		$errors = Validator::make(['category_id' => $category_id], [
			'category_id' => 'required|numeric|category_exists',
		])->errors();

		return $errors;
	}

	/**
	 * Validate insert
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateInsert($data)
	{
		$errors = CategoryLang::validateInsert($data['langs'] ?? []);

		return $errors;
	}

	/**
	 * Validate update
	 *
	 * @param integer $category_id Category id
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateUpdate($category_id, $data)
	{
		$errors = self::validateExists($category_id);
		$errors = CategoryLang::validateUpdate($category_id, $data['langs'] ?? [])->merge($errors);

		return $errors;
	}
}
