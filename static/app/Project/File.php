<?php

namespace App\Project;

use App\Project;
use App\Project\Utils\File as UtilsFile;

use DB;
use Validator;

class File extends Project
{
	/**
	 * Constructor
	 *
	 * @param array $data Data
	 * @return void
	 */
	public function __construct($data)
	{
		parent::__construct($data);

		$this->format();
	}

	/**
	 * Format file
	 *
	 * @return void
	 */
	public function format()
	{
		$this->created_at_utc = to_utc_date($this->created_at);
		$this->updated_at_utc = to_utc_date($this->updated_at);

		$this->is_image = UtilsFile::isImage($this->filepath);

		$this->filepath = dir_files($this->filename);

		if ($this->is_image === true)
			$this->thumbpath = $this->filepath;

		$this->name = array_last(explode('/', $this->filepath));
	}

	/**
	 * Update file
	 *
	 * @param string $name File name
	 * @return void
	 */
	public function update($name)
	{
		$this->_update($this, $name);
		$this->refreshData();
	}

	/**
	 * Refresh file data
	 *
	 * @return void
	 */
	public function refreshData()
	{
		$this->updateData($this->one($this->id));
	}

	/**
	 * Delete file
	 *
	 * @return void
	 */
	public function delete()
	{
		$this->_delete($this);
	}



	/****************************************************************
	 * Static
	 ***************************************************************/

	/**
	 * Check if file exists
	 *
	 * @param integer $file_id File id
	 * @return boolean
	 */
	public static function exists($file_id)
	{
		return DB::table('files')
			->where('id', $file_id)
			->exists();
	}

	/**
	 * Get file
	 *
	 * @param integer $file_id File id
	 * @return object
	 */
	public static function one($file_id)
	{
		$file = DB::table('files')
			->where('id', $file_id)
			->first();

		return self::initialize(__CLASS__, $file);
	}

	/**
	 * Get all files
	 *
	 * @return array
	 */
	public static function all()
	{
		$files = DB::table('files')
			->get();

		return self::initialize(__CLASS__, $files);
	}

	/**
	 * Upload file file
	 *
	 * @param integer $file_id File id to replace
	 * @param array $file File to upload
	 * @param array $options Upload options
	 * @return integer
	 */
	public static function upload($file_id, $file, $options = [])
	{
		// Remove file if exists
		$file_exists = self::exists($file_id);

		if ($file_exists === true)
			UtilsFile::delete(self::one($file_id)->filename);

		// Upload file
		$file_name = UtilsFile::upload($file, $options);

		// Update database
		if ($file_exists === true)
			self::one($file_id)->update($file_name);
		else
			$file_id = self::insert($file_name);

		return $file_id;
	}

	/**
	 * Insert file file
	 *
	 * @param string $file_name File name
	 * @return integer
	 */
	public static function insert($file_name)
	{
		$now = date('Y-m-d H:i:s');

		// Format insert
		$insert_values = [
			'filename' => $file_name,
			'created_at' => $now,
		];

		// Insert database
		$file_id = DB::table('files')
			->insertGetId($insert_values);

		return $file_id;
	}

	/**
	 * Update file
	 *
	 * @param object $file File
	 * @param string $name File name
	 * @return void
	 */
	private static function _update($file, $name)
	{
		$now = date('Y-m-d H:i:s');

		// Format update
		$update_values = [
			'filename' => $name,
			'updated_at' => $now,
		];

		// Update database
		DB::table('files')
			->where('id', $file->id)
			->update($update_values);
	}

	/**
	 * Delete file
	 *
	 * @param object $file File
	 * @return void
	 */
	private static function _delete($file)
	{
		// Delete file
		UtilsFile::delete($file->filename);

		// Delete file
		DB::table('files')
			->where('id', $file->id)
			->delete();
	}



	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate uploads
	 *
	 * @param array $file File to validate
	 * @param boolean $is_required File is required
	 * @param string $file_name File error name
	 * @return MessageBag
	 */
	public static function validateUpload($file, $is_required = false, $is_mimes = true, $file_name = 'file')
	{
		$file = gettype($file) !== 'array' ? [$file_name => $file] : $file;

		$max_size = UtilsFile::getMaxSize();
		$extensions = UtilsFile::getValidExtensions();

		$errors = Validator::make($file, [
			$file_name => ($is_required ? 'required|' : '').'file|max:'.$max_size.($is_mimes ? '|mimes:'.(implode(',', $extensions)) : ''),
		])->errors();

		return $errors;
	}

	/**
	 * Check if uploaded
	 *
	 * @param array $file File to validate
	 * @return boolean
	 */
	public static function isUploaded($file, $file_name = 'file')
	{
		return self::validateUpload($file, true, false, $file_name)->any() === false;
	}
}
