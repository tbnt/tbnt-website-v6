<?php

namespace App\Project\Module;

use App\Project;

use DB;

class Type extends Project
{
	/**
	 * Constructor
	 *
	 * @param array $data Data
	 * @return void
	 */
	public function __construct($data)
	{
		parent::__construct($data);

		$this->format();
	}

	/**
	 * Format module type
	 *
	 * @return void
	 */
	public function format()
	{
		$this->created_at_utc = to_utc_date($this->created_at);
		$this->updated_at_utc = to_utc_date($this->updated_at);
	}



	/****************************************************************
	 * Static
	 ***************************************************************/

	/**
	 * Check if module type id exists
	 *
	 * @param integer $module_type_id Module type id
	 * @return boolean
	 */
	public static function exists($module_type_id)
	{
		return DB::table('modules_types')
			->where('id', $module_type_id)
			->exists();
	}

	/**
	 * Check if module type exists
	 *
	 * @param string $module_type Module type
	 * @return boolean
	 */
	public static function typeExists($module_type)
	{
		return DB::table('modules_types')
			->where('type', $module_type)
			->exists();
	}

	/**
	 * Get module type
	 *
	 * @param integer $module_id Module id
	 * @return object
	 */
	public static function one($module_id)
	{
		$module_type = DB::table('modules_types')
			->where('id', $module_id)
			->first();

		return self::initialize(__CLASS__, $module_type);
	}

	/**
	 * Get all modules types
	 *
	 * @return array
	 */
	public static function all()
	{
		$modules_types = DB::table('modules_types')
			->orderBy('position', 'ASC')
			->get();

		return self::initialize(__CLASS__, $modules_types);
	}

	/**
	 * Get module id
	 *
	 * @param string $module_type Module type
	 * @return integer
	 */
	public static function id($module_type)
	{
		return DB::table('modules_types')
			->where('type', $module_type)
			->first()->id ?? 0;
	}

	/**
	 * Get module type
	 *
	 * @param integer $module_type_id Module type id
	 * @return string
	 */
	public static function type($module_type_id)
	{
		return DB::table('modules_types')
			->where('id', $module_type_id)
			->first()->type ?? '';
	}
}
