<?php

namespace App\Project;

use App\Project;

use App;
use Request;

class Lang extends Project
{
	/**
	 * Constructor
	 *
	 * @param array $data Data
	 * @return void
	 */
	public function __construct($data)
	{
		parent::__construct($data);
	}



	/****************************************************************
	 * Static
	 ***************************************************************/

	/**
	 * Langs
	 *
	 * @var array
	 */
	private static $langs = [];

	/**
	 * Check if lang id exists
	 *
	 * @param integer $lang_id Lang id
	 * @return boolean
	 */
	public static function exists($lang_id)
	{
		return in_array($lang_id, self::allIds());
	}

	/**
	 * Check if lang code exists
	 *
	 * @param string $lang_code Lang code
	 * @return boolean
	 */
	public static function codeExists($lang_code)
	{
		return in_array($lang_code, self::allCodes());
	}

	/**
	 * Get lang id from code
	 *
	 * @param string $lang_code Lang code
	 * @return integer
	 */
	public static function getId($lang_code)
	{
		return self::allWithCodes()[$lang_code]->id ?? 0;
	}

	/**
	 * Get lang code from id
	 *
	 * @param integer $lang_id Lang id
	 * @return string
	 */
	public static function getCode($lang_id)
	{
		return self::allWithIds()[$lang_id]->code ?? '';
	}

	/**
	 * Get default lang code
	 *
	 * @return string
	 */
	public static function getDefaultCode()
	{
		return config('langs.default');
	}

	/**
	 * Get current lang
	 *
	 * @return object
	 */
	public static function getDefault()
	{
		return self::oneByCode(self::getDefaultCode());
	}

	/**
	 * Get current lang id
	 *
	 * @return integer
	 */
	public static function getCurrentId()
	{
		return self::getCurrent()->id ?? 0;
	}

	/**
	 * Get current lang
	 *
	 * @return object
	 */
	public static function getCurrent()
	{
		return self::allWithCodes()[App::getLocale()] ?? null;
	}

	/**
	 * Get current lang
	 *
	 * @param string $lang_code Lang code
	 * @return void
	 */
	public static function setCurrent($lang_code)
	{
		App::setLocale($lang_code);
		Request::instance()->lang = self::getCurrent();
	}

	/**
	 * Get lang
	 *
	 * @param integer $lang_id Lang id
	 * @return object
	 */
	static public function one($lang_id)
	{
		return self::allWithIds()[$lang_id] ?? null;
	}

	/**
	 * Get lang by code
	 *
	 * @param string $lang_code Lang code
	 * @return object
	 */
	static public function oneByCode($lang_code)
	{
		return self::one(self::getId($lang_code));
	}

	/**
	 * Get all langs
	 *
	 * @return array
	 */
	static public function all()
	{
		return self::_config();
	}

	/**
	 * Get all langs ids
	 *
	 * @return array
	 */
	public static function allIds()
	{
		return array_pluck(self::_config(), 'id');
	}

	/**
	 * Get all langs codes
	 *
	 * @return array
	 */
	public static function allCodes()
	{
		return array_pluck(self::_config(), 'code');
	}

	/**
	 * Get all langs by ids
	 *
	 * @return array
	 */
	public static function allWithIds()
	{
		return array_combine(self::allIds(), self::_config());
	}

	/**
	 * Get all langs by codes
	 *
	 * @return array
	 */
	public static function allWithCodes()
	{
		return array_combine(self::allCodes(), self::_config());
	}

	/**
	 * Get all langs from config
	 *
	 * @return array
	 */
	static private function _config()
	{
		if (count(self::$langs) !== 0)
			return self::$langs;

		$langs = config('langs.langs');
		$langs = array_map(
			function($lang, $code, $i) { $lang['id'] = $i + 1; $lang['code'] = $code; return $lang; },
			$langs, array_keys($langs), array_keys(array_values($langs))
		);

		return self::$langs = self::initialize(__CLASS__, $langs);
	}
}
