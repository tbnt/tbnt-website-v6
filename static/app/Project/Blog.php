<?php

namespace App\Project;

use App\Project;
use App\Project\Blog\Category as BlogCategory;
use App\Project\Blog\Lang as BlogLang;
use App\Project\Blog\Module as BlogModule;
use App\Project\Image;
use App\Project\Utils\Image as UtilsImage;

use DB;
use Validator;

class Blog extends Project
{
	/**
	 * Constructor
	 *
	 * @param array $data Data
	 * @return void
	 */
	public function __construct($data)
	{
		parent::__construct($data);

		$this->format();
	}

	/**
	 * Get modules
	 *
	 * @return array
	 */
	public function getModules()
	{
		return BlogModule::all($this->id);
	}

	/**
	 * Format blog
	 *
	 * @return void
	 */
	public function format()
	{
		$this->visible_at_utc = to_utc_date($this->visible_at);
		$this->created_at_utc = to_utc_date($this->created_at);
		$this->updated_at_utc = to_utc_date($this->updated_at);

		$this->langs = BlogLang::get($this->id, $this->lang_id);
		$this->categories = BlogCategory::all($this->id, $this->lang_id);

		$this->thumbnail = Image::one($this->thumbnail_id);
		$this->cover = Image::one($this->image_id);
	}

	/**
	 * Update blog
	 *
	 * @param array $data Blog data
	 * @return void
	 */
	public function update($data)
	{
		$this->_update($this, $data);
		$this->refreshData();
	}

	/**
	 * Duplicate blog
	 *
	 * @return object
	 */
	public function duplicate()
	{
		$blog_id = $this->_duplicate($this);
		$this->refreshData();

		return self::one($blog_id);
	}

	/**
	 * Refresh blog data
	 *
	 * @return void
	 */
	public function refreshData()
	{
		$this->updateData($this->one($this->id));
	}

	/**
	 * Delete blog
	 *
	 * @return void
	 */
	public function delete()
	{
		$this->_delete($this);
	}



	/****************************************************************
	 * Static
	 ***************************************************************/

	/**
	 * Get uploads dir base name
	 *
	 * @param object $blog Blog
	 * @return string
	 */
	public static function getUploadsDirBaseName($blog)
	{
		return 'blogs/'.$blog->id;
	}

	/**
	 * Check if blog exists
	 *
	 * @param integer $blog_id Blog id
	 * @return boolean
	 */
	public static function exists($blog_id)
	{
		return DB::table('blogs')
			->where('id', $blog_id)
			->exists();
	}

	/**
	 * Get blog id from slug
	 *
	 * @param string $blog_slug Blog slug
	 * @param integer $lang_id Lang id
	 * @return integer
	 */
	public static function slugId($blog_slug, $lang_id = 0)
	{
		return BlogLang::slugId($blog_slug, $lang_id);
	}

	/**
	 * Get blog
	 *
	 * @param integer $blog_id Blog id
	 * @param integer $lang_id Lang id
	 * @return object
	 */
	public static function one($blog_id, $lang_id = 0)
	{
		$blog = DB::table('blogs')
			->where('id', $blog_id)
			->first();

		return self::initialize(__CLASS__, $blog, $lang_id);
	}

	/**
	 * Get first blog
	 *
	 * @param integer $blog_id Blog id
	 * @param integer $lang_id Lang id
	 * @return array
	 */
	public static function first($blog_id = 0, $lang_id = 0)
	{
		$now = date('Y-m-d H:i:s');

		$blog = DB::table('blogs')
			->where('is_active', 1)
			->where('visible_at', '<=', $now);

		if ($blog_id !== 0) {
			$blog = $blog
				->whereIn('id', array_flatten(array_map(function($category_id) {
					return BlogCategory::allBlogsIds($category_id);
				}, BlogCategory::allIds($blog_id))));
		}

		if ($lang_id !== 0) {
			$blog = $blog
				->where('lang_id', $lang_id);
		}

		$blog = $blog
			->orderBy('visible_at', 'DESC')
			->take(1)
			->first();

		return self::initialize(__CLASS__, $blog, $lang_id);
	}

	/**
	 * Get next blog
	 *
	 * @param object $blog Blog
	 * @return array
	 */
	public static function next($blog)
	{
		$next_blog = DB::table('blogs')
			->where('id', '!=', $blog->id)
			->whereIn('id', array_flatten(array_map(function($category_id) {
				return BlogCategory::allBlogsIds($category_id);
			}, BlogCategory::allIds($blog->id))))
			->where('lang_id', $blog->lang_id)
			->where('is_active', 1)
			->where('visible_at', '<=', $blog->visible_at)
			->orderBy('visible_at', 'DESC')
			->first();

		if ($next_blog === null)
			return self::first($blog->id, $blog->lang_id);

		return self::initialize(__CLASS__, $next_blog, $blog->lang_id);
	}

	/**
	 * Get all blogs
	 *
	 * @param integer $lang_id Lang id
	 * @param array $filters Filters
	 * @return array
	 */
	public static function all($lang_id = 0, $filters = null)
	{
		$blogs = DB::table('blogs');

		if ($filters !== null) {
			$blogs = $blogs
				->whereIn('id', self::getIds($filters))
				->orderBy('visible_at', 'DESC')
				->get();
		}
		else {
			$blogs = $blogs
				->orderBy('visible_at', 'DESC')
				->get();
		}

		return self::initialize(__CLASS__, $blogs, $lang_id);
	}

	/**
	 * Get blog ids
	 *
	 * @param array $filters Filters
	 * @return array
	 */
	public static function getIds($filters = [])
	{
		$options = array_merge([
			'start' => 0,
			'count' => 20,
			'ids' => -1,
			'category' => -1,
			'lang' => -1,
			'from' => date('Y-m-d H:i:s'),
		], $filters);

		$options['start'] = intval($options['start']);
		$options['count'] = intval($options['count']);
		$options['category'] = intval($options['category']);
		$options['lang'] = intval($options['lang']);

		// Init db
		$blogs_db = DB::table('blogs as b');

		// Skip
		if ($options['start'] !== 0) {
			$blogs_db = $blogs_db
				->skip($options['start']);
		}

		// Take
		if ($options['count'] !== -1) {
			$blogs_db = $blogs_db
				->take($options['count']);
		}

		// Filter ids
		if ($options['ids'] !== -1) {
			$options['ids'] = init_array($options['ids']);

			$blogs_db = $blogs_db
				->whereIn('b.id', $options['ids']);
		}

		// Filter category
		if ($options['category'] !== -1) {
			$blogs_db = $blogs_db
				->whereIn('b.id', BlogCategory::allBlogsIds($options['category']));
		}

		// Filter lang
		if ($options['lang'] !== -1) {
			$blogs_db = $blogs_db
				->where('b.lang_id', $options['lang']);
		}

		// Get ids
		$blogs_ids = $blogs_db
			->where('b.is_active', 1)
			->where('b.visible_at', '<=', $options['from'])
			->orderBy('b.visible_at', 'DESC')
			->pluck('b.id')
			->toArray();

		return array_filter($blogs_ids ?? []);
	}

	/**
	 * Insert blog
	 *
	 * @param array $data Blog data
	 * @return integer
	 */
	public static function insert($data)
	{
		$now = date('Y-m-d H:i:s');

		// Format insert
		$insert_values = [
			'lang_id' => $data['lang_id'],
			'is_active' => $data['is_active'] ?? 0,
			'visible_at' => $data['visible_at'] ?? '',
			'created_at' => $now,
		];

		// Insert blog
		$blog_id = DB::table('blogs')
			->insertGetId($insert_values);

		// Get blog
		$blog = self::one($blog_id);

		// Insert thumbnail
		self::_updateThumbnail($blog, $data['thumbnail']);

		// Insert cover
		self::_updateCover($blog, $data['cover']);

		// Insert langs
		BlogLang::update($blog, $data['langs'] ?? []);

		// Insert categories
		BlogCategory::update($blog, $data['categories'] ?? []);

		return $blog_id;
	}

	/**
	 * Update blog
	 *
	 * @param object $blog Blog
	 * @param array $data Blog data
	 * @return void
	 */
	private static function _update($blog, $data)
	{
		$now = date('Y-m-d H:i:s');

		// Format update
		$update_values = [
			'lang_id' => $data['lang_id'],
			'is_active' => $data['is_active'] ?? 0,
			'visible_at' => $data['visible_at'] ?? '',
			'updated_at' => $now,
		];

		// Update blog
		DB::table('blogs')
			->where('id', $blog->id)
			->update($update_values);

		// Refresh blog
		$blog->refreshData();

		// Update thumbnail
		if (isset($data['thumbnail']) === true)
			self::_updateThumbnail($blog, $data['thumbnail']);

		// Update cover
		if (isset($data['cover']) === true)
			self::_updateCover($blog, $data['cover']);

		// Update langs
		BlogLang::update($blog, $data['langs'] ?? []);

		// Update categories
		BlogCategory::update($blog, $data['categories'] ?? []);

		// Update modules
		BlogModule::update($blog, $data['modules'] ?? []);
	}

	/**
	 * Duplicate blog
	 *
	 * @param object $blog Blog
	 * @return integer
	 */
	private static function _duplicate($blog)
	{
		$now = date('Y-m-d H:i:s');

		// Duplicate blog
		$blog_duplicated_id = DB::table('blogs')
			->insertGetId([
				'lang_id' => $blog->lang_id,
				'is_active' => 0,
				'visible_at' => $blog->visible_at,
				'created_at' => $now,
			]);

		// Get duplicated blog
		$blog_duplicated = self::one($blog_duplicated_id);

		// Duplicate images
		$images_options = ['path' => self::getUploadsDirBaseName($blog_duplicated)];

		DB::table('blogs')
			->where('id', $blog_duplicated->id)
			->update([
				'image_id' => $blog->cover->duplicate($images_options)->id,
				'thumbnail_id' => $blog->thumbnail->duplicate($images_options)->id,
			]);

		// Duplicate langs
		BlogLang::duplicateAll($blog, $blog_duplicated);

		// Duplicate categories
		BlogCategory::duplicateAll($blog, $blog_duplicated);

		// Duplicate modules
		BlogModule::duplicateAll($blog, $blog_duplicated);

		return $blog_duplicated_id;
	}

	/**
	 * Update blog cover
	 *
	 * @param object $blog Blog
	 * @param array $thumbnail Blog thumbnail
	 * @return void
	 */
	private static function _updateThumbnail($blog, $thumbnail)
	{
		if (Image::isUploaded($thumbnail) === false)
			return;

		$now = date('Y-m-d H:i:s');

		// Format update
		$update_values = [
			'thumbnail_id' => Image::upload($blog->thumbnail_id, $thumbnail, [
				'path' => self::getUploadsDirBaseName($blog),
			]),
			'updated_at' => $now,
		];

		// Update blog
		DB::table('blogs')
			->where('id', $blog->id)
			->update($update_values);
	}

	/**
	 * Update blog cover
	 *
	 * @param object $blog Blog
	 * @param array $cover Blog cover
	 * @return void
	 */
	private static function _updateCover($blog, $cover)
	{
		if (Image::isUploaded($cover) === false)
			return;

		$now = date('Y-m-d H:i:s');

		// Format update
		$update_values = [
			'image_id' => Image::upload($blog->image_id, $cover, [
				'path' => self::getUploadsDirBaseName($blog),
			]),
			'updated_at' => $now,
		];

		// Update blog
		DB::table('blogs')
			->where('id', $blog->id)
			->update($update_values);
	}

	/**
	 * Delete blog
	 *
	 * @param object $blog Blog
	 * @return void
	 */
	private static function _delete($blog)
	{
		// Delete cover
		$blog->cover->delete();

		// Delete thumbnail
		$blog->thumbnail->delete();

		// Delete resources folder
		recursive_rmdir(UtilsImage::getDir(self::getUploadsDirBaseName($blog)));

		// Delete langs
		BlogLang::deleteAll($blog);

		// Delete categories
		BlogCategory::deleteAll($blog);

		// Delete modules
		BlogModule::deleteAll($blog, true);

		// Delete blog
		DB::table('blogs')
			->where('id', $blog->id)
			->delete();
	}



	/****************************************************************
	 * Validations
	 ***************************************************************/

	/**
	 * Validate exists
	 *
	 * @param integer $blog_id Blog id
	 * @return MessageBag
	 */
	public static function validateExists($blog_id)
	{
		$errors = Validator::make(['blog_id' => $blog_id], [
			'blog_id' => 'required|numeric|blog_exists',
		])->errors();

		return $errors;
	}

	/**
	 * Validate insert
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateInsert($data)
	{
		$errors = Validator::make($data, [
			'lang_id' => 'required|numeric|lang_exists',
			'is_active' => 'boolean',
			'visible_at' => 'required|date',
		])->errors();

		$errors = self::validateUpdateThumbnail($data)->merge($errors);
		$errors = self::validateUpdateCover($data)->merge($errors);
		$errors = BlogLang::validateInsert($data['lang_id'] ?? 0, $data['langs'] ?? [])->merge($errors);
		$errors = BlogCategory::validateInsert($data['categories'] ?? [])->merge($errors);

		return $errors;
	}

	/**
	 * Validate update
	 *
	 * @param integer $blog_id Blog id
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateUpdate($blog_id, $data)
	{
		$errors = Validator::make($data, [
			'lang_id' => 'required|numeric|lang_exists',
			'is_active' => 'boolean',
			'visible_at' => 'required|date',
		])->errors();

		if (isset($data['thumbnail']) === true)
			$errors = self::validateUpdateThumbnail($data)->merge($errors);

		if (isset($data['cover']) === true)
			$errors = self::validateUpdateCover($data)->merge($errors);

		$errors = self::validateExists($blog_id)->merge($errors);
		$errors = BlogLang::validateUpdate($blog_id, $data['lang_id'] ?? 0, $data['langs'] ?? [])->merge($errors);
		$errors = BlogCategory::validateUpdate($data['categories'] ?? [])->merge($errors);
		$errors = BlogModule::validateUpdate($data['modules'] ?? [])->merge($errors);

		return $errors;
	}

	/**
	 * Validate thumbnail update
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateUpdateThumbnail($data)
	{
		return Image::validateUpload($data['thumbnail'] ?? '', true, true, 'thumbnail');
	}

	/**
	 * Validate cover update
	 *
	 * @param array $data Array to validate
	 * @return MessageBag
	 */
	public static function validateUpdateCover($data)
	{
		return Image::validateUpload($data['cover'] ?? '', true, true, 'cover');
	}
}
