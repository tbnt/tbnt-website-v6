<?php

namespace App\Providers;

use App\Liquid\MyError;

use Request;
use Response;
use Illuminate\Support\ServiceProvider;

use JWTFactory;
use Tymon\JWTAuth\Claims\NotBefore;

class ResponseServiceProvider extends ServiceProvider
{
	/**
	 * Perform post-registration booting of services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->macroSuccess();
		$this->macroError();
		$this->macroResponse();

		$this->macroNotAuthorized();
		$this->macroLoggedOut();
		$this->macroCsrfToken();
		$this->macroPostTooLarge();
		$this->macroNeedReload();

		$this->macroNotFound();
		$this->macroNotAllowed();
		$this->macroServerError();

		$this->macroJwtExpired();
		$this->macroJwtInvalid();
		$this->macroJwtAbsent();
		$this->macroJwtUserNotFound();
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroSuccess()
	{
		Response::macro('success', function ($value = '')
		{
			return Response::response($value);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroError()
	{
		Response::macro('error', function ($error = '')
		{
			return Response::response($error, 400);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroResponse()
	{
		Response::macro('response', function ($response, $code = 200)
		{
			if (is_array($response) === false)
				$response = [$response];

			return Response::json($response, $code);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroNotAuthorized()
	{
		Response::macro('not_authorized', function ()
		{
			$message_bag = message_bag()
				->add('not_authorized', trans('errors.not_authorized'));

			if (is_ajax_group() === true)
				return Response::json($message_bag, 401);

			return response()->view('errors.not-authorized');
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroLoggedOut()
	{
		Response::macro('logged_out', function ()
		{
			$message_bag = message_bag()
				->add('logged_out', trans('errors.logged_out'));

			if (is_ajax_group() === true)
				return Response::json($message_bag, 401);

			return response()->view('errors.logged-out');
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroCsrfToken()
	{
		Response::macro('csrf_token', function ()
		{
			$message_bag = message_bag()
				->add('csrf_token', trans('errors.csrf_token'));

			if (is_ajax_group() === true)
				return Response::json($message_bag, 401);

			return redirect()->back()->withErrors($message_bag);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroPostTooLarge()
	{
		Response::macro('post_too_large', function ()
		{
			$message_bag = message_bag()
				->add('post_too_large', trans('errors.post_too_large'));

			if (is_ajax_group() === true)
				return Response::json($message_bag, 400);

			return redirect()->back()->withErrors($message_bag);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroNeedReload()
	{
		Response::macro('need_reload', function ()
		{
			$message_bag = message_bag()
				->add('need_reload', trans('errors.need_reload'));

			if (is_ajax_group() === true)
				return Response::json($message_bag, 400);

			return redirect()->back()->withErrors($message_bag);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroNotFound()
	{
		Response::macro('notFound', function ()
		{
			return Response::response('not_found', 404);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroNotAllowed()
	{
		Response::macro('notAllowed', function ()
		{
			return Response::response('not_allowed', 405);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroServerError()
	{
		Response::macro('serverError', function ()
		{
			return Response::response('server_error', 503);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroJwtExpired()
	{
		Response::macro('jwtExpired', function ()
		{
			$claims = JWTFactory::resolveClaims();

			foreach ($claims as $claim) {
				if ($claim instanceof NotBefore) {
					$now = new \DateTime('now');
					$now->modify('-'.env('JWT_REFRESH_TTL').' minutes');

					$nbf = new \DateTime('now');
					$nbf->setTimestamp($claim->getValue());

					// var_dump(env('JWT_TTL'));
					// var_dump(env('JWT_REFRESH_TTL'));

					// var_dump($now);
					// var_dump($nbf);

					if ($nbf < $now) return Response::jwtInvalid();
				}
			}

			return Response::response('token_expired', 401);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroJwtInvalid()
	{
		Response::macro('jwtInvalid', function ()
		{
			return Response::response('token_invalid', 401);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroJwtAbsent()
	{
		Response::macro('jwtAbsent', function ()
		{
			return Response::response('token_absent', 401);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function macroJwtUserNotFound()
	{
		Response::macro('jwtUserNotFound', function ()
		{
			return Response::response('token_no_user', 401);
		});
	}
}
