<?php

namespace App\Providers;

use App\Project\Blog;
use App\Project\Category;
use App\Project\File;
use App\Project\Image;
use App\Project\Lang;
use App\Project\Module;
use App\Project\Module\Type as ModuleType;
use App\Project\Utils\Google;

use Validator;

use ForceUTF8\Encoding;
use Illuminate\Support\ServiceProvider;

class ValidatorServiceProvider extends ServiceProvider
{
	/**
	 * Register bindings in the container.
	 *
	 * @return void
	 */
	public function boot()
	{
		/*
		|--------------------------------------------------------------------------
		| Date must be before or equal from :date_to
		|--------------------------------------------------------------------------
		|
		| before_equal:date_to
		|
		*/

		Validator::extend('before_equal', function($attribute, $value, $parameters, $validator)
		{
			return strtotime($validator->getData()[$parameters[0]]) <= strtotime($value);
		});

		/*
		|--------------------------------------------------------------------------
		| Date must be after or equal to :date_from
		|--------------------------------------------------------------------------
		|
		| after_equal:date_from
		|
		*/

		Validator::extend('after_equal', function($attribute, $value, $parameters, $validator)
		{
			return strtotime($validator->getData()[$parameters[0]]) >= strtotime($value);
		});

		/*
		|--------------------------------------------------------------------------
		| Time must be in format H:i
		|--------------------------------------------------------------------------
		|
		| time_format
		|
		*/

		Validator::extend('time_format', function($attribute, $value, $parameters, $validator)
		{
			return Validator::make(['time' => $value], ['time' => 'date_format:"H:i"'])->passes();
		});

		/*
		|--------------------------------------------------------------------------
		| String must not contains invalid UTF-8 characters
		|--------------------------------------------------------------------------
		|
		| is_utf8_malformed
		|
		*/

		Validator::extend('is_utf8_malformed', function($attribute, $value, $parameters, $validator)
		{
			return $value === '' || $value === Encoding::fixUTF8($value);
		});

		/*
		|--------------------------------------------------------------------------
		| Geocode should succeed
		|--------------------------------------------------------------------------
		|
		| geocode_success
		|
		*/

		Validator::extend('geocode_success', function($attribute, $value, $parameters, $validator)
		{
			return Google::geocode($value) !== false;
		});

		/*
		|--------------------------------------------------------------------------
		| Lang id must exists
		|--------------------------------------------------------------------------
		|
		| lang_exists
		|
		*/

		Validator::extend('lang_exists', function($attribute, $value, $parameters, $validator)
		{
			return Lang::exists($value);
		});

		/*
		|--------------------------------------------------------------------------
		| Lang code must exists
		|--------------------------------------------------------------------------
		|
		| lang_code_exists
		|
		*/

		Validator::extend('lang_code_exists', function($attribute, $value, $parameters, $validator)
		{
			return Lang::codeExists($value);
		});

		/*
		|--------------------------------------------------------------------------
		| Image id must exists
		|--------------------------------------------------------------------------
		|
		| image_exists
		|
		*/

		Validator::extend('image_exists', function($attribute, $value, $parameters, $validator)
		{
			return Image::exists($value);
		});

		/*
		|--------------------------------------------------------------------------
		| File id must exists
		|--------------------------------------------------------------------------
		|
		| file_exists
		|
		*/

		Validator::extend('file_exists', function($attribute, $value, $parameters, $validator)
		{
			return File::exists($value);
		});

		/*
		|--------------------------------------------------------------------------
		| Blog id must exists
		|--------------------------------------------------------------------------
		|
		| blog_exists
		|
		*/

		Validator::extend('blog_exists', function($attribute, $value, $parameters, $validator)
		{
			return Blog::exists($value);
		});

		/*
		|--------------------------------------------------------------------------
		| Blog url must be unique
		|--------------------------------------------------------------------------
		|
		| blog_url_unique
		|
		*/

		Validator::extend('blog_url_unique', function($attribute, $value, $parameters, $validator)
		{
			list($blog_id, $lang_id) = array_map('intval', $parameters);

			$blog_id_exists = Blog::slugId(str_slug($value), $lang_id);

			return $blog_id_exists === $blog_id || $blog_id_exists == 0;
		});

		/*
		|--------------------------------------------------------------------------
		| Category id must exists
		|--------------------------------------------------------------------------
		|
		| category_exists
		|
		*/

		Validator::extend('category_exists', function($attribute, $value, $parameters, $validator)
		{
			return Category::exists($value);
		});

		/*
		|--------------------------------------------------------------------------
		| Category url must be unique
		|--------------------------------------------------------------------------
		|
		| category_url_unique
		|
		*/

		Validator::extend('category_url_unique', function($attribute, $value, $parameters, $validator)
		{
			list($category_id, $lang_id) = array_map('intval', $parameters);

			$category_id_exists = Category::slugId(str_slug($value), $lang_id);

			return $category_id_exists === $category_id || $category_id_exists == 0;
		});

		/*
		|--------------------------------------------------------------------------
		| Module id must exists
		|--------------------------------------------------------------------------
		|
		| module_exists
		|
		*/

		Validator::extend('module_exists', function($attribute, $value, $parameters, $validator)
		{
			return Module::exists($value);
		});

		/*
		|--------------------------------------------------------------------------
		| Module type must exists
		|--------------------------------------------------------------------------
		|
		| module_type_exists
		|
		*/

		Validator::extend('module_type_exists', function($attribute, $value, $parameters, $validator)
		{
			return ModuleType::typeExists($value);
		});
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
