<?php

namespace App;

use ArrayObject;
use ArrayIterator;
use IteratorAggregate;

/**
 * sources: https://gist.github.com/eaglstun/1100119
 *
 * extended Array Object, used for getting and setting though both array or object style language,
 * ex $mt->foo or $mt['foo']
 * also doesnt not throw error when unset key is accessed
 *
 * @version 1.0
 * @see function mt()
 * @see http://www.php.net/manual/en/class.arrayobject.php
 * @see http://php.net/manual/en/class.iteratoraggregate.php
 * @see http://php.net/manual/en/language.oop5.overloading.php
 */
class Project extends ArrayObject implements IteratorAggregate
{
	private $storage = array();

	/**
	 * construct a new array object
	 *
	 * @see http://www.php.net/manual/en/arrayobject.construct.php
	 * @param mixed $default Default value
	 */
	public function __construct($default = null)
	{
		if(!is_null($default))
			$this->storage = (array) $default;

		parent::setFlags(parent::ARRAY_AS_PROPS);
		parent::setFlags(parent::STD_PROP_LIST);
	}

	/**
	 * Get value
	 * called on accessing $mt->xxx
	 *
	 * @param int|string $k Key
	 * @return mixed
	 */
	public function __get($k)
	{
		return isset($this->storage[$k]) ? $this->storage[$k] : false;
	}

	/**
	 * Get value
	 * called on accessing $mt[xxx]
	 *
	 * @param int|string $k Key
	 * @return mixed
	 */
	public function offsetGet($k)
	{
		return isset($this->storage[$k]) ? $this->storage[$k] : false;
	}

	/**
	 * Set value
	 * called on $mt->xxx = yyy
	 *
	 * @param int|string $k Key
	 * @param mixed $v Value
	 * @return void
	 */
	public function __set($k, $v)
	{
		$this->storage[$k] = $v;
	}

	/**
	 * Set value
	 * called on $mt[xxx] = yyy
	 *
	 * @param int|string $k Key
	 * @param mixed $v Value
	 * @return void
	 */
	public function offsetSet($k, $v)
	{
		is_null($k) ? array_push($this->storage, $v) : $this->storage[$k] = $v;
	}

	/**
	 * Value isset
	 * called on isset($mt->xxx)
	 *
	 * @param int|string $k Key
	 * @return boolean
	 */
	public function __isset($k)
	{
		return isset($this->storage[$k]);
	}

	/**
	 * Value isset
	 * called on isset($mt[xxx])
	 *
	 * @param int|string $k Key
	 * @return boolean
	 */
	public function offsetExists($k)
	{
		return isset($this->storage[$k]);
	}

	/**
	 * Value unset
	 * called on unset($mt->xxx);
	 *
	 * @param int|string $k Key
	 * @return void
	 */
	public function __unset($k)
	{
		unset($this->storage[$k]);
	}

	/**
	 * Value unset
	 * called on unset($mt[xxx]);
	 *
	 * @param int|string $k Key
	 * @return void
	 */
	public function offsetUnset($k)
	{
		unset($this->storage[$k]);
	}

	/**
	 * called on $mt->count() or count($mt)
	 *
	 * @return int
	 */
	public function count()
	{
		return count($this->storage);
	}

	/**
	 * called on $mt->asort()
	 *
	 * @return void
	 */
	public function asort()
	{
		asort($this->storage);
	}

	/**
	 * called on $mt->ksort()
	 *
	 * @return void
	 */
	public function ksort()
	{
		ksort($this->storage);
	}

	/**
	 * called on foreach()
	 *
	 * @return ArrayIterator
	 */
	public function getIterator()
	{
		return new ArrayIterator($this->storage);
	}

	/**
	 * Get data as object
	 *
	 * @return object
	 */
	public function toObject()
	{
		return (object) $this->storage;
	}

	/**
	 * Get data as array
	 *
	 * @return array
	 */
	public function toArray()
	{
		return (array) $this->storage;
	}

	/**
	 * Refresh data
	 *
	 * @return void
	 */
	public function updateData($data)
	{
		foreach ($data as $k => &$v) $this->{$k} = $v;
	}

	/**
	 * Initialize object
	 *
	 * @param string $class_name Child class name
	 * @param mixed $data Object data
	 * @param integer $lang_id Lang id
	 * @return self
	 */
	public static function initialize($class_name, $data, $lang_id = 0)
	{
		if (is_array($data) === true || is_collection($data) === true) {
			foreach ($data as $k => $v)
				$data[$k] = $data === null ? null : new $class_name($v, $lang_id);
		}
		else {
			$data = $data === null ? null : new $class_name($data, $lang_id);
		}

		return $data;
	}
}
