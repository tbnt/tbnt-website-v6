$( document ).ready(function() {
	var videoEl = document.querySelector('#showreel');
	var controlPlay = document.querySelector('#control-play');
	var controlPause = document.querySelector('#control-pause')
	var controlVolumeUp = document.querySelector('#control-volume-up');
	var controlVolumeDown = document.querySelector('#control-volume-down');
	var boxTeam = document.querySelector('#boxTeam');
	var cursor = document.querySelector('#mainCursor');
	var camera = document.querySelector('#camera');
	var startTour = document.querySelector('#startTour');


	var service1 = document.querySelector('#service1');
	var service2 = document.querySelector('#service2');
	var service3 = document.querySelector('#service3');
	var service4 = document.querySelector('#service4');


	var infoPanel =  $('.info-panel');
	var infoContainer =  $('.info-container');
	var infoContent =  $('.info-content');
	videoEl.pause();

	// boxTeam.setAttribute('animation__rotation', "property: rotation; from: 0 0; to: 0 360 0; dur: 8000; loop: true; easing:easeInOutCubic")

	$(startTour).on('click', function(){
		// camera.setAttribute('animation__scale', "property: scale; from: 0 0 0; to: 5 5 5; dur: 2000");
		// camera.setAttribute('animation__rotation', "property: rotation; from:0 0 0; to: 0 -90 0; dur: 2000");
		camera.removeAttribute('animation__position');
		// camera.setAttribute('animation__position', "property: position; from:0 0 0; to: 0 0 -9; dur: 2000");
		// var tlService = new TimelineMax();
		// tlService
		// // .to(infoPanel, .6, { width: '150px',})
		// .to(infoPanel, .8 , { width: '100%', height: '100%', left: '0%', top: '0%', marginTop: 0, marginLeft: 0 })
		// .to(infoContainer, .8, { autoAlpha: 1});
	});	$(service1).on('click', function(){
		// camera.setAttribute('animation__scale', "property: scale; from: 0 0 0; to: 5 5 5; dur: 2000");
		// camera.setAttribute('animation__rotation', "property: rotation; from:0 0 0; to: 0 -90 0; dur: 2000");
		camera.removeAttribute('animation__position');
		camera.setAttribute('animation__position', "property: position; from:0 0 -9; to: 0 0 -19; dur: 2000");
		// var tlService = new TimelineMax();
		// tlService
		// // .to(infoPanel, .6, { width: '150px',})
		// .to(infoPanel, .8 , { width: '100%', height: '100%', left: '0%', top: '0%', marginTop: 0, marginLeft: 0 })
		// .to(infoContainer, .8, { autoAlpha: 1});
	});


	$(controlPlay).on('click', function(){

		videoEl.play();

		controlPlay.removeAttribute('animation__position');
		controlPlay.removeAttribute('animation__opacity');

		controlPause.removeAttribute('animation__position');
		controlPause.removeAttribute('animation__opacity');

		controlPlay.setAttribute('animation__position', "property: position; from: 0 0 0; to: 0 -0.5 0; dur: 400");
		controlPlay.setAttribute('animation__opacity', "property: opacity; from:1; to:0 ; dur: 400");

		controlPause.setAttribute('animation__position', "property: position; from:0 0.5 0; to: 0 0 0; dur: 400");
		controlPause.setAttribute('animation__opacity', "property: opacity; from:0; to:1 ; dur: 400");

	});

	$(controlPause).on('click', function(){

		videoEl.pause();

		controlPlay.removeAttribute('animation__position');
		controlPlay.removeAttribute('animation__opacity');

		controlPause.removeAttribute('animation__position');
		controlPause.removeAttribute('animation__opacity');

		controlPause.setAttribute('animation__position', "property: position; from: 0 0 0; to: 0 -0.5 0; dur: 400");
		controlPause.setAttribute('animation__opacity', "property: opacity; from:1; to:0 ; dur: 400");

		controlPlay.setAttribute('animation__position', "property: position; from:0 0.5 0; to: 0 0 0; dur: 400");
		controlPlay.setAttribute('animation__opacity', "property: opacity; from:0; to:1 ; dur: 400");

	});

	$(controlVolumeUp).on('click', function(){

		videoEl.muted = true;

		controlVolumeUp.removeAttribute('animation__position');
		controlVolumeUp.removeAttribute('animation__opacity');

		controlVolumeDown.removeAttribute('animation__position');
		controlVolumeDown.removeAttribute('animation__opacity');

		controlVolumeUp.setAttribute('animation__position', "property: position; from: .5 0 0; to: .5 -0.5 0; dur: 400");
		controlVolumeUp.setAttribute('animation__opacity', "property: opacity; from:1; to:0 ; dur: 400");

		controlVolumeDown.setAttribute('animation__position', "property: position; from:.5 0.5 0; to: .5 0 0; dur: 400");
		controlVolumeDown.setAttribute('animation__opacity', "property: opacity; from:0; to:1 ; dur: 400");

	});
	$(controlVolumeDown).on('click', function(){

		videoEl.muted = false;

		controlVolumeUp.removeAttribute('animation__position');
		controlVolumeUp.removeAttribute('animation__opacity');

		controlVolumeDown.removeAttribute('animation__position');
		controlVolumeDown.removeAttribute('animation__opacity');

		controlVolumeDown.setAttribute('animation__position', "property: position; from: .5 0 0; to: .5 -0.5 0; dur: 400");
		controlVolumeDown.setAttribute('animation__opacity', "property: opacity; from:1; to:0 ; dur: 400");

		controlVolumeUp.setAttribute('animation__position', "property: position; from:.5 0.5 0; to: .5 0 0; dur: 400");
		controlVolumeUp.setAttribute('animation__opacity', "property: opacity; from:0; to:1 ; dur: 400");

	});


    // let hideCursor = function() {
    //   cursor.removeAttribute('animation__cursorHideLeave');
    //   cursor.setAttribute('animation__cursorHideEnter', "property: material.color; to:#FFFFFF; dur:300;");
    // }
    // let showCursor = function() {
    //   cursor.removeAttribute('animation__cursorHideEnter');
    //   cursor.setAttribute('animation__cursorHideLeave', "property: material.color; to:#000000; dur:300;");
    // }
    // document.querySelector('#showreelEl').addEventListener('mouseenter', hideCursor);
    // document.querySelector('#showreelEl').addEventListener('mouseleave', showCursor);


});
$(document).on('app.ready', function()
{
	console.log('!!! ANIM END !!!');
});