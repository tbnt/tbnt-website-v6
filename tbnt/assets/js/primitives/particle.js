'use strict';

/**
 * Particle primitive
 *
 * Based on a Codrops tutorial:
 * http://tympanus.net/codrops/2016/04/26/the-aviator-animating-basic-3d-scene-threejs/
 */

const extendDeep = AFRAME.utils.extendDeep;
const meshMixin = AFRAME.primitives.getMeshMixin();

let _particlesConfig = {
	x: [-2, 2],
	y: [-2, 2],
	z: [-3, -4],
	dur: [1000, 2000],
	dir: 'normal',
	easing: 'easeInOutCirc',
	colors: [
		'pink',
	],
};

const Primitive = module.exports.Primitive =
{
	defaultComponents: {
		geometry: {
			primitive: 'circle',
			radius: .05,
		},
		material: {
			color: 'pink',
		},
		position: { x: 3, y: 0, z: -3 },
		particle: {},
	},
	mappings: {
		radius: 'geometry.radius',
		x: 'position.x',
		y: 'position.y',
		z: 'position.z',
		color: 'material.color',
	}
};

const Component = module.exports.Component =
{
	schema: {},

	init: function ()
	{
		this.$el = $(this.el);

		this.initEvents();
		this.addAnimation();
	},

	initEvents: function (oldData)
	{
		let self = this;

		this.eventAnimationEnd = function () { self.removeAnimation(); self.addAnimation(); };
		this.attachAnimationEvent();
	},

	remove: function ()
	{
		this.detachAnimationEvent();
	},

	addAnimation: function ()
	{
		this.applyRandomAnimationAnimation();
	},

	removeAnimation: function ()
	{
		this.el.removeAttribute('animation');
	},

	attachAnimationEvent: function ()
	{
		this.el.addEventListener('animationcomplete', this.eventAnimationEnd);
	},

	detachAnimationEvent: function ()
	{
		this.el.removeEventListener('animationcomplete', this.eventAnimationEnd);
	},

	applyRandomAnimationAnimation: function ()
	{
		const cameraPosition = this.el.sceneEl.camera
			? this.el.sceneEl.camera.el.getAttribute('position')
			: { x: 0, y: 0, z: 0 };

		const cameraRotation = this.el.sceneEl.camera
			? this.el.sceneEl.camera.el.getAttribute('rotation')
			: { x: 0, y: 0, z: 0 };

		let x = parseFloat(getRandom(..._particlesConfig.x).toFixed(3));
		let y = parseFloat(getRandom(..._particlesConfig.y).toFixed(3));
		let z = parseFloat(getRandom(..._particlesConfig.z).toFixed(3));

		const dur = getRandom(..._particlesConfig.dur).toFixed(3);
		const easing = _particlesConfig.easing;
		const dir = _particlesConfig.dir = 'normal';

		if (this.el.sceneEl.camera) {
			let vec = new THREE.Vector3(x, y, z);
				vec.applyQuaternion(this.el.sceneEl.camera.el.object3D.quaternion);

			x = vec.x + cameraPosition.x;
			y = vec.y + cameraPosition.y;
			z = vec.z + cameraPosition.z;
		}

		this.el.setAttribute('rotation', cameraRotation);
		this.el.setAttribute('animation', `property: position; dir: ${dir}; dur: ${dur}; easing: ${easing}; to: ${x} ${y} ${z}`);
	},
};

const add = module.exports.add = (particlesCount = 1, config = {}) =>
{
	setConfig(config);

	let particles = [];

	for (let i = particlesCount - 1; i >= 0; i--) {
		particles.push(
			'<a-particle \
				x="' + getRandom(..._particlesConfig.x) + '" \
				y="' + getRandom(..._particlesConfig.y) + '" \
				z="' + getRandom(..._particlesConfig.z) + '" \
				color="' + _particlesConfig.colors[Math.floor(getRandom(0, _particlesConfig.colors.length))] + '" \
			></a-particle>'
		);
	}

	$('#app').append(particles);
};

const setConfig = module.exports.setConfig = (particlesConfig) =>
{
	_particlesConfig = _.extend({}, _particlesConfig, particlesConfig);
};

const getRandom = function (min, max)
{
	min = min || 0;
	max = max || 1;

	return parseFloat((Math.random() * (max - min) + min).toFixed(3));
};

module.exports.registerAll = (function () {
	var registered = false;
	return function (AFRAME) {
		if (registered) return;
		AFRAME = AFRAME || window.AFRAME;
		AFRAME.registerComponent('particle', Component);
		AFRAME.registerPrimitive('a-particle', extendDeep({}, meshMixin, Primitive));
		registered = true;
	};
}());
