'use strict';

module.exports = {
	primitives: [
		require('./particle'),
	],

	registerAll: function (AFRAME) {
		if (this._registered) return;
		AFRAME = AFRAME || window.AFRAME;

		for (let i = this.primitives.length - 1; i >= 0; i--)
			this.primitives[i].registerAll(AFRAME);

		this._registered = true;
	}
};
