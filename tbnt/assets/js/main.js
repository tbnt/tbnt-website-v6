'use strict';

// Load libraries
require('aframe');
require('aframe-animation-component');

// Load helpers
require('./helpers/index').registerAll();

// Load primitives
require('./primitives/index').registerAll();

// Load components
const particle = require('./primitives/particle');

// Add particles
$(document).ready(() =>
{
	particle.setConfig({
		x: [-6, 6],
		y: [-6, 6],
		z: [-6, -6],
		dur: [500, 800],
		colors: ['#1fd1b1', '#ffc870', '#574ae2', '#ee6352'],
		easing: 'easeInOutCirc',
	});

	particle.add(80);
});
