'use strict';

const securityTimeoutBlockLoader = false;
const securityTimeoutBlockLoaderTime = 1000 * 10;

const delayBeforeSendAppReady = 500;

let wasEmitted = false;

const initLoader = () =>
{
	$(document).ready(() => startLoader());
};

const startLoader = () =>
{
	let itemsLoadedCount = 0;
	let itemsToLoadCount = 0;

	const emitFinished = () =>
	{
		if (wasEmitted === true) return;
			wasEmitted = true;

		setTimeout(() => $(document).trigger('app.ready'), delayBeforeSendAppReady);
	};

	const loadItems = (items, eventName = 'load') =>
	{
		for (let i = 0, c = items.length; i < c; i++) {
			const item = items[i];

			if (isItemLoaded(item) === true) {
				itemsToLoadCount -= 1;

				if (itemsLoadedCount === itemsToLoadCount)
					emitFinished();

				continue;
			}

			item.addEventListener(eventName, function()
			{
				itemsLoadedCount += 1;

				if (itemsLoadedCount === itemsToLoadCount)
					emitFinished();
			});
		};
	};

	const images = document.querySelectorAll('img');
	const medias = document.querySelectorAll('video, audio');
	const assets = document.querySelectorAll('a-assets, a-asset-item, a-scene, a-entity');

	itemsToLoadCount = images.length + medias.length + assets.length;

	loadItems(images, 'load');
	loadItems(medias, 'loadeddata');
	loadItems(assets, 'loaded');

	setTimeout(() => emitFinished(), securityTimeoutBlockLoaderTime);
};

const isItemLoaded = function (item)
{
	const itemTagName = item.tagName.toLowerCase();

	if (itemTagName === 'video') {
		return item.readyState !== undefined && item.readyState === 4;
	}
	else if (itemTagName === 'img') {
		return item.complete !== undefined && item.complete === true;
	}

	return true;
};

module.exports.register = (function () {
	var registered = false;
	return function () {
		if (registered) return;

		initLoader();

		registered = true;
	};
}());
