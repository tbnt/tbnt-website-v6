'use strict';

module.exports = {
	helpers: [
		require('./app'),
	],

	registerAll: function () {
		if (this._registered) return;

		for (let i = this.helpers.length - 1; i >= 0; i--)
			this.helpers[i].register();

		this._registered = true;
	}
};
